tinymce.init({
  selector: 'textarea.tinymce', // change this value according to your HTML
  height: 1000,
  width:1200,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: 'undo redo | formatselect | ' +
  'bold italic ', 
  content_css: ['https://fonts.googleapis.com/css2?family=Hind+Siliguri:wght@300&display=swap'],
  content_style: 'body { font-family:Hind Siliguri, sans-serif; font-size:14px }',
  relative_urls: false,
});

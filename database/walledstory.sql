-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 09, 2021 at 01:08 AM
-- Server version: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- PHP Version: 7.3.25-1+ubuntu20.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `walledstory`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_authentication`
--

CREATE TABLE `user_authentication` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_token` varchar(255) NOT NULL,
  `user_slug` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_status` enum('0','1','5','9') NOT NULL COMMENT '0->Unverified, 1->Live, 5->Block, 9->Deactivated',
  `user_created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_authentication`
--

INSERT INTO `user_authentication` (`user_id`, `user_token`, `user_slug`, `user_name`, `user_password`, `user_email`, `user_status`, `user_created_at`) VALUES
(1, '31eeb344a5adff77f9b60f6ac41f5f3b', '31eeb344a5adff77f9b60f6ac41f5f3b', 'sarasij94', '$2y$10$5DoVOuD/B5dvSPhoQ1xRE.IRza96/EANbXpjVv0RGfR1m0Wel0thW', 'sarasij94@gmail.com', '1', '2021-01-26 20:27:50');

-- --------------------------------------------------------

--
-- Table structure for table `user_cat_stat`
--

CREATE TABLE `user_cat_stat` (
  `usr_cat_stat_id` bigint(40) UNSIGNED NOT NULL,
  `cat_id` int(11) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `count` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_cat_stat`
--

INSERT INTO `user_cat_stat` (`usr_cat_stat_id`, `cat_id`, `user_id`, `count`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_extra`
--

CREATE TABLE `user_extra` (
  `user_extra_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_token` varchar(255) NOT NULL,
  `user_profession` varchar(255) DEFAULT NULL,
  `user_post_count` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_extra`
--

INSERT INTO `user_extra` (`user_extra_id`, `user_id`, `user_token`, `user_profession`, `user_post_count`) VALUES
(1, 1, '31eeb344a5adff77f9b60f6ac41f5f3b', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_info_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_token` varchar(255) NOT NULL,
  `user_info_address` longtext DEFAULT NULL,
  `user_info_city` varchar(100) DEFAULT NULL,
  `user_info_state` varchar(100) DEFAULT NULL,
  `user_info_zip` varchar(20) DEFAULT NULL,
  `user_info_phone` varchar(20) DEFAULT NULL,
  `user_info_country` varchar(255) DEFAULT NULL,
  `user_info_fname` varchar(255) DEFAULT NULL,
  `user_info_lname` varchar(255) DEFAULT NULL,
  `user_info_gender` enum('0','1','2','3') DEFAULT NULL COMMENT '0->Do not want to specify, 1->Male. 2->Female, 3->Others',
  `user_image` varchar(255) DEFAULT NULL,
  `user_bio` longtext DEFAULT NULL,
  `user_profession` varchar(255) DEFAULT NULL,
  `show_name` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0->Dont Show, 1->show',
  `show_profession` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0->Dont Show, 1->show',
  `show_image` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0->Dont Show, 1->show',
  `user_cover_image` varchar(255) DEFAULT NULL,
  `show_address` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0->Dont Show, 1->show',
  `show_gender` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0->Dont Show, 1->show',
  `show_bio` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0->Dont Show, 1->show'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_info_id`, `user_id`, `user_token`, `user_info_address`, `user_info_city`, `user_info_state`, `user_info_zip`, `user_info_phone`, `user_info_country`, `user_info_fname`, `user_info_lname`, `user_info_gender`, `user_image`, `user_bio`, `user_profession`, `show_name`, `show_profession`, `show_image`, `user_cover_image`, `show_address`, `show_gender`, `show_bio`) VALUES
(1, 1, '31eeb344a5adff77f9b60f6ac41f5f3b', '10/G Panpara 5th lane.', 'Kolkata', 'West Bengal', '700123', '8961819473', 'India', 'Sarasij', 'Roy', '1', 'http://localhost/walledstory/uploads/profile/1612681110.png', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'Software Developer', '1', '1', '1', 'http://localhost/walledstory/uploads/cover/1612685142.png', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_registration_otp`
--

CREATE TABLE `user_registration_otp` (
  `otp_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_token` varchar(255) NOT NULL,
  `otp` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_template_statistic`
--

CREATE TABLE `user_template_statistic` (
  `usr_temp_stat_id` bigint(40) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `template` enum('0','1','2','') NOT NULL COMMENT '0->text,1->image,2->video',
  `count` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_web_device_info`
--

CREATE TABLE `user_web_device_info` (
  `user_device_info_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_token` varchar(255) NOT NULL,
  `http_host` varchar(100) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `mobile_browser` varchar(255) DEFAULT NULL,
  `device` mediumtext DEFAULT NULL,
  `server_address` varchar(100) DEFAULT NULL,
  `server_port` varchar(100) DEFAULT NULL,
  `remote_address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_web_device_info`
--

INSERT INTO `user_web_device_info` (`user_device_info_id`, `user_id`, `user_token`, `http_host`, `browser`, `mobile_browser`, `device`, `server_address`, `server_port`, `remote_address`) VALUES
(1, 1, '31eeb344a5adff77f9b60f6ac41f5f3b', 'localhost', NULL, NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', '::1', '80', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `wst_admin`
--

CREATE TABLE `wst_admin` (
  `admin_id` bigint(150) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_ph` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_name` varchar(255) NOT NULL,
  `remember_me_token` varchar(255) DEFAULT NULL,
  `admin_created_by` bigint(150) NOT NULL,
  `admin_created_at` datetime NOT NULL,
  `admin_modifeid_by` bigint(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wst_admin`
--

INSERT INTO `wst_admin` (`admin_id`, `admin_username`, `admin_password`, `admin_ph`, `admin_email`, `admin_name`, `remember_me_token`, `admin_created_by`, `admin_created_at`, `admin_modifeid_by`) VALUES
(1, 'abc', '$2y$10$nftDspy6OGQnndgoepxFq.ZLwH3uxRgm1fz/PLloBR5wLPtag6x1q', '896181473', 'a@b.c', 'Sarasij Roy', NULL, 0, '2020-10-15 13:22:35', NULL),
(2, 'pqr', '$2y$10$nftDspy6OGQnndgoepxFq.ZLwH3uxRgm1fz/PLloBR5wLPtag6x1q', '8348465124', 'p@q.r', 'Sk Sakir Ali', NULL, 0, '2020-10-15 21:33:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wst_category`
--

CREATE TABLE `wst_category` (
  `cat_id` int(11) UNSIGNED NOT NULL,
  `cat_slug` varchar(255) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `cat_count` bigint(20) UNSIGNED NOT NULL,
  `cat_created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `cat_created_by` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wst_category`
--

INSERT INTO `wst_category` (`cat_id`, `cat_slug`, `cat_name`, `cat_count`, `cat_created_at`, `cat_created_by`) VALUES
(1, 'fiction', 'Fiction', 0, '2020-12-08 14:15:49', 1),
(2, 'short-stories', 'Short Stories', 0, '2020-12-08 14:16:16', 1),
(3, 'poem', 'Poem', 0, '2021-01-26 15:28:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wst_category_modification`
--

CREATE TABLE `wst_category_modification` (
  `cat_mod_id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wst_category_modification`
--

INSERT INTO `wst_category_modification` (`cat_mod_id`, `cat_id`, `modified_by`, `modified_at`) VALUES
(1, 2, 1, '2020-12-08 19:57:06'),
(2, 2, 1, '2020-12-08 19:57:25');

-- --------------------------------------------------------

--
-- Table structure for table `wst_gallery`
--

CREATE TABLE `wst_gallery` (
  `gallery_id` bigint(40) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_url` varchar(255) NOT NULL,
  `file_size` varchar(255) NOT NULL,
  `gallery_type` enum('0','1','2') NOT NULL COMMENT '0->profile,1->posts,2->cover'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wst_posts`
--

CREATE TABLE `wst_posts` (
  `post_id` bigint(30) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `post_slug` varchar(255) DEFAULT NULL,
  `post_title` varchar(255) DEFAULT NULL,
  `post_content` longtext NOT NULL,
  `featured_img_url` varchar(255) DEFAULT NULL,
  `featured_img_id` bigint(20) UNSIGNED DEFAULT NULL,
  `cat_id` int(11) NOT NULL,
  `template` enum('0','1','2','') NOT NULL COMMENT '0->text,1->image,2->youtube',
  `post_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wst_posts_modification`
--

CREATE TABLE `wst_posts_modification` (
  `post_mod_id` bigint(40) UNSIGNED NOT NULL,
  `post_id` bigint(30) UNSIGNED NOT NULL,
  `modified_by` bigint(20) UNSIGNED NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_authentication`
--
ALTER TABLE `user_authentication`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_cat_stat`
--
ALTER TABLE `user_cat_stat`
  ADD PRIMARY KEY (`usr_cat_stat_id`);

--
-- Indexes for table `user_extra`
--
ALTER TABLE `user_extra`
  ADD PRIMARY KEY (`user_extra_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_info_id`);

--
-- Indexes for table `user_registration_otp`
--
ALTER TABLE `user_registration_otp`
  ADD PRIMARY KEY (`otp_id`);

--
-- Indexes for table `user_template_statistic`
--
ALTER TABLE `user_template_statistic`
  ADD PRIMARY KEY (`usr_temp_stat_id`);

--
-- Indexes for table `user_web_device_info`
--
ALTER TABLE `user_web_device_info`
  ADD PRIMARY KEY (`user_device_info_id`);

--
-- Indexes for table `wst_admin`
--
ALTER TABLE `wst_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `wst_category`
--
ALTER TABLE `wst_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `wst_category_modification`
--
ALTER TABLE `wst_category_modification`
  ADD PRIMARY KEY (`cat_mod_id`);

--
-- Indexes for table `wst_gallery`
--
ALTER TABLE `wst_gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Indexes for table `wst_posts`
--
ALTER TABLE `wst_posts`
  ADD PRIMARY KEY (`post_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_authentication`
--
ALTER TABLE `user_authentication`
  MODIFY `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_cat_stat`
--
ALTER TABLE `user_cat_stat`
  MODIFY `usr_cat_stat_id` bigint(40) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_extra`
--
ALTER TABLE `user_extra`
  MODIFY `user_extra_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_info_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_registration_otp`
--
ALTER TABLE `user_registration_otp`
  MODIFY `otp_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_template_statistic`
--
ALTER TABLE `user_template_statistic`
  MODIFY `usr_temp_stat_id` bigint(40) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_web_device_info`
--
ALTER TABLE `user_web_device_info`
  MODIFY `user_device_info_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wst_admin`
--
ALTER TABLE `wst_admin`
  MODIFY `admin_id` bigint(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wst_category`
--
ALTER TABLE `wst_category`
  MODIFY `cat_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wst_category_modification`
--
ALTER TABLE `wst_category_modification`
  MODIFY `cat_mod_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wst_gallery`
--
ALTER TABLE `wst_gallery`
  MODIFY `gallery_id` bigint(40) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wst_posts`
--
ALTER TABLE `wst_posts`
  MODIFY `post_id` bigint(30) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Controller extends MX_Controller {

    public $data;

    function __construct() {
        parent:: __construct();
        date_default_timezone_set("Asia/Kolkata");
        
        $this->data['admin_assets'] = base_url('/assets-admin/');
        $this->data['profile_pic_placeholders'] = base_url('/assets/files/profile-pic-placeholder.png');
        $this->data['profile_cover_placeholders'] = base_url('/assets/files/profile_cover_placeholder.png');
        $this->data['default_placeholders'] = base_url('/assets/files/default-placeholder.png');
        $this->data['logo'] = base_url('/assets/files/logo.png');
        

        if( isset($this->session->userdata['wst23xyzsdfretw89lk_user_token']) ){
			$this->load->model('front/frontmodel');
			$this->load->model('admin/adminmodel');
	        $user_token 			= $this->session->userdata['wst23xyzsdfretw89lk_user_token'];
			$user_details 			= $this->frontmodel->get_user_info( $user_token );
			
			$user_status 			= $user_details['user_status'];
			$user_info_fname 		= $user_details['user_info_fname'];
			$user_info_lname 		= $user_details['user_info_lname'];
			$profile_img 			= $user_details['user_image'];
			$show_image 			= $user_details['show_image'];
			$show_name 				= $user_details['show_name'];
	       	$user_id 				= $user_details['user_id'];
	       	$all_cat 				= $this->adminmodel->get_cat_list();
	       	// $header_notifications	= $this->frontmodel->fetch_header_notification( $user_id );

	        $header_data_array = array(
	        	'fname' 				=> $user_info_fname,
	        	'lname'					=> $user_info_lname,
	        	'show_name'				=> $show_name,
	        	'profile_img'			=> $profile_img,
	        	'show_image'			=> $show_image,
	        	'cat_list_head'			=> $all_cat,
	        	// 'header_notifications'	=> $header_notifications,
	        );

	        $this->data['header_data_array'] = $header_data_array;
		}

		$this->data['notification_icons'] = array(
			'0'		=> '<i class="fas fa-heart" style="font-size: 14px;color: #000;"></i>',
			'1'		=> '<i class="fas fa-user-plus" style="font-size: 14px;color: #000;"></i>',
			'2'		=> '<i class="fas fa-envelope-square" style="font-size: 14px;color: #000;"></i>',
		);
           
    }//constructor

    public function sendotp( $ph_no=NULL, $msg_content=NULL ){
        $url = 'https://api.msg91.com/api/v5/otp?extra_param={"Param1":"Value1", "Param2":"Value2", "Param3": "Value3"}&authkey=347811Aaak0z0gzY6064200cP1&template_id=606421417ef9e20cf35dc752&mobile=91'.$ph_no.'&invisible=1&otp='.$msg_content.'&userip=IPV4 User IP&email=Email ID';
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          //https://api.msg91.com/api/v5/otp?extra_param={"Param1":"Value1", "Param2":"Value2", "Param3": "Value3"}&authkey=347811AIc0HBvTTgH95fbcb21bP1&template_id=5fbcb7e084311351f503609a&mobile=8961819473&invisible=1&otp=1234&userip=IPV4 User IP&email=Email ID
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
          CURLOPT_HTTPHEADER => array(
            "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }

    }//fn

    public function push_notification( $body, $user_id){
        $url = "https://fcm.googleapis.com/fcm/send";
        // $token = $device_id;
        $topic = '/topic/'.$user_id;
        $serverKey = 'AAAAJuyqIhk:APA91bG3O8jZrR3a8LK296wTvSD0BXYzNtq1ZtHJWFZC0G74X_J7dsI1jR0ysPx_7_KBAMVYtceKCVdj2UmR6SRquOKsJ44sr7AB-Qju1ICMHH7lnid1htfvwrQK_97V03nHSFUfzq3K';
 
        $title = $title;
        //$body = "Hello I am from Your php server";
        $notification = array( 'body' => $body, 'title' =>$title  );       
        // $arrayToSend = array('to' => $token,'collapse_key'=>'type_a','notification'=>$notification ,'data' => $notification);
        $arrayToSend = array('to' => $topic,'collapse_key'=>'type_a','data' => $notification);
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === FALSE) {
            //die('FCM Send Error: ' . curl_error($ch));
            return array(
                'code' => 0,
                'json' => $response
            );
        }else{
            return array(
                'code' => 1,
                'json' => $response
            );
        }
        curl_close($ch);
    }//fn

    function push_notification_like_post($device_id = NULL, $notification = NULL, $action_details = NULL){
        if($device_id == NULL || $notification == NULL){
            return 0; 
            exit();
        }
        $url = "https://fcm.googleapis.com/fcm/send";
        $token = $device_id;
        $serverKey = 'AAAAJuyqIhk:APA91bG3O8jZrR3a8LK296wTvSD0BXYzNtq1ZtHJWFZC0G74X_J7dsI1jR0ysPx_7_KBAMVYtceKCVdj2UmR6SRquOKsJ44sr7AB-Qju1ICMHH7lnid1htfvwrQK_97V03nHSFUfzq3K';

        //if()
        $body = "Hello I am from Your php server";
        $arrayToSend = array('to' => $token, 'data' => $notification,'priority'=>'high');
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === FALSE) {
            //die('FCM Send Error: ' . curl_error($ch));
            return '0';
            // echo '0';
        }else{
            return '1';
            // echo $response;
        }
        curl_close($ch);
    }
}
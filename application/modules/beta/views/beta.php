<!DOCTYPE html>
<html>
<head>
	<title>Walledstory</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css')?>">
	<link rel="icon" href="<?php echo $this->data['logo'] ?>" type="image/gif" sizes="16x16">
    <!-- <link rel="stylesheet" href="<?php //echo base_url('assets/css/croppie.css')?>"> -->
    <!-- <link rel="stylesheet" href="<?php //echo base_url('assets/css/style.css?v=').microtime()?>"> -->
    <!-- <link rel="stylesheet" href="<?php //echo base_url('assets/css/responsive.css?v=').microtime()?>"> -->

        <!--General Scripts-->
    <!-- <script src="<?php// echo base_url('assets/js/jquery-3.5.1.slim.min.js')?>"></script> -->

    <script src="<?php echo site_url('assets-admin/vendor/jquery/jquery-3.3.1.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/thether.min.js')?>"></script> 
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
     <script src="<?php echo base_url('assets/js/popper.js')?>"></script>
    <!-- <script src="<?php //echo base_url('assets/js/croppie.js')?>"></script> -->
    <style type="text/css">
    	.beta-section{background-image: url(<?php echo site_url('assets/files/wsbanner.png') ?>); background-repeat: no-repeat; background-position: center; background-size: cover; height: 72vh; width: 100%; margin-top: 14vh}
    	.beta-div{position: absolute; width: 40%; background: rgba(255,255,255,0.5); height: 42vh; overflow-y: scroll;top:29vh; margin-left: 30%}
    	.beta-div h2{text-align: center;font-size: 28px; color: blue;padding-top: 15px}
    	.beta-div p{text-align: center;font-size: 15px; padding: 28px}
    	.beta-div form{text-align: center;}
    	@media only screen and (max-device-width: 360px){
	        .beta-section{margin-top: 0px; height: 100vh;}
	        .beta-div{margin-left: 0px; width: 100%; padding: 0px 140px; top:0vh; height: 100vh; padding-top: 10vh}
	        .beta-div h2{font-size: 62px}
	        .beta-div p{font-size: 36px}
	    }
	    @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
	        .beta-section{margin-top: 0px; height: 100vh;}
	        .beta-div{margin-left: 0px; width: 100%; padding: 0px 140px; top:0vh; height: 100vh; padding-top: 10vh}
	        .beta-div h2{font-size: 62px}
	        .beta-div p{font-size: 36px}
	    }

    </style>
</head>
<body>
	<section class="beta-section" style="">
		
	</section>
	<div class="beta-div" style="">
		<h2 style="">Beta Login</h2>
		<p style="">
			After being in development for the better part of the last two years, we are launching Walledstory in the private beta mode. Registration is based entirely on the basis of invitation. If you have been invited through mail, please enter your password below: 
		</p>
		<form action="<?php echo site_url('beta/beta_login'); ?>" method="POST" style="">
			<input type="password" name="beta_password" required>
			<input type="submit" name="" class="btn btn-primary" value="Beta Login">
		</form>
		<p style="">
			Walledstory respects your privacy. Even as you read this, our developers are trying to ensure that Walledstory is the safest networking website in the world. 
			<br>
			Walledstory's networking algorithm has been designed and developed in India and we take pride in being the first ones to do so.
		</p>
	</div>
	<!-- <script type="text/javascript">
		$(document).ready( function(){

		} );
	</script> -->
</body>
</html>
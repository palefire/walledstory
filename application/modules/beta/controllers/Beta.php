<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beta extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        

	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		if( isset($this->session->userdata['wst23xyzsdfretw89lk_beta_token']) ){
			return redirect(site_url('front'));
		}
		$beta_cookie = get_cookie('ws_beta_cookie');
		if( $beta_cookie == 'sohini' ){
			$this->session->set_userdata( 'wst23xyzsdfretw89lk_beta_token', 'beta_user' );
			return redirect( site_url('front') );
			die();
		}
		$this->load->view('beta');
		
	}//fn

	public function beta_login(){
		$password = $this->input->post('beta_password');
		if( $password == '8961819473' ){
			$this->session->set_userdata( 'wst23xyzsdfretw89lk_beta_token', 'beta_user' );
			set_cookie('ws_beta_cookie', 'sohini', time()+6500);
			return redirect( site_url('front') );
		}else{
			return redirect( site_url('beta') );
		}
	}//fn

	public function kill_beta(){
		$this->session->unset_userdata('wst23xyzsdfretw89lk_beta_token');
		return redirect( site_url());
	}

}//class
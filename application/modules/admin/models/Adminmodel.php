<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Adminmodel extends CI_model {


    /**
     * CONSTRUCTOR FUNCTION
     */
    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }


    /**
     * Checklogin FUNCTION
     */
    public function checklogin( $username, $password){
        $q = $this->db
                    ->where( array('admin_username'=>$username) )
                    ->get('wst_admin');
        $result = $q->result_array();
        if( $q->num_rows() > 0 ){
            $password_db = $result[0]['admin_password'];
            if(password_verify( $password, $password_db)) {
                return $result[0];
            }else{
                return FALSE;
            }

        }else{
            return FALSE; 
        }
    }//fn

     /**
     * Check Remember Me FUNCTION
     */
    public function check_remember_me( $token){
        if( $token != '' ){
            $q = $this->db
                    ->where( array('remember_me_token'=>$token) )
                    ->get('wst_admin');
            $result = $q->result_array();
            if( $q->num_rows() > 0 ){
                return $result[0];
            }else{
                return false;
            }
        }else{
            return false;
        }
       
    }//fn

     /**
     * Create Slug FUNCTION
     */
    public function create_slug( $name ){
        $slug = strtolower($name); 
        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.

        return $slug;
    }//fn

    public function get_admin_by_id( $admin_id ){
        $q = $this->db
                    ->where( array('admin_id'=>$admin_id) )
                    ->get('wst_admin');
        $result = $q->result_array();
            if( $q->num_rows() > 0 ){
                return $result[0];
            }else{
                return false;
            }
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Category function
    |--------------------------------------------------------------------------
    */

    //check if cat slug exists?
    public function check_if_cat_slug_exists( $cat_slug ){
        $q = $this->db
                    ->like('cat_slug', $cat_slug)
                    ->get('wst_category');
        if( $q->num_rows() > 0 ){
            $exist_number = $q->num_rows();
            $cat_slug = $cat_slug.'-'.$exist_number;
            return $cat_slug;
        }else{
            return $cat_slug;
        }
    }//fn

    public function get_cat_list(){
        $q = $this->db
                    ->select('
                        wst_category.cat_id,
                        wst_category.cat_slug,
                        wst_category.cat_name,
                        wst_category.cat_count
                        ')
                    ->order_by("cat_name", 'ASC')
                    ->get('wst_category');
        if( $q->num_rows() > 0 ){
            return $q->result_array();
        }else{
            return false;
        }
    }//fn

    //OTP

    public function get_otp(){
        $q = $this->db
                    ->select('
                        user_registration_otp.user_id,
                        user_registration_otp.otp,
                        user_registration_otp.created_at,
                        user_authentication.user_id,
                        user_authentication.user_email,
                        ')
                    ->join('user_authentication', 'user_registration_otp.user_id = user_authentication.user_id')
                    ->get('user_registration_otp');
        if( $q->num_rows() > 0 ){
            return $q->result_array();
        }else{
            false;
        }
    }//fn

    public function get_post( $limit = null, $offset = null ){
        if( $limit == null ){
            $q = $this->db
                        ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.post_status,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        // ->where( array('post_status' => '1') )
                        ->order_by('post_id', 'ASC')
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }else{
            $q = $this->db
                        ->select('wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.post_status,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        // ->where( array('post_status' => '1') )
                        ->order_by('post_id', 'ASC')
                        ->limit($limit, $offset)
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }
    }//fn

    public function get_post_by_id( $post_id ){
        $q = $this->db
                    ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname, 
                            user_info.user_image,
                            user_info.user_cover_image,
                            user_info.user_bio,
                            user_info.show_image,
                            user_info.show_bio,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                    ->where(array('wst_posts.post_id' => $post_id))
                    ->join('user_info','wst_posts.user_id = user_info.user_id')
                    ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                    ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                    ->get('wst_posts');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0];
        }else{
            return false;
        }

    }//fn

    public function get_all_text_posts(){
        $q = $this->db
                    ->select('
                        wst_posts.post_id,
                        wst_posts.user_id,
                        wst_posts.post_slug,
                        wst_posts.post_title,
                        wst_posts.post_content,
                        wst_posts.post_status,
                        wst_posts.featured_img_url,
                        wst_posts.cat_id,
                        wst_posts.template,
                        wst_posts.post_time,
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_info.user_image,
                        user_info.show_image,
                        user_authentication.user_name,
                        user_authentication.user_token,
                        wst_category.cat_name
                        ')
                    ->where(array('wst_posts.template' => '0'))
                    ->join('user_info','wst_posts.user_id = user_info.user_id')
                    ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                    ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                    // ->where( array('post_status' => '1') )
                    ->order_by('post_id', 'ASC')
                    ->get('wst_posts');
        if( $q->num_rows()>0 ){
            return $q->result_array();
        }else{
            return null;
        }
    }//fn

    public function get_all_image_posts(){
        $q = $this->db
                    ->select('
                        wst_posts.post_id,
                        wst_posts.user_id,
                        wst_posts.post_slug,
                        wst_posts.post_title,
                        wst_posts.post_content,
                        wst_posts.post_status,
                        wst_posts.featured_img_url,
                        wst_posts.cat_id,
                        wst_posts.template,
                        wst_posts.post_time,
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_info.user_image,
                        user_info.show_image,
                        user_authentication.user_name,
                        user_authentication.user_token,
                        wst_category.cat_name
                        ')
                    ->where(array('wst_posts.template' => '1'))
                    ->join('user_info','wst_posts.user_id = user_info.user_id')
                    ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                    ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                    // ->where( array('post_status' => '1') )
                    ->order_by('post_id', 'ASC')
                    ->get('wst_posts');
        if( $q->num_rows()>0 ){
            return $q->result_array();
        }else{
            return null;
        }
    }//fn

    public function get_all_users(){
        $q = $this->db
                    ->select('
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_info.user_image,
                        user_info.show_image,
                        user_info.user_info_phone,
                        user_info.user_id,
                        user_authentication.user_email,
                        user_authentication.user_status
                        ')
                    ->join('user_authentication','user_info.user_id = user_authentication.user_id')
                    ->get('user_info');
        if( $q->num_rows()>0 ){
            return $q->result_array();
        }else{
            return null;
        }
    }//fn

    public function get_user_by_id($userid){
        $q = $this->db
                    ->select('
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_info.user_image,
                        user_info.show_image,
                        user_info.user_info_phone,
                        user_authentication.user_email,
                        user_authentication.user_status
                        ')
                    ->where(array('user_info.user_id' => $userid))
                    ->join('user_authentication','user_info.user_id = user_authentication.user_id')
                    ->get('user_info');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0];
        }else{
            return null;
        }
    }//fn
  
}//class

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        if( !isset($this->session->userdata['wst23xyzsdfretw89lk_admin_id']) ){
			return redirect('admin/login');
			
		}
        
         $this->load->model('adminmodel');
         $this->load->model('front/frontmodel','frontmodel');
         $admin_details = $this->adminmodel->get_admin_by_id( $this->session->userdata['wst23xyzsdfretw89lk_admin_id'] );
         $this->data['admin_details'] = $admin_details;
	}

	/*
	|--------------------------------------------------------------------------
	| Dashboard function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('dashboard');
		$this->load->view('footer');
		
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Posts function
	|--------------------------------------------------------------------------
	*/
	public function all_posts(){
		
		$data['all_posts'] = $this->adminmodel->get_post();
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('posts/all_posts',$data);
		$this->load->view('footer');
		
	}//fn

	public function all_text_posts(){
		
		$data['all_text_posts'] = $this->adminmodel->get_all_text_posts();
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('posts/all_text_posts',$data);
		$this->load->view('footer');
		
	}//fn

	public function all_image_posts(){
		
		$data['all_image_posts'] = $this->adminmodel->get_all_image_posts();
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('posts/all_image_posts',$data);
		$this->load->view('footer');
		
	}//fn

	public function single_posts( $post_id ){
		$data['single_post_by_post_id'] = $this->adminmodel->get_post_by_id($post_id);
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('posts/single_post',$data);
		$this->load->view('footer');
	}//fn

	public function edit_post( $post_id ){
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('posts/edit_post');
		$this->load->view('footer');
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Category function
	|--------------------------------------------------------------------------
	*/
	public function all_cat(){
		$all_cat = $this->adminmodel->get_cat_list();
		// print_r($all_cat);
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('category/all_cat', array(
			'cat_list' => $all_cat,
		));
		$this->load->view('footer');
	}//fn

	public function add_cat_to_db(){
		$cat_name = $this->input->post('cat_name');
		// echo $cat_name;
		$cat_slug_raw = $this->adminmodel->create_slug($cat_name);
		$cat_slug = $this->adminmodel->check_if_cat_slug_exists($cat_slug_raw);
		// echo $cat_slug;	
		$data = array(
			'cat_slug' 			=> $cat_slug,
			'cat_name'			=> $cat_name,
			'cat_count'			=> 0,
			'cat_created_by'	=> $this->session->userdata['wst23xyzsdfretw89lk_admin_id']
		);
		$insert = $this->db->insert('wst_category', $data);
		if( $insert ){
			$this->session->set_flashdata( 'cat_addition_success' , 'Category added succesfully' );
			return redirect('admin/all_cat');
		}else{
			$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot insert !' );
			return redirect('admin/all_cat');
		}
	}//fn

	public function edit_cat_in_db(){
		$cat_name = $this->input->post('cat_name');
		$cat_id = $this->input->post('cat_id');
		$data = array(
			'cat_name'			=> $cat_name,
		);
		$update = $this->db->update('wst_category',$data, array('cat_id'=> $cat_id));
		if( $update ){
			$mod_data = array(
				'cat_id'		=> $cat_id,
				'modified_by'	=> $this->session->userdata['wst23xyzsdfretw89lk_admin_id'],
			);
			$insert = $this->db->insert('wst_category_modification', $mod_data);
			$this->session->set_flashdata( 'cat_addition_success' , 'Category updated succesfully' );
			return redirect('admin/all_cat');
		}else{
			$this->session->set_flashdata( 'insert_service_error' , 'Database error ! Cannot edit !' );
			return redirect('admin/all_cat');
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Users function
	|--------------------------------------------------------------------------
	*/
	public function all_users(){
		
		$data['all_users'] = $this->adminmodel->get_all_users();
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('users/all_users',$data);
		$this->load->view('footer');
		
	}//fn

	public function single_user( $userid ){

		$data['single_user_by_user_id'] = $this->adminmodel->get_user_by_id($userid);
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('users/single_user',$data);
		$this->load->view('footer');
	}

	/*
	|--------------------------------------------------------------------------
	| OTP function
	|--------------------------------------------------------------------------
	*/

	public function OTP(){
		$all_otp = $this->adminmodel->get_otp();
		// print_r($all_otp);

		
		$this->load->view('header', array(
			'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('SEO/otp', array(
			'all_otp' => $all_otp,
		));
		$this->load->view('footer');
	}//fn

	

}//class
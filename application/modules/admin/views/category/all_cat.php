
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">All Categories</h2>
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url('admin/')  ?>" class="breadcrumb-link">Dashboard</a></li>
                                
                                <li class="breadcrumb-item active" aria-current="page">All Categories</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        
        <div class="row">
            <!-- ============================================================== -->
            <!-- data table  -->
            <!-- ============================================================== -->
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Data Tables - Print, Excel, CSV, PDF Buttons</h5>
                        <p>This example shows DataTables and the Buttons extension being used with the Bootstrap 4 framework providing the styling.</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered second" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Cat Nam</th>
                                        <th>Slug</th>
                                        <th>Count</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if( $cat_list ){
                                            foreach($cat_list as $cat){
                                    ?>
                                                <tr>
                                                    <td><?php echo $cat['cat_id'] ?></td>
                                                    <td><a href="#" data-bs-toggle="modal" data-bs-target="#categorymodal" data-bs-id="<?php echo $cat['cat_id'] ?>" data-bs-name="<?php echo $cat['cat_name'] ?>"   ><?php echo $cat['cat_name'] ?></a></td>
                                                    <td><?php echo $cat['cat_slug'] ?></td>
                                                    <td><?php echo $cat['cat_count'] ?></td>
                                                </tr>
                                    <?php
                                            }//if 
                                        }//foreach
                                    ?>

                           
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>ID</th>
                                        <th>Cat Nam</th>
                                        <th>Slug</th>
                                        <th>Count</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">

                <div class="card">
                    <?php 
                        $cat_addition_success = $this->session->flashdata('cat_addition_success');
                        if( $cat_addition_success ){
                    ?>
                            <div class="card-body border-top">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>Great!</strong> <?php echo $cat_addition_success; ?>
                                    <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </a>
                                </div>
                            </div>
                    <?php 
                        } 
                    ?>
                    <h5 class="card-header">Add Category</h5>
                    <div class="card-body">
                        <form action="<?php echo site_url('admin/add_cat_to_db')  ?>" method="post"> 
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Name</label>
                                <input id="inputText3" type="text" class="form-control" name="cat_name">
                            </div>
                            <button class="btn btn-primary btn-sm" type="submit">Add Category</button>
                        </form>
                    </div>
                    
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end data table  -->
            <!-- ============================================================== -->
        </div>     
    </div>  



<div class="modal fade" id="categorymodal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="<?php echo site_url('admin/edit_cat_in_db')  ?>" method="post"> 
          <div class="modal-body">
            
              <div class="mb-3">
                <label for="recipient-name" class="col-form-label">Name:</label>
                <input type="text" class="form-control" id="cat-name-modal" name="cat_name">
                <input type="hidden" class="form-control" id="cat-id-modal" name="cat_id">
              </div>
              
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Edit</button>
          </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
    var categorymodal = document.getElementById('categorymodal');
    categorymodal.addEventListener('show.bs.modal', function (event) {
        // Button that triggered the modal
        var button = event.relatedTarget;
        // Extract info from data-bs-* attributes
        var cat_id = button.getAttribute('data-bs-id');
        var cat_name = button.getAttribute('data-bs-name');
        // alert(cat_name);
        $('#cat-id-modal').val(cat_id);
        $('#cat-name-modal').val(cat_name);
        // If necessary, you could initiate an AJAX request here
        // and then do the updating in a callback.
        //
        // Update the modal's content.
        var modalTitle = categorymodal.querySelector('.modal-title');
        modalTitle.textContent = 'Edit ' + cat_name;
        // cat_id.value = cat_id
    })
</script>
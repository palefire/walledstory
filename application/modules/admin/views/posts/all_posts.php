<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">All Posts</h2>
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url('admin/')  ?>" class="breadcrumb-link">Dashboard</a></li>
                                
                                <li class="breadcrumb-item active" aria-current="page">All Posts</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        
        <div class="row">
            <!-- ============================================================== -->
            <!-- data table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Data Tables - Print, Excel, CSV, PDF Buttons</h5>
                        <p>This example shows DataTables and the Buttons extension being used with the Bootstrap 4 framework providing the styling.</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered second" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Template</th>
                                        <th>Author</th>
                                        <th>Status</th>
                                        <th>Likes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $CI =& get_instance();
                                    $CI->load->model('front/frontmodel','frontmodel');
                                        foreach($all_posts as $posts){
                                            $like_count = $CI->frontmodel->like_count( $posts['post_id'] );
                                           //echo '<pre>'; print_r($all_posts);die();
                                    ?>
                                    <tr>
                                        <td><?php echo $posts['post_id']; ?></td>
                                        <td><a href="<?php echo site_url('/admin/single_posts/').$posts['post_id'] ?>"><?php echo $posts['post_title']; ?></a></td>
                                        <td><?php echo $posts['cat_name']; ?></td>
                                        <td><?php if($posts['template']==0){echo "WS TEXT";}else if($posts['template']==1){echo "WS IMAGE";}else{echo "WS YOUTUBE";}  ?></td>
                                        <td><?php echo $posts['user_info_fname']." ".$posts['user_info_lname']; ?></td>
                                        <?php
                                            if($posts['post_status']==0){
                                        ?>
                                            <td><span class="badge badge-warning">Draft</span></td>
                                        <?php
                                            }else if($posts['post_status']==1){
                                        ?>
                                            <td><span class="badge badge-success">Live</span></td>
                                        <?php
                                            }else{
                                        ?>
                                            <td><span class="badge badge-danger">Reject</span></td>
                                        <?php
                                            }
                                        ?>
                                        <td><?php echo $like_count; ?></td>
                                    </tr>
                                    <?php
                                        }
                                    ?>       
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end data table  -->
            <!-- ============================================================== -->
        </div>     
    </div>  

 <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content">
                <div class="row">
                    <div class="col-xl-12">
                        <!-- ============================================================== -->
                        <!-- pageheader  -->
                        <!-- ============================================================== -->
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="page-header" id="top">
                                    <h2 class="pageheader-title">Form Elements </h2>
                                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                    <div class="page-breadcrumb">
                                        <nav aria-label="breadcrumb">
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                                <li class="breadcrumb-item"><a href="<?php echo site_url('admin/all_posts'); ?>" class="breadcrumb-link">All Posts</a></li>
                                                <li class="breadcrumb-item"><a href="<?php echo site_url('/admin/single_posts/'). 1 ?>" class="breadcrumb-link">The Indian downfall in the 20th Century.</a></li>
                                                <li class="breadcrumb-item active" aria-current="page">Edit Post</li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- ============================================================== -->
                        <!-- basic form  -->
                        <!-- ============================================================== -->
                        <div class="row">
                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                                <div class="section-block" id="basicform">
                                    <h3 class="section-title">Edit Posts</h3>
                                    
                                </div>
                                <div class="card">
                                    <h5 class="card-header">Edit</h5>
                                    <div class="card-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="inputText3" class="col-form-label">Title</label>
                                                <input id="inputText3" type="text" class="form-control" value="The Indian downfall in the 20th Century.">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Content</label>
                                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="30" >
                                                    “We are also fortunate that Eoin Morgan, the 2019 World Cup-winning captain, who has been the vice-captain, is willing to lead the side going forward. Dinesh Karthik and Eoin have worked brilliantly together during this tournament and although Eoin takes over as captain, this is effectively a role swap and we expect that this transition will work in a seamless manner.

                                                    “On behalf of everyone at Kolkata Knight Riders, we thank DK for all his contributions as the captain over the past two and a half years and wish Eoin the very best going forward,” he added.

                                                    Also read- Wimbledon Championships 2020 cancelled for the first time since World War II.
                                                    Meanwhile, Morgan, who was named as KKR’s vice-captain at the start of this IPL, led England to their maiden 50-over World Cup triumph last year. Morgan also powered England to the 2016 T20 WC final where they came runner up against West Indies. He was picked up by KKR in the 2019 auction for INR 5.25 crore. Incidentally, Morgan had previously played for KKR for three seasons – 2011 to 2013.
                                                </textarea>
                                            </div>
                                        </form>
                                    </div>
                                   
                                </div>
                            </div>

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                <div class="section-block" id="select">
                                    <h3 class="section-title">Extra</h3>
                                    <p>Category, Template, Author, Tags, Status, Date, Image</p>
                                </div>
                                <div class="card">
                                    <h5 class="card-header">Post Settings</h5>
                                    <div class="card-body">
                                        <form>
                                            <div class="form-group">
                                                <label for="input-select">Status : </label>
                                                <span class="badge badge-success">Live</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="input-select">Date : </label>
                                                <span class="badge badge-info">22/05/2019 21:32:56</span>
                                            </div>
                                            <div class="form-group">
                                                <label for="input-select">Author</label>
                                                <select class="form-control" id="input-select">
                                                    <option>Select Author</option>
                                                    <option selected>Sarasij Roy</option>
                                                    <option>Sk Sakir Ali</option>
                                                    <option>Saikat Ghorai</option>
                                                    <option>Amartya Karmakar</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="input-select">Category</label>
                                                <select class="form-control" id="input-select">
                                                    <option>Select Category</option>
                                                    <option>No Category</option>
                                                    <option selected>Polictics</option>
                                                    <option>Nation</option>
                                                    <option>Short Stories</option>
                                                    <option>Poem</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="input-select">Template</label>
                                                <select class="form-control" id="input-select">
                                                    <option>WS Blog</option>
                                                    <option>WS Image</option>
                                                    <option selected>WS Video</option>
                                                    
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="input-select">Featured Image</label>
                                            </div>
                                            <div class="form-group">
                                                <img style="width: 100%" src="<?php echo base_url('assets-admin/img/ws-jpeg.jpg') ?>" />
                                            </div>
                                            <div class="custom-file mb-3">
                                                <input type="file" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Select Image</label>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end basic form  -->
                        <!-- ============================================================== -->
                        
                        
                    </div>
                    
                </div>
            
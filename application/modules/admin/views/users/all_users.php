<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">All Users</h2>
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo site_url('admin/')  ?>" class="breadcrumb-link">Dashboard</a></li>
                                
                                <li class="breadcrumb-item active" aria-current="page">All Users</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        
        <div class="row">
            <!-- ============================================================== -->
            <!-- data table  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Data Tables - Print, Excel, CSV, PDF Buttons</h5>
                        <p>This example shows DataTables and the Buttons extension being used with the Bootstrap 4 framework providing the styling.</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered second" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Template</th>
                                        <th>Author</th>
                                        <th>Status</th>
                                        <th>Views</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($all_users as $allUsers){
                                    ?>
                                    <tr>
                                        <td><?php echo $allUsers['user_id']; ?></td>
                                        <td><a href="<?php echo site_url('/admin/single_user/').$allUsers['user_id'] ?>"><?php echo $allUsers['user_info_fname']." ".$allUsers['user_info_lname']; ?></a></td>
                                        <td>Politics</td>
                                        <td>WS Blog</td>
                                        <td>Sarasij Roy</td>
                                        <?php
                                            if($allUsers['user_status']==0){
                                        ?>
                                            <td><span class="badge badge-warning">Unverified</span></td>
                                        <?php
                                            }else if($allUsers['user_status']==1){
                                        ?>
                                            <td><span class="badge badge-success">Live</span></td>
                                        <?php
                                            }else if($allUsers['user_status']==5){
                                        ?>
                                            <td><span class="badge badge-danger">Block</span></td>
                                        <?php
                                            }else{
                                        ?>
                                            <td><span class="badge badge-dark">Deactivated</span></td>
                                        <?php
                                            }
                                        ?>
                                        <td>33</td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end data table  -->
            <!-- ============================================================== -->
        </div>     
    </div>  

	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        $this->apiuser = 'sarasij94';
        $this->apipass = '123';
        $this->response_text = array(0 => 'Fail', 1 => 'Success', 9 => 'Authentication Error', 404 => 'Page Not Found!');

        $this->load->model('admin/adminmodel');
        $this->load->model('front/frontmodel');
      
	}//constructor

	/*
	|--------------------------------------------------------------------------
	| Login Api
	|--------------------------------------------------------------------------
	*/
	public function login(){
		//1->Login Success, 5->email not verified, 0->authentication error
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$email 	= $all_data->email;
			$password 	= $all_data->password;
			$login_success = $this->frontmodel->checklogin( $email, $password);
			// print_r($login_success);
			if( $login_success ){
				if( $login_success['user_status'] == '0' ){
					$array = array(
						"code"	 		=> 5,
						"message" 		=> 'Email not verified',
						"user_id"		=> $login_success['user_id'],
						"user_token"	=> $login_success['user_token'],
						"show_name" 	=> $login_success['show_name'],
						"user_fname" 	=> $login_success['user_info_fname'],
						"user_lname" 	=> $login_success['user_info_lname'],
						"user_img" 		=> $login_success['user_image'],
					);
					echo json_encode($array);
				}else{
					$array = array(
						"code"	 		=> 1,
						"message" 		=> 'Login Success',
						"user_id"		=> $login_success['user_id'],
						"user_token"	=> $login_success['user_token'],
						"show_name" 	=> $login_success['show_name'],
						"user_fname" 	=> $login_success['user_info_fname'],
						"user_lname" 	=> $login_success['user_info_lname'],
						"user_img" 		=> $login_success['user_image'],
					);
					echo json_encode($array);
				}

			}else{
				$array = array(
					"code" => 0,
					"message" => 'Login fail'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Signup Api
	|--------------------------------------------------------------------------
	*/
	public function generate_user_token(  ){
        $user_token = substr(str_shuffle(md5(time())), 0, 32);;
        $q = $this->db
                    ->where( array('user_token'=>$user_token) )
                    ->get('user_authentication');
        if( $q->num_rows() > 0 ){
            $this->generate_user_token();
        }else{
            return $user_token;
        }
    }//fn
	public function signup(){
		//1->Registration Success, 4->Username Exists, 5->Email Exists, 0->DB error, 9-> Authenticaion error
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$email 		= $all_data->email;
			$password 	= $all_data->password;
			$username 	= $all_data->username;
			$phone 	= $all_data->phone;
			$qcheckusername = $this->db
                    ->select('user_authentication.user_id')
                    ->where( array('user_name'=>$username) )
                    ->get('user_authentication');
            if( $qcheckusername->num_rows() > 0 ){
            	$array = array(
					"code" => 4,
					"message" => 'Username exists'
				);
				echo json_encode($array);
            }else{
            	$qcheckemail = $this->db
                        ->select('user_authentication.user_id')
                        ->where( array('user_email'=>$email) )
                        ->get('user_authentication');
                if( $qcheckemail->num_rows() > 0 ){
	                $array = array(
						"code" => 5,
						"message" => 'Email exists'
					);
					echo json_encode($array);
	            }else{
	            	$user_token = $this->generate_user_token();
	            	$data1 = array(
	                    'user_token'        =>$user_token,
	                    'user_slug'         =>$user_token,
	                    'user_name'         =>$username,
	                    'user_password'     =>password_hash($password, CRYPT_BLOWFISH),
	                    'user_email'        =>$email,
	                    'user_status'       =>'0'
	                );
	                $insert1 = $this->db->insert('user_authentication', $data1);
                	$user_id = $this->db->insert_id();
                	if( $insert1 ){
                    $data2 = array(
                        'user_id'       	=> $user_id,
                        'user_token'    	=> $user_token,
                        'user_info_phone'   => $phone,
                    );
                    $insert2 = $this->db->insert('user_info', $data2);
                    if( $insert2 ){
                    	$data3 = array(
	                        'user_id'       	=> $user_id,
	                        'user_token'    	=> $user_token,
	                    );
                        $insert3 = $this->db->insert('user_extra', $data3);
                        if( $insert3 ){
                            
                            $data4 = array(
                                'user_id'       => $user_id,
                                'user_token'    => $user_token,
                            );
                            $data6 = array(
                                'user_id'       => $user_id,
                                'user_token'    => $user_token,
                            );
                            $insert4 = $this->db->insert('user_web_device_info', $data4);
                            $insert6 = $this->db->insert('user_mobile_device_info', $data6);
                            if( $insert4 AND $insert6){
                                $otp = rand(100000,999999);

                                $this->load->library('email');
                                //SMTP & mail configuration
                                $config = array(
                                    'protocol'  => 'smtp',
                                    'smtp_host' => 'ssl://smtp.googlemail.com',
                                    'smtp_port' => 465,
                                    'smtp_user' => 'palefire2019@gmail.com',
                                    'smtp_pass' => '1sarasij2roy',
                                    'mailtype'  => 'html',
                                    'charset'   => 'utf-8'
                                );
                                $this->email->initialize($config);
                                $this->email->set_mailtype("html");
                                $this->email->set_newline("\r\n");

                                $this->email->to(strval($email));
                                $this->email->from('palefire2019@gmail.com','Walledstory');
                                $list = array('sarasij94@gmail.com', 'sarasij@thepalefire.com');
                                $this->email->cc($list);
                                $this->email->subject('OTP');
                                $this->email->message( $otp );

                                if(!$this->email->send()){
                                    // print_r($this->email->print_debugger());
                                }else{
                                    // echo  1;
                                }
                                $otp_data = array(
                                    'user_id'       => $user_id,
                                    'user_token'    => $user_token,
                                    'otp'           => $otp,
                                );
                                $insert5 = $this->db->insert('user_registration_otp', $otp_data);
                                if( $insert5 ){
                                	$this->sendotp($phone, $otp);
                                   	$array = array(
										"code" => 1,
										"message" => 'Registration Succesful',
										'user_id'	=>$user_id,
										// "otp"	=> $otp
									);
                                   echo json_encode($array);
                                }else{
                                    $array = array(
										"code" => 0,
										"message" => 'Error'
									);
									echo json_encode($array);
                                }
                                
                            }
                        }
                    }
                }
	            }
            }
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function update_device_info(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 			= $all_data->user_id;
			$device_id 			= $all_data->device_id;
			$firebase_token 	= $all_data->firebase_token;
			$mac_address 		= $all_data->mac_address;
			$appVersion 		= $all_data->appVersion;
			$apiLevel 			= $all_data->apiLevel;
			$manufacturer 		= $all_data->manufacturer;
			$model 				= $all_data->model;
			$versionRelease 	= $all_data->versionRelease;

			$data = array(
				'device_id' 		=> $device_id,
				'firebase_token' 	=> $firebase_token,
				'mac_address'		=> $mac_address,
				'appVersion'		=> $appVersion,
				'apiLevel'			=> $apiLevel,
				'manufacturer'		=> $manufacturer,
				'model'				=> $model,
				'versionRelease'	=> $versionRelease,
			);
			$update = $this->db->update('user_mobile_device_info', $data, array('user_id' => $user_id));

			if( $update ){
				$array = array(
					"code" => 1,
					"message" => 'Device detail updated Succesfullly'
				);
				echo json_encode($array);
			}else{
				$array = array(
					"code" => 0,
					"message" => 'DB Error.'
				);
				echo json_encode($array);
			}

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Check if username exist Api
	|--------------------------------------------------------------------------
	*/
	public function check_username_exists(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$username 	= $all_data->username;
			$check_result 	= $this->frontmodel->check_username_exist( $username );
			if( $check_result ){
				$array = array(
					"code" => 1,
					"message" => 'Username exists!'
				);
				echo json_encode($array);
			}else{
				$array = array(
					"code" => 0,
					"message" => 'Username does not exists!'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Check if email exist Api
	|--------------------------------------------------------------------------
	*/
	public function check_email_exists(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$email 	= $all_data->email;
			$check_result 	= $this->frontmodel->check_email_exist( $email );
			if( $check_result ){
				$array = array(
					"code" => 1,
					"message" => 'Email exists!'
				);
				echo json_encode($array);
			}else{
				$array = array(
					"code" => 0,
					"message" => 'Email does not exists!'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	
	/*
	|--------------------------------------------------------------------------
	| Verify Otp Api
	|--------------------------------------------------------------------------
	*/
	public function verify_otp(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$otp 	= $all_data->otp;
			$user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$profile_info = $this->frontmodel->get_user_by_token($user_token);
			$verify_email = $this->frontmodel->verify_email( $user_token, $otp );
			if($verify_email){
				$array = array(
					"code" => 1,
					"message" => 'Otp verified succesfully!',
					// 'user_details'=> $profile_info,
					"user_id"		=> $profile_info['user_id'],
					"user_token"	=> $user_token,
					"show_name" 	=> $profile_info['show_name'],
					"user_fname" 	=> $profile_info['user_info_fname'],
					"user_lname" 	=> $profile_info['user_info_lname'],
					"user_img" 		=> $profile_info['user_image'],
				);
				echo json_encode($array);
			}else{
				$array = array(
					"code" => 0,
					"message" => 'Otp wrong!'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Save Profile Info while registration Api
	|--------------------------------------------------------------------------
	*/
	public function save_profile_info_signup(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$user_token = $this->frontmodel->get_user_token_by_id( $user_id );

			$user_info_address 	= $all_data->user_address;
			$user_info_city 	= $all_data->user_city;
			$user_info_state 	= $all_data->user_state;
			$user_info_zip 		= $all_data->user_zip;
			// $user_info_phone 	= $all_data->user_phone;
			$user_info_country 	= $all_data->user_country;
			$fname 				= $all_data->user_fname;
			$lname 				= $all_data->user_lname;
			$user_info_gender 	= $all_data->user_gender;
			$data = array(
				'user_info_address' => $user_info_address,
				'user_info_city' 	=> $user_info_city,
				'user_info_state'	=> $user_info_state,
				'user_info_zip'		=> $user_info_zip,
				// 'user_info_phone'	=> $user_info_phone,
				'user_info_country'	=> $user_info_country,
				'user_info_fname'	=> $fname,
				'user_info_lname'	=> $lname,
				'user_info_gender'	=> $user_info_gender,
			);
			$insert = $this->frontmodel->save_extra_info_api( $data, $user_token );
			if( $insert ){
				$array = array(
					"code" => 1,
					"message" => 'Info updated succesfully'
				);
				echo json_encode($array);
			}else{
				$array = array(
					"code" => 0,
					"message" => 'Error'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Save profileimage nad cover image Api
	|--------------------------------------------------------------------------
	*/
	public function save_profile_and_cover_image(){
		// $all_data_json = file_get_contents('php://input');
		$all_data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		if (!isset($all_data['apiuser']) || !isset($all_data['apipass'])) {
            $return_res = array('code' => 9, 'message' => $this->response_text[9]);
            echo json_encode($return_res);
            exit();
        }elseif( $all_data['apiuser'] != $this->apiuser OR $all_data['apipass'] != $this->apipass ){
        	$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
        }else{
        	$user_id 	= $all_data['user_id'];
        	$profile_img_upload_error_msg = 'Profile picture upload not used.';
        	$cover_img_upload_error_msg = 'Cover picture upload not used';
        	//upload profile image
			if( isset($_FILES["profile_image"]["name"]) ){
				$config['upload_path']          = './uploads/profile/';
	            $config['allowed_types']        = 'gif|jpg|png|jpeg';
	            $config['max_size']             = 2000;
	            $config['max_width']            = 5000;
	            $config['max_height']           = 3000;
	            $config['file_name']           	= time().'.jpeg';
	            $this->load->library('upload', $config);
	            if ( !$this->upload->do_upload('profile_image')){
	    //         	$array = array(
					// 	'code' 		=> 0,
					// 	'message' 	=> strip_tags( $this->upload->display_errors() ),
					// );
					// echo json_encode($array);
					// die();
					$pro_img_upload_status = 0;
					$profile_img_upload_error_msg = strip_tags( $this->upload->display_errors() );
	            }else{
	            	$uploaded_data =  $this->upload->data();
	            	$gallery_data = array(
						'user_id'	=> $user_id,
						'file_name' => $uploaded_data["file_name"],
						'file_url' 	=> site_url('uploads/profile/').$uploaded_data["file_name"],
						'file_size' 	=> $uploaded_data["file_size"],
						'gallery_type' 	=> '0',
					);
					$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
					$gallery_id = $this->db->insert_id();
					$profile_img_url = site_url('uploads/profile/').$uploaded_data["file_name"];
					$update = $this->db->update('user_info',array('user_image'=>$profile_img_url),array('user_id'=>$user_id));
					$pro_img_upload_status = 1;
					$profile_img_upload_error_msg = "Success";
	            }
			}else{
				$pro_img_upload_status = 9;
			}

			//upload cover image
			if( isset($_FILES["cover_image"]["name"]) ){
				$config1['upload_path']          = './uploads/cover/';
	            $config1['allowed_types']        = 'gif|jpg|png|jpeg';
	            $config1['max_size']             = 2000;
	            $config1['max_width']            = 5000;
	            $config1['max_height']           = 3000;
	            $config1['file_name']            = time().'.jpeg';
	            if( $pro_img_upload_status == 9 ){
	            	$this->load->library('upload', $config1);
	            }else{
	            	$this->upload->initialize( $config1 );
	            }
	            
	            if ( !$this->upload->do_upload('cover_image')){
	    //         	$array = array(
					// 	'code' 		=> 0,
					// 	'message' 	=> strip_tags( $this->upload->display_errors() ),
					// );
					// echo json_encode($array);
					// die();
					$cover_img_upload_status = 0;
					$cover_img_upload_error_msg = strip_tags( $this->upload->display_errors() );
	            }else{
	            	$uploaded_data1 =  $this->upload->data();
	            	$gallery_data1 = array(
						'user_id'	=> $user_id,
						'file_name' => $uploaded_data1["file_name"],
						'file_url' 	=> site_url('uploads/cover/').$uploaded_data1["file_name"],
						'file_size' 	=> $uploaded_data1["file_size"],
						'gallery_type' 	=> '2',
					);
					$insert_gallery1 = $this->db->insert('wst_gallery', $gallery_data1);
					$gallery_id1 = $this->db->insert_id();
					$cover_img_url = site_url('uploads/cover/').$uploaded_data1["file_name"];
					$update = $this->db->update('user_info',array('user_cover_image'=>$cover_img_url),array('user_id'=>$user_id));
					$cover_img_upload_status = 1;
					$cover_img_upload_error_msg = "Success";
	            }
			}else{
				$cover_img_upload_status = 9;
			}



			if( $pro_img_upload_status == 1 and $cover_img_upload_status == 1){
				$array = array(
					"code" => 1,
					"message" => 'Profile Image and cover image updated succesfully',
					"profile_url" => str_replace('localhost','192.168.0.99',$profile_img_url),
					"cover_url" => str_replace('localhost','192.168.0.99',$cover_img_url),
				);
				echo json_encode($array);
			}

			if( $pro_img_upload_status == 1 and $cover_img_upload_status == 9){
				$array = array(
					"code" => 1,
					"message" => 'Profile Image updated succesfully',
					"profile_url" => str_replace('localhost','192.168.0.99',$profile_img_url),
				);
				echo json_encode($array);
			}
			if( $cover_img_upload_status == 1 and $pro_img_upload_status == 9){
				$array = array(
					"code" 		=> 1,
					"message" 	=> 'Cover Image updated succesfully',
					"cover_url" => str_replace('localhost','192.168.0.99',$cover_img_url),
				);
				echo json_encode($array);
			}
			if( $pro_img_upload_status == 0 or $cover_img_upload_status == 0){
				$array = array(
					"code"				=> 0,
					"profile_message" 	=> $profile_img_upload_error_msg,
					"cover_message" 	=> $cover_img_upload_error_msg,

				);
				echo json_encode($array);
			}
        }

	}//fn

	/*
	|--------------------------------------------------------------------------
	| Profile Information Api
	|--------------------------------------------------------------------------
	*/
	public function profile(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$user_token = $this->frontmodel->get_user_token_by_id( $user_id );

			$profile_info = $this->frontmodel->get_user_by_token($user_token);
			$array = array(
				"code" => 1,
				"message" => "Success",
				"data"	=> $profile_info
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function other_profile(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_token 	= $all_data->user_token;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );

			$profile_info = $this->frontmodel->get_user_by_token_without_id($user_token);
			$array = array(
				"code" => 1,
				"message" => "Success",
				"data"	=> $profile_info
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function edit_profile(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 			= $all_data->user_id;
			$user_info_fname 	= $all_data->user_fname;
			$user_info_lname 	= $all_data->user_lname;
			$show_name 			= $all_data->show_name;
			$user_info_gender 	= $all_data->user_gender;
			$show_gender 		= $all_data->show_gender;
			$user_bio 			= $all_data->user_bio;
			$show_bio 			= $all_data->show_bio;
			$show_image 		= $all_data->show_image;

			$show_address 		= $all_data->show_address;
			$user_info_phone 	= $all_data->user_phone;
			$user_info_address 	= $all_data->user_address;
			$user_info_city 	= $all_data->user_city;
			$user_info_state 	= $all_data->user_state;
			$user_info_zip 		= $all_data->user_zip;
			$user_info_country 	= $all_data->user_country;
			$user_profession 	= $all_data->user_profession;
			$show_profession 	= $all_data->show_profession;

			$data = array(
				'user_info_fname' 		=> $user_info_fname,
				'user_info_lname' 		=> $user_info_lname,
				'show_name' 			=> $show_name,
				'user_info_gender' 		=> $user_info_gender,
				'show_gender' 			=> $show_gender,
				'user_bio' 				=> $user_bio,
				'show_bio' 				=> $show_bio,
				'show_address' 			=> $show_address,
				'user_info_phone' 		=> $user_info_phone,
				'user_info_address' 	=> $user_info_address,
				'user_info_city' 		=> $user_info_city,
				'user_info_state' 		=> $user_info_state,
				'user_info_zip' 		=> $user_info_zip,
				'user_info_country' 	=> $user_info_country,
				'user_profession'		=> $user_profession,
				'show_profession'		=> $show_profession,
				'show_image'			=> $show_image,
			);
			$update = $this->db->update('user_info', $data, array('user_id' => $user_id));
			if($update){
				$array = array(
					"code" => 1,
					"message" => 'Profile updated succesfully!'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Blog Api's
	|--------------------------------------------------------------------------
	*/
	public function home_blog(){
		/*
		"post_id": "19",
        "user_id": "1",
        "post_slug": "test-again",
        "post_title": "test againas",
        "post_content": "<p>lal la ga ga</p>",
        "featured_img_url": "http://localhost/walledstory/uploads/post/1614499418.jpeg",
        "cat_id": "1",
        "template": "0",
        "post_time": "2021-02-28 00:54:11",
        "user_info_fname": "Sarasij",
        "user_info_lname": "Roy",
        "user_image": "http://localhost/walledstory/uploads/profile/1613288505.png",
        "show_image": "1",
        "show_name": "1",
        "user_name": "sarasij94",
        "user_token": "31eeb344a5adff77f9b60f6ac41f5f3b",
        "cat_name": "Psychology"
        */
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$per_page 	= $all_data->per_page;
			$offset 	= $all_data->offset;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$posts = $this->frontmodel->get_post($per_page, $offset);
			$post_data_counter = 0;
			$post_data = array();
			if($posts){
				foreach( $posts as $post ){
					$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
	            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
	            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
					$post_data[$post_data_counter] = array(
						'post_id'			=> $post['post_id'],
						'post_title'		=> $post['post_title'],
						'post_content'		=> $post['post_content'],
						'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
						'post_time'			=> strtotime($post['post_time'])*1000,
						'cat'				=> $post['cat_name'],
						'author_token'		=> $post['user_token'],
						'author_fname'		=> $post['user_info_fname'],
						'author_lname'		=> $post['user_info_lname'],
						'author_username'	=> $post['user_name'],
						'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
						'show_authorname'	=> $post['show_name'],
						'show_authorimg'	=> $post['show_image'],
						'check_like'		=> $check_like,
						'like_count'		=> $like_count,
						'check_favourite'	=> $check_favourite,
						'slug'				=> $post['post_slug'],
						'template'			=> $post['template']
					);
					$post_data_counter++;
				}
			}else{
				$post_data = array();
			}

			$popular_posts = $this->frontmodel->get_popular_posts();
			$popular_post_data_counter = 0;
			$popular_post_data = array();
			if($popular_posts){
				foreach( $popular_posts as $popular_post ){
					$popular_check_like = $this->frontmodel->check_like( $popular_post['post_id'], $user_id );
	            	$popular_like_count = $this->frontmodel->like_count( $popular_post['post_id'] );
	            	$popular_check_favourite = $this->frontmodel->check_favourite( $popular_post['post_id'], $user_id );
					$popular_post_data[$popular_post_data_counter] = array(
						'post_id'			=> $popular_post['post_id'],
						'post_title'		=> $popular_post['post_title'],
						'post_content'		=> $popular_post['post_content'],
						'post_img_url'		=> str_replace('localhost','192.168.0.99',$popular_post['featured_img_url']),
						'post_time'			=> strtotime($popular_post['post_time'])*1000,
						'cat'				=> $popular_post['cat_name'],
						'author_token'		=> $popular_post['user_token'],
						'author_fname'		=> $popular_post['user_info_fname'],
						'author_lname'		=> $popular_post['user_info_lname'],
						'author_username'	=> $popular_post['user_name'],
						'author_image'		=> str_replace('localhost','192.168.0.99',$popular_post['user_image']),
						'show_authorname'	=> $popular_post['show_name'],
						'show_authorimg'	=> $popular_post['show_image'],
						'check_like'		=> $popular_check_like,
						'like_count'		=> $popular_like_count,
						'check_favourite'	=> $popular_check_favourite,
						'slug'				=> $popular_post['post_slug'],
						'template'			=> $popular_post['template']
					);
					$popular_post_data_counter++;
				}
			}else{
				$popular_post_data = array();
			}
			$array = array(
				"code" => 1,
				"message" => "Success",
				"posts"	=> $post_data,
				"popular_posts"	=> $popular_post_data
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function shorts_blog(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$per_page 	= $all_data->per_page;
			$offset 	= $all_data->offset;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$posts = $this->frontmodel->get_shorts_post($per_page, $offset);
			$post_data_counter = 0;
			$post_data = array();
			if($posts){
				foreach( $posts as $post ){
					$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
	            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
	            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
					$post_data[$post_data_counter] = array(
						'post_id'			=> $post['post_id'],
						'post_title'		=> $post['post_title'],
						'post_content'		=> $post['post_content'],
						'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
						'post_time'			=> strtotime($post['post_time'])*1000,
						'cat'				=> $post['cat_name'],
						'author_token'		=> $post['user_token'],
						'author_fname'		=> $post['user_info_fname'],
						'author_lname'		=> $post['user_info_lname'],
						'author_username'	=> $post['user_name'],
						'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
						'show_authorname'	=> $post['show_name'],
						'show_authorimg'	=> $post['show_image'],
						'check_like'		=> $check_like,
						'like_count'		=> $like_count,
						'check_favourite'	=> $check_favourite,
						'slug'				=> $post['post_slug'],
						'template'			=> $post['template']
					);
					$post_data_counter++;
				}
			}else{
				$post_data = array();
			}
			$array = array(
				"code" => 1,
				"message" => "Success",
				"posts"	=> $post_data
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function shots_blog(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$per_page 	= $all_data->per_page;
			$offset 	= $all_data->offset;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$posts = $this->frontmodel->get_shots_post($per_page, $offset);
			$post_data_counter = 0;
			$post_data = array();
			if($posts){
				foreach( $posts as $post ){
					$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
	            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
	            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
					$post_data[$post_data_counter] = array(
						'post_id'			=> $post['post_id'],
						'post_title'		=> $post['post_title'],
						'post_content'		=> $post['post_content'],
						'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
						'post_time'			=> strtotime($post['post_time'])*1000,
						'cat'				=> $post['cat_name'],
						'author_token'		=> $post['user_token'],
						'author_fname'		=> $post['user_info_fname'],
						'author_lname'		=> $post['user_info_lname'],
						'author_username'	=> $post['user_name'],
						'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
						'show_authorname'	=> $post['show_name'],
						'show_authorimg'	=> $post['show_image'],
						'check_like'		=> $check_like,
						'like_count'		=> $like_count,
						'check_favourite'	=> $check_favourite,
						'slug'				=> $post['post_slug'],
						'template'			=> $post['template']
					);
					$post_data_counter++;
				}
			}else{
				$post_data = array();
			}
			$array = array(
				"code" => 1,
				"message" => "Success",
				"posts"	=> $post_data
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function profile_blog(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_token = $all_data->user_token;
			$user_id 	= $this->frontmodel->get_user_id_by_token( $user_token );
			$per_page 	= $all_data->per_page;
			$offset 	= $all_data->offset;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$posts = $this->frontmodel->get_post_user($per_page, $offset, $user_id);
			$post_data_counter = 0;
			$post_data = array();
			if($posts){
				foreach( $posts as $post ){
					$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
	            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
	            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
					$post_data[$post_data_counter] = array(
						'post_id'			=> $post['post_id'],
						'post_title'		=> $post['post_title'],
						'post_content'		=> $post['post_content'],
						'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
						'post_time'			=> strtotime($post['post_time'])*1000,
						'cat'				=> $post['cat_name'],
						'author_token'		=> $post['user_token'],
						'author_fname'		=> $post['user_info_fname'],
						'author_lname'		=> $post['user_info_lname'],
						'author_username'	=> $post['user_name'],
						'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
						'show_authorname'	=> $post['show_name'],
						'show_authorimg'	=> $post['show_image'],
						'check_like'		=> $check_like,
						'like_count'		=> $like_count,
						'check_favourite'	=> $check_favourite,
						'slug'				=> $post['post_slug'],
						'template'			=> $post['template']
					);
					$post_data_counter++;
				}
			}else{
				$post_data = array();
			}
			$array = array(
				"code" => 1,
				"message" => "Success",
				"posts"	=> $post_data
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function favourite_blog(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			// $per_page 	= $all_data->per_page;
			// $offset 	= $all_data->offset;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$posts = $this->frontmodel->get_favourite_post($user_id);
			$post_data_counter = 0;
			$post_data = array();
			if($posts){
				foreach( $posts as $post ){
					$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
	            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
	            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
					$post_data[$post_data_counter] = array(
						'post_id'			=> $post['post_id'],
						'post_title'		=> $post['post_title'],
						'post_content'		=> $post['post_content'],
						'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
						'post_time'			=> strtotime($post['post_time'])*1000,
						'cat'				=> $post['cat_name'],
						'author_token'		=> $post['user_token'],
						'author_fname'		=> $post['user_info_fname'],
						'author_lname'		=> $post['user_info_lname'],
						'author_username'	=> $post['user_name'],
						'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
						'show_authorname'	=> $post['show_name'],
						'show_authorimg'	=> $post['show_image'],
						'check_like'		=> $check_like,
						'like_count'		=> $like_count,
						'check_favourite'	=> $check_favourite,
						'slug'				=> $post['post_slug'],
						'template'			=> $post['template']
					);
					$post_data_counter++;
				}
			}else{
				$post_data = null;
			}
			$array = array(
				"code" => 1,
				"message" => "Success",
				"posts"	=> $post_data
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function post_by_id(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$post_id 	= $all_data->post_id;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$post = $this->frontmodel->get_post_by_id($post_id);
			// print_r($posts);
			if($post){
				$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
				$post_data = array(
					'post_id'			=> $post['post_id'],
					'post_title'		=> $post['post_title'],
					'post_content'		=> $post['post_content'],
					'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
					'post_time'			=> strtotime($post['post_time'])*1000,
					'cat'				=> $post['cat_name'],
					'author_token'		=> $post['user_token'],
					'author_fname'		=> $post['user_info_fname'],
					'author_lname'		=> $post['user_info_lname'],
					'author_username'	=> $post['user_name'],
					'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
					'show_authorname'	=> $post['show_name'],
					'show_authorimg'	=> $post['show_image'],
					'check_like'		=> $check_like,
					'like_count'		=> $like_count,
					'check_favourite'	=> $check_favourite,
					'slug'				=> $post['post_slug'],
					'template'			=> $post['template']
				);
			}else{
				$post_data = array();
			}
			$array = array(
				"code" => 1,
				"message" => "Success",
				"posts"	=> $post_data
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	public function post_by_slug(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$post_slug 	= $all_data->post_slug;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$post = $this->frontmodel->get_post_by_slug($post_slug);
			// print_r($posts);
			if($post){
				$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
				$post_data = array(
					'post_id'			=> $post['post_id'],
					'post_title'		=> $post['post_title'],
					'post_content'		=> $post['post_content'],
					'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
					'post_time'			=> strtotime($post['post_time'])*1000,
					'cat'				=> $post['cat_name'],
					'author_token'		=> $post['user_token'],
					'author_fname'		=> $post['user_info_fname'],
					'author_lname'		=> $post['user_info_lname'],
					'author_username'	=> $post['user_name'],
					'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
					'show_authorname'	=> $post['show_name'],
					'show_authorimg'	=> $post['show_image'],
					'check_like'		=> $check_like,
					'like_count'		=> $like_count,
					'check_favourite'	=> $check_favourite,
					'slug'				=> $post['post_slug'],
					'template'			=> $post['template']
				);
			}else{
				$post_data = array();
			}
			$array = array(
				"code" => 1,
				"message" => "Success",
				"posts"	=> $post_data
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Popular Posts Api's
	|--------------------------------------------------------------------------
	*/
	public function popular_posts(){
		
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			// $per_page 	= $all_data->per_page;
			// $offset 	= $all_data->offset;
			// $user_token = $this->frontmodel->get_user_token_by_id( $user_id );
			$posts = $this->frontmodel->get_popular_posts();
			$post_data_counter = 0;
			$post_data = array();
			if($posts){
				foreach( $posts as $post ){
					$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
	            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
	            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
					$post_data[$post_data_counter] = array(
						'post_id'			=> $post['post_id'],
						'post_title'		=> $post['post_title'],
						'post_content'		=> $post['post_content'],
						'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
						'post_time'			=> strtotime($post['post_time']),
						'cat'				=> $post['cat_name'],
						'author_token'		=> $post['user_token'],
						'author_fname'		=> $post['user_info_fname'],
						'author_lname'		=> $post['user_info_lname'],
						'author_username'	=> $post['user_name'],
						'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
						'show_authorname'	=> $post['show_name'],
						'show_authorimg'	=> $post['show_image'],
						'check_like'		=> $check_like,
						'like_count'		=> $like_count,
						'check_favourite'	=> $check_favourite,
					);
					$post_data_counter++;
				}
			}else{
				$post_data = null;
			}
			$array = array(
				"code" => 1,
				"message" => "Success",
				"posts"	=> $post_data
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Search Api
	|--------------------------------------------------------------------------
	*/
	public function Search(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$keyword 	= $all_data->keyword;

			$posts = $this->frontmodel->search_post_by_keyword($keyword);
			$post_data_counter = 0;
			$post_data = array();
			if($posts){
				foreach( $posts as $post ){
					$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
	            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
	            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
					$post_data[$post_data_counter] = array(
						'post_id'			=> $post['post_id'],
						'post_title'		=> $post['post_title'],
						'post_content'		=> $post['post_content'],
						'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
						'post_time'			=> strtotime($post['post_time']),
						'cat'				=> $post['cat_name'],
						'author_token'		=> $post['user_token'],
						'author_fname'		=> $post['user_info_fname'],
						'author_lname'		=> $post['user_info_lname'],
						'author_username'	=> $post['user_name'],
						'show_authorname'	=> $post['show_name'],
						'show_authorimg'	=> $post['show_image'],
						'check_like'		=> $check_like,
						'like_count'		=> $like_count,
						'check_favourite'	=> $check_favourite,
					);
					$post_data_counter++;
				}
			}else{
				$post_data = array();
			}
			$peoples = $this->frontmodel->search_user_by_keyword($keyword);
			$people_data_counter = 0;
			$people_data = array();
			if($peoples){
				foreach( $peoples as $people ){
					$people_data[$people_data_counter] = array(
						'user_token'			=> $people['user_token'],
						'fname'					=> $people['user_info_fname'],
						'lname'					=> $people['user_info_lname'],
						'user_image'			=> $people['user_image'],
						'user_name'				=> $people['user_name'],
						'show_image'			=> $people['show_image'],
						'show_name'				=> $people['show_name'],
					);
					$people_data_counter++;
				}
			}else{
				$people_data = array();
			}


			if( $posts && $peoples ){
				$array = array(
					"code" 		=> 1,
					"message" 	=> 'Success',
					"post"		=> $post_data,
					"people"	=> $people_data
				);
				echo json_encode($array);
			}else{
				if( $posts ){
					$array = array(
						"code" 		=> 1,
						"message" 	=> 'Success',
						"post"		=> $post_data,
						"people"	=> $people_data
					);
					echo json_encode($array);
				}elseif( $peoples ){
					$array = array(
						"code" 		=> 1,
						"message" 	=> 'Success',
						"post"		=> $post_data,
						"people"	=> $people_data
					);
					echo json_encode($array);
				}else{
					$array = array(
						"code" 		=> 1,
						"message" 	=> 'Success',
						"post"		=> $post_data,
						"people"	=> $people_data
					);
					echo json_encode($array);
				}
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Notification Api
	|--------------------------------------------------------------------------
	*/
	public function notification(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$notification = $this->frontmodel->fetch_notification($user_id);
			// print_r($notification);
			$array = array(
				"code" => 1,
				"message" => 'Success',
				"data"	=> $notification
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Create Post Api
	|--------------------------------------------------------------------------
	*/
	public function create_post(){
		// $all_data_json = file_get_contents('php://input');
		$all_data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		if (!isset($all_data['apiuser']) || !isset($all_data['apipass'])) {
            $return_res = array('code' => 9, 'message' => $this->response_text[9]);
            echo json_encode($return_res);
            exit();
        }elseif( $all_data['apiuser'] != $this->apiuser OR $all_data['apipass'] != $this->apipass ){
        	$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
        }else{
        	$user_id 	= $all_data['user_id'];
        	$title 		= $all_data['title'];
        	$slug 		= $this->frontmodel->generate_post_slug( $title );
        	$content 	= $all_data['content'];
        	$cat_id 	= $all_data['cat_id'];
        	$template 	= $all_data['template'];

        	if( isset($_FILES["pstimg"]["name"]) ){
        		$config['upload_path']          = './uploads/post/';
	            $config['allowed_types']        = 'gif|jpg|png|jpeg';
	            $config['max_size']             = 2000;
	            $config['max_width']            = 5000;
	            $config['max_height']           = 3000;
	            $config['file_name']           	= time().'.jpeg';
	            $this->load->library('upload', $config);
	            if ( !$this->upload->do_upload('pstimg')){
	            	$array = array(
						'code' 		=> 0,
						'message' 	=> strip_tags( $this->upload->display_errors() ),
					);
					echo json_encode($array);
					die();
	            }else{
	            	$uploaded_data =  $this->upload->data();
	            	$gallery_data = array(
						'user_id'	=> $user_id,
						'file_name' => $uploaded_data["file_name"],
						'file_url' 	=> site_url('uploads/post/').$uploaded_data["file_name"],
						'file_size' 	=> $uploaded_data["file_size"],
						'gallery_type' 	=> '1',
					);
					$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
					$gallery_id = $this->db->insert_id();
					$featured_img_url = site_url('uploads/post/').$uploaded_data["file_name"];
	            }
        	}else{
        		$featured_img_url = null;
				$gallery_id 	= null;
        	}
			$post_data = array(
				'user_id' 			=> $user_id,
				'post_slug' 		=> $slug,
				'post_title' 		=> $title,
				'post_content' 		=> base64_decode($content),
				'featured_img_url' 	=> $featured_img_url,
				'featured_img_id' 	=> $gallery_id,
				'cat_id' 			=> $cat_id,
				'template' 			=> $template,
			);

			
			$insert_post = $this->db->insert('wst_posts', $post_data);
			$post_id = $this->db->insert_id();
			if( $insert_post ){
				$update_user_cat_stat = $this->frontmodel->update_user_cat_stat($user_id, $cat_id);
				if( $update_user_cat_stat ){
					$post = $this->frontmodel->get_post_by_id($post_id);
					// print_r($posts);
					if($post){
						$check_like = $this->frontmodel->check_like( $post['post_id'], $user_id );
		            	$like_count = $this->frontmodel->like_count( $post['post_id'] );
		            	$check_favourite = $this->frontmodel->check_favourite( $post['post_id'], $user_id );
						$post_data = array(
							'post_id'			=> $post['post_id'],
							'post_title'		=> $post['post_title'],
							'post_content'		=> $post['post_content'],
							'post_img_url'		=> str_replace('localhost','192.168.0.99',$post['featured_img_url']),
							'post_time'			=> strtotime($post['post_time'])*1000,
							'cat'				=> $post['cat_name'],
							'author_token'		=> $post['user_token'],
							'author_fname'		=> $post['user_info_fname'],
							'author_lname'		=> $post['user_info_lname'],
							'author_username'	=> $post['user_name'],
							'author_image'		=> str_replace('localhost','192.168.0.99',$post['user_image']),
							'show_authorname'	=> $post['show_name'],
							'show_authorimg'	=> $post['show_image'],
							'check_like'		=> $check_like,
							'like_count'		=> $like_count,
							'check_favourite'	=> $check_favourite,
						);
					}else{
						$post_data = array();
					}
					$array = array(
						'code'	=> 1,
						'message'	=> 'Succesfully post created',
						'post'		=> $post_data
					);
					echo json_encode($array);
					die();
				}else{
					$array = array(
						'code' 		=> 0,
						'message' 	=> 'Error in Updateing the user category statistics'
					);
					echo json_encode($array);
					die();
				}
			}else{
				$array = array(
					'code' 		=> 0,
					'message' 	=> 'Error in Post upload to database'
				);
				echo json_encode($array);
				die();
			}
        }

	}//fn
	/*
	|--------------------------------------------------------------------------
	| Edit Post Api
	|--------------------------------------------------------------------------
	*/
	public function edit_post(){
		// $all_data_json = file_get_contents('php://input');
		$all_data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		if (!isset($all_data['apiuser']) || !isset($all_data['apipass'])) {
            $return_res = array('code' => 9, 'message' => $this->response_text[9]);
            echo json_encode($return_res);
            exit();
        }elseif( $all_data['apiuser'] != $this->apiuser OR $all_data['apipass'] != $this->apipass ){
        	$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
        }else{
        	$user_id 	= $all_data['user_id'];
        	$post_id 	= $all_data['post_id'];
        	$title 		= $all_data['title'];
        	// $slug 		= $this->frontmodel->generate_post_slug( $title );
        	$content 	= $all_data['content'];
        	$cat_id 	= $all_data['cat_id'];
        	// $template 	= $all_data['template'];

        	if( isset($_FILES["pstimg"]["name"]) ){
        		$config['upload_path']          = './uploads/post/';
	            $config['allowed_types']        = 'gif|jpg|png|jpeg';
	            $config['max_size']             = 2000;
	            $config['max_width']            = 5000;
	            $config['max_height']           = 3000;
	            $config['file_name']           	= time().'.jpeg';
	            $this->load->library('upload', $config);
	            if ( !$this->upload->do_upload('pstimg')){
	            	$array = array(
						'code' 		=> 0,
						'message' 	=> strip_tags( $this->upload->display_errors() ),
					);
					echo json_encode($array);
					die();
	            }else{
	            	$uploaded_data =  $this->upload->data();
	            	$gallery_data = array(
						'user_id'	=> $user_id,
						'file_name' => $uploaded_data["file_name"],
						'file_url' 	=> site_url('uploads/post/').$uploaded_data["file_name"],
						'file_size' 	=> $uploaded_data["file_size"],
						'gallery_type' 	=> '1',
					);
					$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
					$gallery_id = $this->db->insert_id();
					$featured_img_url = site_url('uploads/post/').$uploaded_data["file_name"];
					$old_image_id = $this->frontmodel->get_img_id_for_post( $post_id );
					if( $old_image_id ){
						$old_delete = $this->frontmodel->delete_old_image_after_post_image_update($old_image_id);
					}
	            }
	            $post_data = array(
					'user_id' 			=> $user_id,
					// 'post_slug' 		=> $slug,
					'post_title' 		=> $title,
					'post_content' 		=> $content,
					'featured_img_url' 	=> $featured_img_url,
					'featured_img_id' 	=> $gallery_id,
					'cat_id' 			=> $cat_id,
					// 'template' 			=> $template,
				);
        	}else{
        		$post_data = array(
					'user_id' 			=> $user_id,
					// 'post_slug' 		=> $slug,
					'post_title' 		=> $title,
					'post_content' 		=> $content,
					// 'featured_img_url' 	=> $featured_img_url,
					// 'featured_img_id' 	=> $gallery_id,
					'cat_id' 			=> $cat_id,
					// 'template' 			=> $template,
				);
        	}
			

			
			$update = $this->db->update('wst_posts', $post_data, array('post_id' => $post_id));
			if( $update ){
				$array = array(
					'code'	=> 1,
					'message'	=> 'Succesfully post updated'
				);
				echo json_encode($array);
				die();
			}else{
				$array = array(
					'code' 		=> 0,
					'message' 	=> 'Error in Post upgrade to database'
				);
				echo json_encode($array);
				die();
			}
        }

	}//fn
	/*
	|--------------------------------------------------------------------------
	| Delete Post Api
	|--------------------------------------------------------------------------
	*/
	public function delete_post(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$post_id 	= $all_data->post_id;
			$old_image_id = $this->frontmodel->get_img_id_for_post( $post_id );
			$delete_post = $this->db->delete('wst_posts', array('post_id' => $post_id));
			if( $delete_post ){
				$delete_like  		= $this->db->delete('wst_posts_like', array('post_id' => $post_id));
				$delete_favourite  = $this->db->delete('wst_favourite_post', array('post_id' => $post_id));
				$array = array(
					'code' 		=> 1,
					'message' 	=> 'Post Deleted succsefully',
				);
				echo json_encode($array);
				die();
			}else{
				$array = array(
					'code' 		=> 0,
					'message' 	=> 'Error in Post delete from database'
				);
				echo json_encode($array);
				die();
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Report Posts Api
	|--------------------------------------------------------------------------
	*/
	public function report_post(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
			$report_reason 	= $all_data->report_reason;
			$report_reason_explain 	= $all_data->report_reason_explain;
			$post_id 	= $all_data->post_id;
			$author_token 	= $all_data->author_token;
			$author_id = $this->frontmodel->get_user_id_by_token($author_token);
			//convert to author_id
			if( $this->frontmodel->if_already_reported( $post_id, $user_id ) ){
				$array = array(
						"code" => 5,
						"message" => 'Post already reported!'
					);
					echo json_encode($array);
			}else{
				$data = array(
					'post_id' 				=> $post_id,
					'user_id' 				=> $user_id,
					'author_id' 			=> $author_id,
					'report_reason' 		=> $report_reason,
					'report_reason_explain' => $report_reason_explain,
				);
				$insert = $this->db->insert( 'wst_report_posts', $data );
				if( $insert ){
					$array = array(
						"code" => 1,
						"message" => 'Post reported succesfully'
					);
					echo json_encode($array);
				}else{
					$array = array(
						"code" => 0,
						"message" => 'Post report failed.'
					);
					echo json_encode($array);
				}	
			}
			
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Like unlike Api
	|--------------------------------------------------------------------------
	*/
	public function like_unlike(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 				= $all_data->user_id;
			$post_id 				= $all_data->post_id;
			$like_unlike_switch 	= $all_data->like_unlike_switch;
			$author_token 			= $all_data->author_token;
			$author_id 				= $this->frontmodel->get_user_id_by_token( $author_token );
			// switch = 1 -> add like, swicth = 0 -> remove like.
			if( $like_unlike_switch == 1 ){
				//add like.
				$check_like = $this->frontmodel->check_like( $post_id, $user_id );
				if( $check_like ){
					$array = array(
						"code" => 0,
						"message" => "Like already exists"
					);
					echo json_encode($array);
				}else{
					$data = array(
						'post_id'		=> $post_id,
						'user_id'		=> $user_id,
					);
					$insert_like = $this->db->insert('wst_posts_like', $data);
					if($insert_like){
						$user_full_name  	= $this->frontmodel->get_user_name_by_id($user_id);
						$content 			= '<b>'.$user_full_name.'</b> liked your post.';

						$not_data 			= array(
							'not_type' 		=> '0',
							'not_user_id'	=> $author_id,
							'not_user_token'=> $author_token,
							'content'		=> $content,
							'not_status'	=> '0',
							'created_by'	=> $user_id,
						);
						if( $author_id == $user_id ){
							$insert_not = true;
						}else{
							$insert_not = $this->db->insert('wst_notification', $not_data);
							// create firebase push notification here
							$user_token = $this->frontmodel->get_user_token_by_id( $user_id );
							$liked_by_details = $this->frontmodel->get_user_by_token( $user_token );
							//firebase notification
							// $repl = site_url('/post/');
							$post_slug = $this->frontmodel->get_post_slug_by_id( $post_id );
							$details = array(
								'notification_id'	=> $this->db->insert_id(),
								'token' 			=> $user_token,
								'username'			=> $liked_by_details['user_name'],
								'user_info_fname'	=> $liked_by_details['user_info_fname'],
								'user_info_lname'	=> $liked_by_details['user_info_lname'],
								'user_image'		=> str_replace('localhost','192.168.0.99',$liked_by_details['user_image']),
								'show_name'			=> $liked_by_details['show_name'],
								'show_image'		=> $liked_by_details['show_image'],
								'message'			=> '[[[author_name]]] has liked your post',
								'details'			=> array('notification_type' => 1, 'post_slug'=> $post_slug)
							);
							$notification = array(
								'code' 	=> 2,
								'data' 	=> $details
							);
							$token = $this->frontmodel->get_user_device_token($author_id);
							// $this->push_notification( $body, $author_id);
							$firebase = $this->push_notification_like_post( $token, $notification, $author_id);
						}
						if( $insert_not ){
							$array = array(
								"code" => 1,
								"message" => "Post is liked."
							);
							echo json_encode($array);
						}else{
							$array = array(
								"code" => 0,
								"message" => "Problem in notification creation"
							);
							echo json_encode($array);
						}
					}
				}
			}else{
				// remove like
				$check_like = $this->frontmodel->check_like( $post_id, $user_id );
				if( $check_like ){
					$delete_like = $this->db->delete('wst_posts_like', array( 'post_id' => $post_id,'user_id' => $user_id, ));
					if( $delete_like ){
						$array = array(
							"code" => 1,
							"message" => "Post is disliked."
						);
						echo json_encode($array);
					}else{
						$array = array(
							"code" => 0,
							"message" => "Database error!."
						);
						echo json_encode($array);
					}
				}else{
					$array = array(
						"code" => 0,
						"message" => "Like does not exists"
					);
					echo json_encode($array);
				}
			}

		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| fav_unfav Api
	|--------------------------------------------------------------------------
	*/
	public function fav_unfav(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 			= $all_data->user_id;
			$post_id 			= $all_data->post_id;
			$fav_unfav_switch 	= $all_data->fav_unfav_switch;
			$author_token 		= $all_data->author_token;
			$author_id 			= $this->frontmodel->get_user_id_by_token( $author_token );
			// switch = 1 -> add fav, swicth = 0 -> remove fav.
			if( $fav_unfav_switch == 1 ){
				//add fav
				$check_favourite = $this->frontmodel->check_favourite( $post_id, $user_id );
				if( $check_favourite ){
					$array = array(
						"code" => 0,
						"message" => "Already marked as favourite."
					);
					echo json_encode($array);
				}else{
					$data = array(
						'post_id'		=> $post_id,
						'user_id'		=> $user_id,
						'author_id'		=> $author_id,
					);
					$insert = $this->db->insert('wst_favourite_post', $data);
					if( $insert ){
						$array = array(
							"code" => 1,
							"message" => "Post is marked favourite."
						);
						echo json_encode($array);
					}else{
						$array = array(
							"code" => 0,
							"message" => "Database error."
						);
						echo json_encode($array);
					}
				}
			}else{
				//remove fav
				$check_favourite = $this->frontmodel->check_favourite( $post_id, $user_id );
				if( $check_favourite ){
					$delete = $this->db->delete('wst_favourite_post', array( 'post_id' => $post_id,'user_id' => $user_id, ));
					if( $delete ){
						$array = array(
							"code" => 1,
							"message" => "Succesfully removed from favourite."
						);
						echo json_encode($array);
					}else{
						$array = array(
							"code" => 0,
							"message" => "Database error."
						);
						echo json_encode($array);
					}
				}else{
					$array = array(
						"code" => 0,
						"message" => "Not marked as favourite."
					);
					echo json_encode($array);
				}
			}
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Category List Api
	|--------------------------------------------------------------------------
	*/
	public function get_all_category(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			// $user_id 	= $all_data->user_id;
			$all_cat = $this->adminmodel->get_cat_list();
			$cat_list = array();
			if( $all_cat ){
				$cat_counter = 0;
				foreach( $all_cat as $cat ){
					$cat_list[$cat_counter] = array(
						'cat_id' 	=> $cat['cat_id'],
						'cat_name'	=> $cat['cat_name']
					);
					$cat_counter++;
				}
			}else{
				$cat_list = array();
			}
			$reportreason = array(
				'Promotes Violence',
                'Racism or Racist Remarks',
                'Promotes Child Abuse',
                'Nudity',
                'Hate mongering',
                'Animal abuse',
                'Gender abuse',
                'False information',
                'Sale of goods',
                'Harrassment',
                'Copyright issues',
                'Provocation of negativity through race, ethnic and class disputes.',
		    	'Others',
			);

			$array = array(
				"code" => 1,
				"data" => $cat_list,
				"report_reason"=> $reportreason
			);
			echo json_encode($array);
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Sample Api
	|--------------------------------------------------------------------------
	*/
	public function sample(){
		$all_data_json = file_get_contents('php://input');
		$all_data = ( isset( $all_data_json ) && $all_data_json != "" )? json_decode( $all_data_json ) : "";
		if( $all_data->apiuser == $this->apiuser AND $all_data->apipass == $this->apipass ){
			$user_id 	= $all_data->user_id;
		}else{
			$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Image Sample Api
	|--------------------------------------------------------------------------
	*/
	public function image_sample(){
		// $all_data_json = file_get_contents('php://input');
		$all_data = $this->input->post(NULL, TRUE); // returns all POST items with XSS filter
		if (!isset($all_data['apiuser']) || !isset($all_data['apipass'])) {
            $return_res = array('code' => 9, 'message' => $this->response_text[9]);
            echo json_encode($return_res);
            exit();
        }elseif( $all_data['apiuser'] != $this->apiuser OR $all_data['apipass'] != $this->apipass ){
        	$array = array(
				"code" => 9,
				"message" => $this->response_text[9]
			);
			echo json_encode($array);
        }else{
        	$user_id 	= $all_data['apius'];
			
        }

	}//fn
	
	public function test_mail(){
		$this->load->library('email');
        //SMTP & mail configuration

        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'palefire2019@gmail.com',
            'smtp_pass' => '1pale2fire',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->to('sarasij94@gmail.com');
        $this->email->from('palefire2019@gmail.com','Walledstory');
        $this->email->subject('OTP');
        $this->email->message( 'hello' );

        if(!$this->email->send()){
            print_r($this->email->print_debugger());
        }else{
            echo  1;
        }
	}//fn


}//class
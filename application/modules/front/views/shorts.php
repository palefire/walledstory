<style type="text/css">
    

    @media only screen and (max-device-width: 360px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important }
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
        .posts{display: none}

    }
     @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important ; top:46%;}
        .croppie-container .cr-boundary{width: 230px !important; height: 150px !important}
        .croppie-container .cr-viewport{width: 210px !important; height: 135px !important}
        .croppie-container .cr-image{width: 585 !important; height: 388 !important}
        .croppie-container .cr-overlay,{width: 210px !important; height: 135px !important}
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
        .posts{display: none}
    }
</style>
<div class="row box">

    <!-- Left box -->
    <div class="box-1">

        <!-- Left sidebar -->
        <div class="left-side-bar">
            <div class="container-fluid">
                <div class="row">

                    <!-- Profile Picture -->
                    <div class="col-12 side-bar-dp text-center">
                        <?php
                            if( $profile['show_image'] == 1 ){
                                if( $profile['user_image'] != null ){
                        ?>
                                    <img id="profile-image-left" src="<?php echo $profile['user_image'] ?>" alt="">
                        <?php            
                                }else{
                        ?>
                                    <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                        <?php
                                }
                        ?>
                            
                        <?php
                            }else{
                        ?>
                                <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                        <?php
                            }
                        ?>
                        <div></div>
                        <span><?php echo $header_data_array['fname'].' '.$header_data_array['lname'] ?> </span>
                    </div>
                    <!-- End Profile Picture -->

                    <div class="col-12 side-bar-menu-list">
                        <a href="<?php echo site_url() ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item active"><span><i class="fas fa-home"></i></span>Home
                            </div>
                        </a>
                        <a href="<?php echo site_url('profile') ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-user"></i></span>Profile
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none" data-toggle="modal" data-target="#notificationModal">
                            <div class="side-bar-menu-list-item">
                                <span>
                                    <i class="fas fa-bell"></i>
                                </span>
                                Notifications 
                                <label class="notification-label <?php 
                                    if( $notification['unseen']['number'] > 0 ){
                                        echo 'not-label-show';
                                    }else{
                                        echo 'not-label-hide';
                                    }

                                ?>"><?php echo $notification['unseen']['number'] ?></label>
                            </div>
                        </a>
                        <!-- <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-envelope"></i></span>Messages
                            </div>
                        </a> -->
                        <a href="<?php echo site_url('favourites') ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-star"></i></span>Favourites
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <!-- Left sidebar end -->

    </div>
    <!-- Left Box End -->

    <!-- Middle box -->
    <div class="box-2">
        <div class="container">
            

            

            
            
            <!-- Current Posts -->
            <div class="row current-posts">
                <?php echo $all_blog_view; ?>
                
            </div>
            <!-- Current Posts End -->
            <!-- Load more button -->
            <div class="text-center" id="load-more-parent-div">
                <button class="btn btn-primary btn-lg load-more" id="load-more-btn" data-load_more_index = "1">
                    Load more 
                    <i class="fas fa-angle-double-right load-more-i"></i>
                    <i class="fas fa-spinner fa-spin loading-more-i" style="display: none"></i>
                </button>
            </div>
            <!-- Load more button -->
        </div>
    </div>
    <!-- Right box -->
    <div class="box-3">

        <?php echo $right_bar_view; ?>
    </div>
    <!-- Right Box End -->
</div>
<script type="text/javascript">

    

    $(document).ready(function(){
        $("#load-more-btn").on('click', function(){
            var that = $(this);
            var load_more_index = that.attr('data-load_more_index');
            load_more_index = parseInt( load_more_index );
            // alert(load_more_index);

            $.ajax({
                beforeSend : function(xhr){
                  that.children('.load-more-i').hide();
                  that.children('.loading-more-i').show();
                },
                url: "<?php echo site_url('front/load_more_shorts_posts') ?>",
                type: "POST",
                data: {
                    "load_more_index":load_more_index,
                },
                success: function (data) {
                    console.log(data);
                    $data_array = jQuery.parseJSON(data);
                    if( $data_array.more_exist == 1 ){
                        that.parent('#load-more-parent-div').siblings('.current-posts').append($data_array.posts);
                        that.children('.load-more-i').show();
                        that.children('.loading-more-i').hide();
                    }else{
                        that.parent('#load-more-parent-div').siblings('.current-posts').append($data_array.posts);
                        that.hide();
                    }
                // console.log(data)
                    var new_load_more_index = load_more_index +1;
                    that.attr('data-load_more_index', new_load_more_index);
                },
                error : function(response ){
                    console.log(response)
                }
            });//ajax

        });
    });
</script>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Walledstory</title>
    <link rel="icon" href="<?php echo $this->data['logo'] ?>" type="" sizes="16x16">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/croppie.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css?v=').microtime()?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css?v=').microtime()?>">
    
    <link href="<?php echo base_url('assets/fontawesome/css/all.css')?>" rel="stylesheet">



       <!--General Scripts-->
    <script src="<?php echo base_url('assets/js/jquery-3.5.1.slim.min.js')?>"></script>
    <script src="<?php echo site_url('assets-admin/vendor/jquery/jquery-3.3.1.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/popper.js')?>"></script>
    <script src="<?php echo base_url('assets/js/thether.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/croppie.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/tinymce/js/tinymce/tinymce.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/tinymce/js/tinymce/init-tinymce.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/tinymce/js/tinymce/jquery.tinymce.min.js')?>"></script>
    <style type="text/css">
        .dj.not-unseen{background-color: #7C88D2;height: auto;width: 100%}
        .notification-label{font-size: 8px;margin-left: 8px; padding: 2px 5px; background: red; border-radius: 6px; margin-top: 2px; }
        .not-label-show{display: block}
        .not-label-hide{display: none}
        .active-post-content{width: 100%;} 

        .ws-like-spinner{display: none}
        .ws-favourite-spinner{display: none; margin-left: 18px; margin-top: 3px }
        .btn-primary{background-color: #7C88D2; border-color: #7C88D2}
        #searchModal{}
        #searchModal .col-md-6{height: 700px;}
        #searchModal ul{margin-top: 40px; margin-left: 48px; }
        #searchModal ul li{font-size: 28px}
        #searchModal ul li a{color: #fff}
        #searchModal ul li a:hover{text-decoration: none}
        .ws-search-spinner{text-align: center; margin-top:140px; }
        .spinner-post-list{display: none}
        #post-serach-list-ul{display: none}
        .spinner-people-list{display: none}

        .playstore{width: 100%;}
        .playstore img{width: 100%;}
        
    </style>
</head>

<body>
    <style type="text/css">
        .tox-tinymce{width: 100% !important; height: 300px !important; border-radius: 10px}
        .tox .tox-editor-container{background:#E2D3B6;}
        .tox .tox-edit-area__iframe{background:unset;}
        .tox .tox-statusbar{display: none}
        .tox button{background-color: #fff !important; }
        .tox .tox-tbtn--bespoke .tox-tbtn__select-label{color: black}
    </style>


    <!-- Main Box -->
    <div class="container main-box">

        <div class="row">
            <!-- Header -->
            <div class="col-12 main-header">
                <div class="main-header-left">
                    <div class="main-header-dot"></div>
                    <div class="main-header-image">
                        <a href="<?php echo site_url() ?>"><img src="<?php echo base_url('assets/files/ws-logo.png')?>" alt="Walledstory"></a>
                    </div>
                </div>
                <div class="main-header-right">
                    <div class="main-header-logout">
                        <label><a href="<?php echo site_url('front/login/logout') ?>">Logout</a></label>
                    </div>
                    <div class="main-header-dp">
                        <a href="<?php echo site_url('profile') ?>">
                            <?php
                                if( $header_data_array['profile_img'] != null ){
                            ?>
                                    <img src="<?php echo $header_data_array['profile_img']?>" alt="">
                            <?php
                                }else{
                            ?>
                                    <img src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                            <?php
                                }
                            ?>
                        </a>
                    </div>
                </div>
            </div>
            <!-- Header End -->
        </div>
    <!-- Notification modal-->
    <div class="modal fixed-right fade" id="notificationModal" tabindex="-1" aria-labelledby="notificationModalLabel" aria-hidden="true" style="">
        <div class="modal-dialog modal-dialog-aside" style="position: absolute; ;">
            <div class="modal-content" style="height:100vh ; width: 260px">
                <div class="modal-header">
                    <h5 class="modal-title" id="notificationModalLabel">Notification</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="overflow-y: scroll;">
                    
                    
                
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                    <a href="#" class="btn btn-primary btn-sm">See All</a>
                </div>
            </div>
        </div>
    </div>

   
    <!-- Mobile Menu modal-->
    <div id="mobilemenuModal" class="modal fixed-left fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-aside" role="document">
            <div class="modal-content" style="background:#7c88d2;">
                <div class="modal-header" style="border:none">
                    <!-- <h5 class="modal-title">Walledstory</h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   <div class="content">
                        <div class="row">
                            <div class="col-12">
                                <div style="margin: auto;height: 200px; width: 200px;">
                                    <a href="<?php echo site_url('profile') ?>">
                                        <?php
                                            if( $header_data_array['profile_img'] != null ){
                                        ?>
                                                <img style="width: 100%; height: 100%; border-radius: 50%" src="<?php echo $header_data_array['profile_img']?>" alt="">
                                        <?php
                                            }else{
                                        ?>
                                                <img style="width: 100%; height: 100%; border-radius: 50%" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                                        <?php
                                            }
                                        ?>
                                    </a>
                                </div>
                            </div>
                            <div class="col-12" style="display: flex;flex-direction: column; margin-top: 30px">
                                <a href="<?php echo site_url() ?>" style="text-decoration: none">
                                    <div style="display: flex; flex-direction: row; font-size: 1rem; color: #fff; padding: 1vh 1vh; cursor: pointer; margin-bottom: 2px;margin-left: 50%; transform: translateX(-50%);"><span style="width: 30px; text-align: center;"><i class="fas fa-home"></i></span>Home
                                    </div>
                                </a>
                                <a href="<?php echo site_url('profile') ?>" style="text-decoration: none">
                                    <div style="display: flex; flex-direction: row; font-size: 1rem; color: #fff; padding: 1vh 1vh; cursor: pointer; margin-bottom: 2px;margin-left: 50%; transform: translateX(-50%);"><span style="width: 30px; text-align: center;"><i class="fas fa-user"></i></span>Profile
                                    </div>
                                </a>
                                <a href="<?php echo site_url('front/login/logout') ?>" style="text-decoration: none">
                                    <div style="display: flex; flex-direction: row; font-size: 1rem; color: #fff; padding: 1vh 1vh; cursor: pointer; margin-bottom: 2px;margin-left: 50%; transform: translateX(-50%);"><span style="width: 30px; text-align: center;"><i class="fas fa-sign-out-alt"></i></span>Logout
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="border-top: none">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div> <!-- modal-bialog .// -->
    </div> <!-- modal.// -->

    <!-- Under Devlopment modal-->
    <div id="underdevelopmentModal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Walledstory</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>The section is under development</p>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div> <!-- modal-bialog .// -->
    </div>

    <!-- Download the app modal-->
    <div id="downloadtheappModal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Walledstory</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Download the app for this activity!</p>
            <a href="#">
                <div class="playstore" style="width: 100%;">
                    <img src="<?php echo base_url('assets/files/playstore.jpg') ?>" />
                </div>
                
            </a>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div> <!-- modal-bialog .// -->
    </div>

    <!-- Mobile Create Post modal-->
    <div class="modal fade" id="mobile-post-form-modal" tabindex="-1" role="dialog" aria-labelledby="postFormModal"aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable ws-modal" role="document" style="width: 85%; top: 45%">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 ws-modal-header">
                                <div class="ws-modal-title">Create Post</div>
                                <div class="ws-modal-buttons">
                                    <div class="ws-modal-buttons-body">                                                        
                                        <div class="ws-modal-switch-button">
                                            <input type="radio" name="mobile_post_template" value="0" checked>
                                            <span>Shorts</span>
                                        </div>
                                        <div class="ws-modal-switch-button">
                                           
                                            <input type="radio" name="post_template" value="1">
                                            <span>Shots</span>
                                        </div>
                                       
                                    </div>                                                    
                                </div>
                            </div>
                            <div class="ws-modal-body">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12">
                                            <form class="ws-modal-form" autocomplete="off" id="mobile-post-form">
                                                <div class="form-group title-parent">
                                                  <label for="title">Title</label>
                                                  <input type="text" class="form-control" id="mobile-title" placeholder="Your title here" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                  <label for="mobile-sharewiththeworld">Share with the world</label>
                                                  <textarea class="form-control tinymce" id="mobile-sharewiththeworld" rows="5"></textarea>
                                                </div>
                                                <div class="form-group content-parent">
                                                  <label for="title">Image</label>
                                                  <input type="file" class="form-control" id="mobile-image" >
                                                  
                                                </div>
                                                <div class="row">
                                                    <div class="col-6 text-left">
                                                        <select class="custom-select my-1 mr-sm-2" id="mobile-choosecategory">
                                                            <option value="0">Category</option>
                                                            <?php
                                                                foreach( $header_data_array['cat_list_head'] as $cat ){
                                                            ?>
                                                                    <option value="<?php echo $cat['cat_id'] ?>"><?php echo $cat['cat_name'] ?></option>
                                                                    
                                                            <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="col-6 text-right">
                                                        <button type="button" class="btn btn-primary my-1" id="mobile-submit-post">Share</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Mobile Menu modal-->
    <div id="searchModal" class="modal fixed-left fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-aside" role="document" style="width: 100%">
            <div class="modal-content" style="background:#18191B;">
                <div class="modal-header" style="border:none">
                    <!-- <h5 class="modal-title">Walledstory</h5> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="width: 100%; overflow: hidden">
                   <input type="text" name="search" id="ws-search" placeholder="Search" autocomplete="off" style="width: 90%; text-align: center; margin-left: 50%; transform: translateX(-50%); border: none; height: 50px; font-size: 26px; border-radius: 10px;">
                   <div class="row" style="margin-top: 20px">
                       <div class="col-md-6" style="border-right: 1px solid ">
                           <h1 style="text-align: center; color:#ddd">Posts</h1>
                           <ul id="post-serach-list-ul">
                               <!-- <li><a href="#">The Train to Barrackpore</a></li>
                               <li><a href="#">My Train in my backyard.</a></li>
                               <li><a href="#">My night of terror.</a></li>
                               <li><a href="#">There was a cro so black that it defied darkness.</a></li>
                               <li><a href="#">Once upon a time in mexico.</a></li>
                               <li><a href="#">My Train in my backyard.</a></li>
                               <li><a href="#">My Train in my backyard.</a></li> -->
                           </ul>
                            <div class="fa-3x ws-search-spinner spinner-post-list">
                                <i class="fas fa-spinner fa-spin" style="color: #5164CB"></i>
                            </div>
                       </div>  
                       <div class="col-md-6">
                           <h1 style="text-align: center; color:#ddd">People</h1>
                           <ul id="people-serach-list-ul">
                               <!-- <li><a href="#">Sarasij Roy</a></li>
                               <li><a href="#">Sohini876.</a></li>
                               <li><a href="#">Saikat Ghorai.</a></li>
                               <li><a href="#">Rishav Deb Roy.</a></li>
                               <li><a href="#">Sk Juhi ali.</a></li>
                               <li><a href="#">Srikanta maji.</a></li>
                               <li><a href="#">Bojha Chowdhury.</a></li> -->
                           </ul>
                           <div class="fa-3x ws-search-spinner spinner-people-list">
                                <i class="fas fa-spinner fa-spin" style="color: #5164CB"></i>
                            </div>
                       </div>  
                   </div>
                </div>
                <div class="modal-footer" style="border-top: none">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div> <!-- modal-bialog .// -->
    </div> <!-- modal.// -->


<script type="text/javascript">
    $('#notificationModal').on('show.bs.modal', function (event) {
        // var button = $(event.relatedTarget) 
        // var recipient = button.data('whatever') 
        // var modal = $(this)
        // modal.find('.modal-title').text('New message to ' + recipient)
        // modal.find('.modal-body input').val(recipient)
        var that = $(this);
        $.ajax({
            beforeSend : function(xhr){

            },
            url : "<?php echo site_url('front/load_modal_notification_data') ?>",
            type : 'POST',
            data : {
                    
            },
            success: function( data ){
                // console.log(data);
                that.children('.modal-dialog').children('.modal-content').children('.modal-body').html(data);
                $('.notification-label').removeClass('not-label-show');
                $('.notification-label').addClass('not-label-hide');
            },
            error: function(response){
                console.log(response);
            }
        });//ajax
    })

    $(document).ready(function(){
        $('#mobile-submit-post').on('click', function(){
            var wordcount = tinymce.activeEditor.plugins.wordcount;
            var title   = $("#mobile-post-form").children('.title-parent').children('#mobile-title').val();
            var content = tinyMCE.activeEditor.getContent();
            var cat_id = $('#mobile-choosecategory').val();
            var template = $('input[name="mobile_post_template"]:checked').val();
            if( title.length == 0 ){
                return alert("Please provide a title to the post.");
            }
            if( content.length == 0 ){
                return alert("Please provide a content to the post.");
            }
            if( cat_id == 0 ){
                return alert("Please provide a category to the post.");
            }
            var data = new FormData();
            data.append('pstimg', $('#mobile-image').prop('files')[0]);
            data.append('title', title);
            data.append('content', content);
            data.append('cat_id', cat_id);
            data.append('template', template);

            $.ajax({
                url : "<?php echo base_url('front/add_post_db_mobile'); ?>",
                type: 'post',
                data : data,
                contentType: false,
                cache: false,
                processData: false,
                success: function(data){
                    var data_array = jQuery.parseJSON(data);
                    if( $data_array.code == 1 ){
                        location.reload();
                    }else{
                         console.log(data_array.message)
                    }
                    // console.log(data)
                    },
                    error: function(data){
                        console.log(data);
                    }
            }); //ajax
        });
    });

    $(document).ready(function(){
        $('#ws-search').on('keyup', function(){
            var that = $(this);
            var keyword = that.val();
            $.ajax({
                beforeSend : function(xhr){
                    // $('#post-serach-list-ul').hide();
                    $('.spinner-post-list').show();
                    $('.spinner-people-list').show();
                },
                url : "<?php echo site_url('front/ws_search') ?>",
                type : 'POST',
                data : {
                    keyword : keyword,     
                },
                success: function( data ){
                    console.log(data);
                    var data_array = jQuery.parseJSON(data);
                    if( data_array.code == 1 ){
                        //posts
                        $('.spinner-post-list').hide();
                        $("#post-serach-list-ul").html(data_array.post_list);
                        $('#post-serach-list-ul').show();

                        //people
                        $('.spinner-people-list').hide();
                        $("#people-serach-list-ul").html(data_array.people_list);
                        $('#people-serach-list-ul').show();
                    }else{
                        //posts
                        $('.spinner-post-list').hide();
                        $("#post-serach-list-ul").html('<li>Oops!Something went wrong. Please try again after sometime.</li>');
                        $('#post-serach-list-ul').show();
                         //people
                        $('.spinner-people-list').hide();
                        $("#people-serach-list-ul").html('<li>Oops!Something went wrong. Please try again after sometime.</li>');
                        $('#people-serach-list-ul').show();
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });//ajax
        });
    });

</script>
   

   
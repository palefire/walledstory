<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WalledStory</title>
    <link rel="icon" href="<?php echo $this->data['logo'] ?>" type="image/gif" sizes="16x16">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/stepper.css">
    <link href="assets/fontawesome/css/all.css" rel="stylesheet">
    <style>
      .stepper{display: none;}
      @media only screen and (max-device-width: 360px){
        .l-main-box{max-height:100%;min-height: 100%}
        .l-form span{font-size: 1.6em}
        .l-main-box form option{font-size: 14px}
      }
       @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        .l-main-box{max-height:100%;min-height: 100%}
        .l-form span{font-size: 1.6em}
        .l-main-box form option{font-size: 14px}
      }
    </style>

</head>

<body class="l-body">

    <!-- Main Box -->
    <div class="container l-main-box">
        <div class="row">
            <div class="col-12 text-center l-form mt-3 mb-3">
                <span>Profile Information</span>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="stepper">
                    <div class="stepper-single">
                        <i class="fas fa-check-circle"></i>
                        <p>Verify Email</p>
                    </div>
                    <div class="stepper-line"></div>
                    <div class="stepper-single">
                        <i class="far fa-check-circle"></i>
                        <p style="text-decoration: underline;">Profile Info</p>
                    </div>
                    <div class="stepper-line"></div>
                    <div class="stepper-single">
                        <i class="far fa-check-circle"></i>
                        <p>Profile Image</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <form method="post" action="<?php echo site_url('front/login/save_extra_profile_info_to_db') ?>" autocomplete="off">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">First Name</label>
                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter your First Name" name="fname" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter your Last Name" name="lname" required>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Gender</label>
                        <select class="form-control" aria-describedby="Default select example" name="user_info_gender">
                            <option selected>Do not want to specify</option>
                            <option value="1">Male</option>
                            <option value="2">Female</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter your address." name="user_info_address" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">City/Village</label>
                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter your city/village." name="user_info_city" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">State</label>
                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter your state." name="user_info_state" required>
                    </div>
                </div>
               <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Country</label>
                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter your country." name="user_info_country" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Zip</label>
                        <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Enter your zip/pin." name="user_info_zip" required>
                    </div>
                </div>



            </div>
            <div class="row">
                <div class="col-md-6 col-6">
                    <div class="form-group text-center">
                        <a href="<?php echo site_url() ?>" type="button" class="btn btn-info" id="" >Skip</a>
                    </div>
                </div>
                <div class="col-md-6 col-6">
                    <div class="form-group text-center">
                        <input type="hidden" class="form-control" aria-describedby="emailHelp" placeholder="Enter your phone number" name="user_info_phone" value="<?php echo $user_info_phone ?>" readonly>
                        <button type="submit" class="btn btn-primary" id="sign-up" >Save</button>
                    </div>
                </div>
            </div>
        </form>
       
    </div>

    <!--General Scripts-->

    <script src="assets/js/jquery-3.5.1.slim.min.js"></script>
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/thether.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url('assets-admin/vendor/jquery/jquery-3.3.1.min.js') ?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            
        });
    </script>
</body>

</html>
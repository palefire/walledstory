<style type="text/css">
    

    @media only screen and (max-device-width: 360px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important }
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
        .posts{display: none}

    }
    @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important ; top:46%;}
        .croppie-container .cr-boundary{width: 230px !important; height: 150px !important}
        .croppie-container .cr-viewport{width: 210px !important; height: 135px !important}
        .croppie-container .cr-image{width: 585 !important; height: 388 !important}
        .croppie-container .cr-overlay,{width: 210px !important; height: 135px !important}
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
        .posts{display: none}
    }
</style>
<div class="row box">

    <!-- Left box -->
    <div class="box-1">

        <!-- Left sidebar -->
        <div class="left-side-bar">
            <div class="container-fluid">
                <div class="row">

                    <!-- Profile Picture -->
                    <div class="col-12 side-bar-dp text-center">
                        <?php
                            if( $profile['show_image'] == 1 ){
                                if( $profile['user_image'] != null ){
                        ?>
                                    <img id="profile-image-left" src="<?php echo $profile['user_image'] ?>" alt="">
                        <?php            
                                }else{
                        ?>
                                    <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                        <?php
                                }
                        ?>
                            
                        <?php
                            }else{
                        ?>
                                <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                        <?php
                            }
                        ?>
                        <div></div>
                        <span><?php echo $header_data_array['fname'].' '.$header_data_array['lname'] ?> </span>
                    </div>
                    <!-- End Profile Picture -->

                    <div class="col-12 side-bar-menu-list">
                        <a href="<?php echo site_url() ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item active"><span><i class="fas fa-home"></i></span>Home
                            </div>
                        </a>
                        <a href="<?php echo site_url('profile') ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-user"></i></span>Profile
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none" data-toggle="modal" data-target="#notificationModal">
                            <div class="side-bar-menu-list-item">
                                <span>
                                    <i class="fas fa-bell"></i>
                                </span>
                                Notifications 
                                <label class="notification-label <?php 
                                    if( $notification['unseen']['number'] > 0 ){
                                        echo 'not-label-show';
                                    }else{
                                        echo 'not-label-hide';
                                    }

                                ?>"><?php echo $notification['unseen']['number'] ?></label>
                            </div>
                        </a>
                        <!-- <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-envelope"></i></span>Messages
                            </div>
                        </a> -->
                        <a href="<?php echo site_url('favourites') ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-star"></i></span>Favourites
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <!-- Left sidebar end -->

    </div>
    <!-- Left Box End -->

    <!-- Middle box -->
    <div class="box-2">
        <div class="container">
            <div class="row">

                <!-- Popular Stories -->
                <div class="col-2 mobile-hide"><label for="story-box">Popular</label></div>
                <div class="col-12 col-md-10 col-lg-10 stories">
                    <?php echo $popular_post_view; ?>
                    
                </div>
                <!-- Popular Stories end -->
            </div>

            <!-- Post Form -->
            <div class="row posts">
                <div class="col-md-2 col-lg-2 mobile-hide"><label for="post-box">Posts</label></div>
                <div class="col-12 col-md-10 col-lg-10 post-box" data-toggle="modal" data-target="#post-form-modal">
                    <?php
                    if( $header_data_array['show_image'] == 1 ){
                        if( $header_data_array['profile_img'] != null ){
                    ?>
                                <img src="<?php echo $header_data_array['profile_img'] ?>" alt="">
                    <?php            
                            }else{
                    ?>
                                <img src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                    <?php
                            }
                    ?>
                        
                    <?php
                        }else{
                    ?>
                            <img src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                    <?php
                        }
                    ?>
                    <form>
                        <div class="form-group">
                            <textarea class="form-control" id="exampleFormControlTextarea1"
                                placeholder="What's on your mind?" rows="2" data-toggle="modal" data-target="#post-form-modal"></textarea>
                        </div>
                        <button type="button" data-toggle="modal" data-target="#post-form-modal" class="btn btn-md btn-primary" style="float: right">Share</button>
                    </form>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="post-form-modal" tabindex="-1" role="dialog" aria-labelledby="postFormModal" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable ws-modal" role="document">
                    <div class="modal-content">
                        <!-- <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div> -->
                        <div class="modal-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 ws-modal-header">
                                        <div class="ws-modal-title">Create Post</div>
                                        <div class="ws-modal-buttons">
                                            <div class="ws-modal-buttons-body">                                                        
                                                <div class="ws-modal-switch-button">
                                                    <!-- <i class="fas fa-circle"></i> -->
                                                    <input type="radio" name="post_template" value="0" checked>
                                                    <span>Shorts</span>
                                                </div>
                                                <div class="ws-modal-switch-button">
                                                    <!-- <i class="fas fa-circle"></i> -->
                                                    <input type="radio" name="post_template" value="1">
                                                    <span>Shots</span>
                                                </div>
                                               <!--  <div class="ws-modal-switch-button">
                                                    <i class="fas fa-circle"></i>
                                                    <span>Video</span>
                                                </div> -->
                                            </div>                                                    
                                        </div>
                                    </div>
                                    <div class="ws-modal-body">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12">
                                                    <form class="ws-modal-form" autocomplete="off" id="post-form">
                                                        <div class="form-group title-parent">
                                                          <label for="title">Title</label>
                                                          <input type="text" class="form-control" id="title" placeholder="Your title here" autocomplete="off">
                                                        </div>
                                                        <div class="form-group">
                                                          <label for="sharewiththeworld">Share with the world</label>
                                                          <textarea class="form-control tinymce" id="sharewiththeworld" rows="5"></textarea>
                                                        </div>
                                                        <div class="form-group content-parent">
                                                          <label for="title">Image</label>
                                                          <input type="file" class="form-control" id="image" >
                                                          <div id="upload-demo" style="margin-top: 20px"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6 text-left">
                                                                <select class="custom-select my-1 mr-sm-2" id="choosecategory">
                                                                    <option value="0">Category</option>
                                                                    <?php
                                                                        foreach( $cat_list as $cat ){
                                                                    ?>
                                                                            <option value="<?php echo $cat['cat_id'] ?>"><?php echo $cat['cat_name'] ?></option>
                                                                            
                                                                    <?php
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-6 text-right">
                                                                <button type="button" class="btn btn-primary my-1" id="submit-post">Share</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div> -->
                    </div>
                </div>
            </div>
            <!-- Post Form End -->
            
            <!-- Current Posts -->
            <div class="row current-posts">
                <?php echo $all_blog_view; ?>
                
            </div>
            <!-- Current Posts End -->
            <!-- Load more button -->
            <div class="text-center" id="load-more-parent-div">
                <button class="btn btn-primary btn-lg load-more" id="load-more-btn" data-load_more_index = "1">
                    Load more 
                    <i class="fas fa-angle-double-right load-more-i"></i>
                    <i class="fas fa-spinner fa-spin loading-more-i" style="display: none"></i>
                </button>
            </div>
            <!-- Load more button -->
        </div>
    </div>
    <!-- Right box -->
    <div class="box-3">

        <?php echo $right_bar_view; ?>
    </div>
    <!-- Right Box End -->
</div>
<script type="text/javascript">

    //upload image using croppie
   
    var resize = $('#upload-demo').croppie({
        enableExif: true,
        enableOrientation: true,    
        viewport: { // Default { width: 100, height: 100, type: 'square' } 
            width: 585,
            height: 388,
            type: 'square' //square
        },
        boundary: {
            width: 650,
            height: 450
        }
    });
    $('#image').on('change', function () { 
      var reader = new FileReader();
        reader.onload = function (e) {
          resize.croppie('bind',{
            url: e.target.result
          }).then(function(){
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
    });
    $('#submit-post').on('click', function (ev) {
        tinyMCE.activeEditor.isNotDirty = true;
        var wordcount = tinymce.activeEditor.plugins.wordcount;
        var title   = $("#post-form").children('.title-parent').children('#title').val();
        // var content = $('#sharewiththeworld').val();
        var content = tinyMCE.activeEditor.getContent();
        var cat_id = $('#choosecategory').val();
        var template = $('input[name="post_template"]:checked').val();
        // alert(template);
        if( title.length == 0 ){
            return alert("Please provide a title to the post.");
        }
        if( content.length == 0 ){
            return alert("Please provide a content to the post.");
        }
        if( cat_id == 0 ){
            return alert("Please provide a category to the post.");
        }
        resize.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            if( document.getElementById("image").files.length == 0 ){
                img = '';
            }
            $.ajax({
              url: "<?php echo site_url('front/add_post_db') ?>",
              type: "POST",
              data: {
                "img":img,
                "title":title,
                "content":content,
                "cat_id":cat_id,
                "template":template,
                },
              success: function (data) {
                // html = '<img src="' + img + '" />';
                // $("#preview-crop-image").html(html);
                $data_array = jQuery.parseJSON(data);
                if( $data_array.code == 1 ){
                    location.reload();
                }else{
                     console.log(data_array.message)
                }
                // console.log(data)
              },
              error : function(response ){
                console.log(response)
              }
            });//ajax
        });
    });//on click

    $(document).ready(function(){
        $("#load-more-btn").on('click', function(){
            var that = $(this);
            var load_more_index = that.attr('data-load_more_index');
            load_more_index = parseInt( load_more_index );
            // alert(load_more_index);

            $.ajax({
                beforeSend : function(xhr){
                  that.children('.load-more-i').hide();
                  that.children('.loading-more-i').show();
                },
                url: "<?php echo site_url('front/load_more_posts') ?>",
                type: "POST",
                data: {
                    "load_more_index":load_more_index,
                },
                success: function (data) {
                    console.log(data);
                    $data_array = jQuery.parseJSON(data);
                    if( $data_array.more_exist == 1 ){
                        that.parent('#load-more-parent-div').siblings('.current-posts').append($data_array.posts);
                        that.children('.load-more-i').show();
                        that.children('.loading-more-i').hide();
                    }else{
                        that.parent('#load-more-parent-div').siblings('.current-posts').append($data_array.posts);
                        that.hide();
                    }
                // console.log(data)
                    var new_load_more_index = load_more_index +1;
                    that.attr('data-load_more_index', new_load_more_index);
                },
                error : function(response ){
                    console.log(response)
                }
            });//ajax

        });
    });
</script>
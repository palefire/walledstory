     </div>
<!-- Side Main Black Box-->
    <div class="container side-main-box">
        <div class="row">
            <div class="col-12 search-button" data-toggle="modal" data-target="#searchModal">
                <a href="#"><i class="fas fa-search"></i></a>
            </div>
            <div class="col-12 option-button">
                <a href="#"><i class="fas fa-bars"></i></a>
            </div>
            <div class="col-12 toggle-button">
                <a href="#"><i class="fas fa-toggle-on"></i></a>
            </div>
        </div>
    </div>
    <!-- Side Main Black Box End-->

<style type="text/css">
    
    /*Left*/
    .modal .modal-dialog-aside{
        width: 350px;
        max-width:80%; height: 100%; margin:0;
        transform: translate(0); transition: transform .2s;
    }
    nav .nav{display: none;}

    .modal .modal-dialog-aside .modal-content{  height: inherit; border:0; border-radius: 0;}
    .modal .modal-dialog-aside .modal-content .modal-body{ overflow-y: auto }
    .modal.fixed-left .modal-dialog-aside{ margin-left:auto;  transform: translateX(100%); }
    .modal.fixed-right .modal-dialog-aside{ margin-right:auto; transform: translateX(-100%); }

    .modal.show .modal-dialog-aside{ transform: translateX(0);}
    

    @media only screen and (max-device-width: 360px){
        .nav{}
        body{ margin: 0 0 55px 0; }
    .nav{ display: block;position: fixed; bottom: 0; width: 100%; height: 55px; box-shadow: 0 0 3px rgba(0, 0, 0, 0.2); background-color: #ffffff; display: flex; overflow-x: auto; }
    .nav__link{ display: flex; flex-direction: column; align-items: center; justify-content: center; flex-grow: 1; min-width: 50px; overflow: hidden; white-space: nowrap; font-family: sans-serif; font-size: 13px; color: #000; text-decoration: none; -webkit-tap-highlight-color: transparent; transition: background-color 0.1s ease-in-out;}
    a.nav__link{text-decoration: none;}
    .nav__link:hover { background-color: #eeeeee; }
    .nav__link--active { color: #7c88d2; }
    .nav__icon { font-size: 18px; }
    .nav__icon.add_newpost{font-size: 40px}
    }
    @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        .nav{}
        body{ margin: 0 0 55px 0; }
    .nav{ display: block;position: fixed; bottom: 0; width: 100%; height: 55px; box-shadow: 0 0 3px rgba(0, 0, 0, 0.2); background-color: #ffffff; display: flex; overflow-x: auto; }
    .nav__link{ display: flex; flex-direction: column; align-items: center; justify-content: center; flex-grow: 1; min-width: 50px; overflow: hidden; white-space: nowrap; font-family: sans-serif; font-size: 13px; color: #000; text-decoration: none; -webkit-tap-highlight-color: transparent; transition: background-color 0.1s ease-in-out;}
    a.nav__link{text-decoration: none;}
    .nav__link:hover { background-color: #eeeeee; }
    .nav__link--active { color: #7c88d2; }
    .nav__icon { font-size: 18px; }
    .nav__icon.add_newpost{font-size: 40px}
    }

</style>


   <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
    <nav class="nav laptop-hide">
      <a href="#" class="nav__link" data-toggle="modal" data-target="#notificationModal">
        <i class="far fa-bell nav__icon"></i>
        <span class="nav__text">Notification</span>
      </a>
      <a href="<?php echo site_url() ?>" class="nav__link nav__link--active">
        <i class="fas fa-house-user nav__icon"></i>
        <span class="nav__text">Home</span>
      </a>
      <a href="#" class="nav__link" data-toggle="modal" data-target="#downloadtheappModal">
        <i class="fas fa-plus-circle nav__icon add_newpost"></i>
        <span class="nav__text"></span>
      </a>
      <a href="<?php echo site_url('favourites') ?>" class="nav__link" >
        <i class="fas fa-heart nav__icon"></i>
        <span class="nav__text">Favorites</span>
      </a>
      <a href="#" class="nav__link" data-toggle="modal" data-target="#mobilemenuModal">
        <i class="far fa-circle nav__icon"></i>
        <span class="nav__text">Go To</span>
      </a>
    </nav>

</body>

</html>

<script type="text/javascript">
    setInterval( function(){
        $.ajax({
          beforeSend : function(xhr){

          },
          url : "<?php echo site_url('front/fetch_unseen_notification_number') ?>",
          type : 'POST',
          data : {
                
          },
          success: function( data ){
              // console.log(data);
              if( data > 0 ){
                // console.log(data);
                $('.notification-label').removeClass('not-label-hide');
                $('.notification-label').addClass('not-label-show');
                $('.notification-label').html(data);
              }else{
                $('.notification-label').removeClass('not-label-show');
                $('.notification-label').addClass('not-label-hide');

              }
          },
          error: function(response){
              console.log(response);
          }
        });//ajax
    }, 3000 );
</script>
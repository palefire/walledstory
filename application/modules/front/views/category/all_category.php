<link rel="stylesheet" href="<?php echo base_url('assets/css/style-dark-mode.css?v=').microtime()?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css?v=').microtime()?>">
<style type="text/css">
    .ct-main-box{background-color: #000; width: calc(100% - 5vh); min-height: calc(100vh - 10vh); max-height: calc(100vh - 10vh); display: flex; flex-direction: column; 
            border-radius: 30px 0 0 30px;}
    .ct-left-side-bar{width: 60%; background-color: #112B46; height: 100%; border-radius: 0 30px 0 30px;}
    .ct-left-side-bar .side-bar-dp{padding: 3vh 0;}
    .ct-left-side-bar .side-bar-dp img{width: 75%; border-radius: 50%; margin-bottom: 1vh}
    .ct-left-side-bar .side-bar-dp span{font-size: 1.5rem; font-weight: 600; color: #fff; text-shadow: 1px 1px 2px #00000094;}
    .ct-left-side-bar .side-bar-menu-list{display: flex; flex-direction: column;}
    .ct-left-side-bar .side-bar-menu-list .side-bar-menu-list-item{display: flex; flex-direction: row; font-size: 0.8rem; color: #fff; padding: 1vh 1vh; cursor: pointer; margin-bottom: 2px;}
    .ct-left-side-bar .side-bar-menu-list .side-bar-menu-list-item:hover{background-color: #0f337a; border-radius: 10px;}
    .ct-left-side-bar .side-bar-menu-list .active{background-color: #0f337a; border-radius: 10px;}
    .ct-left-side-bar .side-bar-menu-list .side-bar-menu-list-item span{width: 30px; text-align: center;}
    .ct-left-side-bar .side-bar-menu-list .side-bar-menu-list-item span i{font-size: 0.8rem;}
    .ct-box-2{padding: 0 0 40px 0}
    .ct-box-2 .back-box-size-1{background-color: #112B46;height: 140px;width: 100%; margin-top:30px;border-radius: 30px;display: flex;justify-content: center;align-items:center;text-decoration: none;}
    .ct-box-2 .back-box-size-2{background-color: #112B46;height: 190px;width: 100%; margin-top:30px;border-radius: 30px;display: flex;justify-content: center;align-items:center;text-decoration: none;}
    .ct-box-2 .back-box-size-3{background-color: #112B46;height: 80px;width: 100%; margin-top:30px;border-radius: 30px;display: flex;justify-content: center;align-items:center;text-decoration: none;}
    .ct-box-2 .back-box-size-4{background-color: #112B46;height: 140px;width: 100%; margin-top:30px;border-radius: 30px;display: flex;justify-content: center;align-items:center;text-decoration: none;}
    .ct-box-2 .back-box-size-5{background-color: #112B46;height: 140px;width: 100%; margin-top:30px;border-radius: 30px;display: flex;justify-content: center;align-items:center;text-decoration: none;}
    .ct-box-2 .back-box-size-6{background-color: #112B46;height: 80px;width: 100%; margin-top:30px;border-radius: 30px;display: flex;justify-content: center;align-items:center;text-decoration: none;}
</style>
<div class="row box">

    <!-- Left box -->
    <div class="box-1">

        <!-- Left sidebar -->
        <div class="left-side-bar">
            <div class="container-fluid">
                <div class="row">

                    <!-- Profile Picture -->
                    <div class="col-12 side-bar-dp text-center">
                        <?php
                            if( $profile['show_image'] == 1 ){
                                if( $profile['user_image'] != null ){
                        ?>
                                    <img id="profile-image-left" src="<?php echo $profile['user_image'] ?>" alt="">
                        <?php            
                                }else{
                        ?>
                                    <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                        <?php
                                }
                        ?>
                            
                        <?php
                            }else{
                        ?>
                                <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                        <?php
                            }
                        ?>
                        <div></div>
                        <span><?php echo $header_data_array['fname'].' '.$header_data_array['lname'] ?> </span>
                    </div>
                    <!-- End Profile Picture -->

                    <div class="col-12 side-bar-menu-list">
                        <a href="<?php echo site_url() ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item active"><span><i class="fas fa-home"></i></span>Home
                            </div>
                        </a>
                        <a href="<?php echo site_url('profile') ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-user"></i></span>Profile
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none" data-toggle="modal" data-target="#notificationModal">
                            <div class="side-bar-menu-list-item">
                                <span>
                                    <i class="fas fa-bell"></i>
                                </span>
                                Notifications 
                                <label class="notification-label <?php 
                                    if( $notification['unseen']['number'] > 0 ){
                                        echo 'not-label-show';
                                    }else{
                                        echo 'not-label-hide';
                                    }

                                ?>"><?php echo $notification['unseen']['number'] ?></label>
                            </div>
                        </a>
                        <!-- <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-envelope"></i></span>Messages
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-star"></i></span>Favourites
                            </div>
                        </a> -->
                    </div>

                </div>
            </div>
        </div>
        <!-- Left sidebar end -->

    </div>
    <!-- Left Box End -->

    <!-- Middle box -->
    <div class="box-2 ct-box-2">
        <div class="text-white lead font-weight-bold">Categories</div>
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                     <div class="col-md-12" >
                        <a class="back-box-size-1" href="<?php echo site_url('category/psychology') ?>">
                            <div class="text-white lead">Psychology</div>
                        </a>
                    </div>
                    <div class="col-md-12">
                        <a class="back-box-size-2" href="<?php echo site_url('category/philosophy') ?>">
                            <div class="text-white lead">Philosophy</div>
                        </a>
                    </div>
                </div>           
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <a class="back-box-size-3" href="<?php echo site_url('category/cinema') ?>">
                            <div class="text-white lead">Cinema</div>
                        </a>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <a class="back-box-size-4" href="<?php echo site_url('category/book-review') ?>">
                                    <div class="text-white lead">Book Review</div>
                                    
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a class="back-box-size-5" href="<?php echo site_url('category/sports') ?>">
                                    <div class="text-white lead">Sports</div>     
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a class="back-box-size-6" href="<?php echo site_url('category/film-review') ?>">
                            <div class="text-white lead">Film Review</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                     <div class="col-md-12">
                        <a class="back-box-size-1" href="<?php echo site_url('category/politics') ?>">
                            <div class="text-white lead">Politics</div>
                        </a>
                    </div>
                    <div class="col-md-12">
                        <a class="back-box-size-2" href="<?php echo site_url('category/art') ?>">
                            <div class="text-white lead">Art</div>
                        </a>
                    </div>
                </div>           
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <a class="back-box-size-3" href="<?php echo site_url('category/media') ?>">
                            <div class="text-white lead">Media</div>
                        </a>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <a class="back-box-size-4" href="<?php echo site_url('category/culture') ?>">
                                    <div class="text-white lead">Culture</div>
                                    
                                </a>
                            </div>
                            <div class="col-md-6">
                                <a class="back-box-size-5" href="<?php echo site_url('category/society') ?>">
                                    <div class="text-white lead">Society</div>     
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a class="back-box-size-6" href="<?php echo site_url('category/design') ?>">
                            <div class="text-white lead">Design</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Right box -->
    <div class="box-3">

        <!-- Right sidebar-->
        <?php echo $right_bar_view; ?>
        <!-- Right Sidebar end -->

    </div>
    <!-- Right Box End -->
</div>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WalledStory</title>
    <link rel="icon" href="<?php echo $this->data['logo'] ?>" type="image/gif" sizes="16x16">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="assets/fontawesome/css/all.css" rel="stylesheet">


    <style type="text/css">
        .l-box .l-box-1 .l-header{padding:5vh 7vh 4vh 7vh}
        .l-form-fields form{padding: 2% 18%}
        .l-box .l-box-1 .l-header{padding:5vh 7vh 2vh 7vh}
        .l-form-fields form input{margin-bottom: 2vh}
        .l-form-fields form input{}
        @media only screen and (max-device-width: 360px){
            .l-box .l-box-2{display: none}
            .l-box .l-box-1 {width: 100%}
            .l-box .l-box-1 .l-header{padding: 3vh 7vh 3vh 7vh}
            .l-form-fields form{padding:10% 0px}
            .l-form span{font-size: 1.5em;}
            .l-form-fields form label{font-size: 1.2em;}
            .l-form-fields form button{font-size: 1.2em;}
            .l-form-fields p{font-size: 0.9em;}
        }
         @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
            .l-box .l-box-2{display: none}
            .l-box .l-box-1 {width: 100%}
            .l-box .l-box-1 .l-header{padding: 3vh 7vh 3vh 7vh}
            .l-form-fields form{padding:10% 0px}
            .l-form span{font-size: 1.5em;}
            .l-form-fields form label{font-size: 1.2em;}
            .l-form-fields form button{font-size: 1.2em;}
            .l-form-fields p{font-size: 0.9em;}
        }

    </style>
</head>

<body class="l-body">

    <!-- Main Box -->
    <div class="container l-main-box">

        <div class="row l-box">

            <!-- Left box -->
            <div class="l-box-1">
                <div class="container">
                    <div class="row">
                        <div class="col-12 l-header">
                            <div class="l-header-dot">
                                <div class="l-header-inner-dot"></div>
                            </div>
                            <div class="l-header-logo">
                                <a href="#"><img src="assets/files/ws-logo.png" class="rounded mx-auto d-block" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center l-form">
                            <span>Create Account</span>
                        </div>
                        <div class="col-12 l-form-fields">
                            <form method="POST" action="<?php echo site_url('front/login/register_user_to_db') ?>" autocomplete="off">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" class="form-control" id="email-check" aria-describedby="emailHelp" placeholder="Enter email" name="email">
                                    <span style="margin-bottom: 4vh; color: red; padding-left: 20px; font-size: 15px; display: none" id="email-exist-label">Email already Exists!</span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Phone Number</label>
                                    <input type="text" class="form-control" id="email-check" aria-describedby="emailHelp" placeholder="Enter 10 digit phone" name="phone">
                                    
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Username</label>
                                    <input type="text" class="form-control" id="username-check" aria-describedby="emailHelp" placeholder="Enter Username" name="username" style="margin-bottom: 4px">
                                    <span style="margin-bottom: 4vh; color: red; padding-left: 20px; font-size: 15px; display: none" id="username-exist-label">Username already Exists!</span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary" id="sign-up" disabled="true">Sign Up</button>
                                </div>
                                <p class="mb-0 text-center">Already have an account? <a href="<?php echo site_url('login') ?>"><b>SIGN IN</b></a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left Box End -->

            <!-- Right box -->
            <div class="l-box-2">
                <div class="l-read-more text-center">
                    <a href="">
                        <i class="fas fa-play-circle"></i>
                        <span>Read More</span>
                    </a>                    
                </div>
            </div>
            <!-- Right Box End -->
        </div>
    </div>

    <!--General Scripts-->

    <script src="assets/js/jquery-3.5.1.slim.min.js"></script>
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/thether.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url('assets-admin/vendor/jquery/jquery-3.3.1.min.js') ?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#username-check").on('keyup', function(){
                var username = $(this).val();
                var email = $("#email-check").val();
                // alert(username);
                $.ajax({
                    beforeSend : function(xhr){
                        
                    },
                    url : "<?php echo site_url('front/login/check_username') ?>",
                    type : 'POST',
                    data : {
                        'username' : username
                    },
                    success: function( data ){
                        console.log(data);
                        if( data == 1 ){
                            $.ajax({
                                beforeSend : function(xhr){
                                    
                                },
                                url : "<?php echo site_url('front/login/check_email') ?>",
                                type : 'POST',
                                data : {
                                    'email' : email
                                },
                                success : function(data){
                                    if( data == 1 ){
                                        $("#username-exist-label").hide();
                                        $("#sign-up").prop('disabled', '');
                                    }else{
                                        $("#username-exist-label").hide();
                                    }
                                    
                                },
                                error:function(response){
                                    console.log(respose);
                                }
                            });
                            
                        }else{
                            $("#username-exist-label").show();
                            $("#sign-up").prop('disabled', 'true');
                        }
                        
                    },
                    error: function(response){
                        console.log(response);
                    }
                });//ajax
            } );

            $("#email-check").on('keyup', function(){
                var email = $(this).val();
                var username = $("#username-check").val();
                // alert(username);
                $.ajax({
                    beforeSend : function(xhr){
                        
                    },
                    url : "<?php echo site_url('front/login/check_email') ?>",
                    type : 'POST',
                    data : {
                        'email' : email
                    },
                    success: function( data ){
                        console.log(data);
                        if( data == 1 ){
                            $.ajax({
                                beforeSend : function(xhr){
                                    
                                },
                                url : "<?php echo site_url('front/login/check_username') ?>",
                                type : 'POST',
                                data : {
                                    'username' : username
                                },
                                success: function( data ){
                                    console.log(data);
                                    if( data == 1 ){
                                        $("#email-exist-label").hide();
                                        $("#sign-up").prop('disabled', '');
                                    }else{
                                        $("#email-exist-label").hide();
                                    }
                                    
                                },
                                error: function(response){
                                    console.log(response);
                                }
                            });//ajax
                        }else{
                            $("#email-exist-label").show();
                            $("#sign-up").prop('disabled', 'true');
                        }
                        
                    },
                    error: function(response){
                        console.log(response);
                    }
                });//ajax
            } );

        });
    </script>
</body>

</html>
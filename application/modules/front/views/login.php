<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WalledStory</title>
    <link rel="icon" href="<?php echo $this->data['logo'] ?>" type="image/gif" sizes="16x16">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="assets/fontawesome/css/all.css" rel="stylesheet">
    <style type="text/css">
        
        @media only screen and (max-device-width: 360px){
            .l-box .l-box-2{display: none}
            .l-box .l-box-1 {width: 100%}
            .l-header{padding: 3vh 7vh 5vh 7vh}
            .l-form-fields form{padding:22% 0px}
            .l-form span{font-size: 1.5em;}
            .l-form-fields form label{font-size: 1.2em;}
            .l-form-fields form button{font-size: 1.2em;}
            .l-form-fields p{font-size: 0.9em;}
        }
        @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
            .l-box .l-box-2{display: none}
            .l-box .l-box-1 {width: 100%}
            .l-header{padding: 3vh 7vh 5vh 7vh}
            .l-form-fields form{padding:22% 0px}
            .l-form span{font-size: 1.5em;}
            .l-form-fields form label{font-size: 1.2em;}
            .l-form-fields form button{font-size: 1.2em;}
            .l-form-fields p{font-size: 0.9em;}
        }
    </style>
</head>

<body class="l-body">
    <div style="position: absolute; z-index: 999999; left: 20px; top: 26vh; ">
        <?php 
            $login_failed = $this->session->flashdata('login_failed');
            if( $login_failed ){
         ?>
            <div class="alert alert-dismissible alert-warning">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4 class="alert-heading">Oops!</h4>
                <p class="mb-0"><?php echo $login_failed; ?></p>
            </div>
        <?php } 
        ?>
    </div>
    <!-- Main Box -->
    <div class="container l-main-box">

        <div class="row l-box">

            <!-- Left box -->
            <div class="l-box-1">
                <div class="container">
                    <div class="row">
                        <div class="col-12 l-header">
                            <div class="l-header-dot">
                                <div class="l-header-inner-dot"></div>
                            </div>
                            <div class="l-header-logo">
                                <a href="#"><img src="assets/files/ws-logo.png" class="rounded mx-auto d-block" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center l-form">
                            <span>Login</span>
                        </div>
                        
                        <div class="col-12 l-form-fields">
                            <form action="<?php echo site_url('front/login/verify_login') ?>" method="POST">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="username">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary">Sign In</button>
                                </div>
                                <p class="mb-0 text-center">Do not have an account? <a href="<?php echo site_url('signup') ?>"><b>SIGN UP</b></a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left Box End -->

            <!-- Right box -->
            <div class="l-box-2">
                <div class="l-read-more text-center">
                    <a href="">
                        <i class="fas fa-play-circle"></i>
                        <span>Read More</span>
                    </a>                    
                </div>
            </div>
            <!-- Right Box End -->
        </div>
    </div>

    <!--General Scripts-->
    <script src="assets/js/jquery-3.5.1.slim.min.js"></script>
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/thether.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

</body>

</html>
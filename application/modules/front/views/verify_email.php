<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WalledStory</title>
    <link rel="icon" href="<?php echo $this->data['logo'] ?>" type="image/gif" sizes="16x16">
    <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="assets/fontawesome/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/stepper.css">
    <style type="text/css">
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
          -webkit-appearance: none;
          margin: 0;
        }
        input[type=number] {
          -moz-appearance: textfield;
        }
        .stepper{display: none;}
        .stepper .stepper-single{float: left; margin-right: 3%; margin-left: 4%; font-size: 10px;margin-left: 5%; margin-right: 4%}
        .stepper .stepper-single i{margin-left: 35%; font-size: 18px; color: #7b88d1}
        .stepper .stepper-line{width: 40px; margin-top: 12px; height: 7px}
        .l-box .l-box-1 .l-header{padding:5vh 7vh 4vh 7vh}
        .verify-form{padding: 17% 18%}
        @media only screen and (max-device-width: 360px){
            .l-box .l-box-2{display: none}
            .l-box .l-box-1 {width: 100%}
            .l-header{padding: 3vh 7vh 5vh 7vh}
            .l-form-fields form{padding:10% 0px}
            .l-form span{font-size: 1.5em;}
            .l-form-fields form label{font-size: 1.2em;}
            .l-form-fields form button{font-size: 1.2em;}
            .l-form-fields p{font-size: 0.9em;}
        }
        @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
            .l-box .l-box-2{display: none}
            .l-box .l-box-1 {width: 100%}
            .l-header{padding: 3vh 7vh 5vh 7vh}
            .l-form-fields form{padding:10% 0px}
            .l-form span{font-size: 1.5em;}
            .l-form-fields form label{font-size: 1.2em;}
            .l-form-fields form button{font-size: 1.2em;}
            .l-form-fields p{font-size: 0.9em;}
        }
    </style>
</head>

<body class="l-body">
    <div style="position: absolute; z-index: 999999; left: 20px; top: 26vh; ">
        <?php 
            $login_failed = $this->session->flashdata('login_failed');
            if( $login_failed ){
         ?>
            <div class="alert alert-dismissible alert-warning">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4 class="alert-heading">Opps!</h4>
                <p class="mb-0"><?php echo $login_failed; ?></p>
            </div>
        <?php } 
        ?>
    </div>
    <!-- Main Box -->
    <div class="container l-main-box">

        <div class="row l-box">

            <!-- Left box -->
            <div class="l-box-1">
                <div class="container">
                    <div class="row">
                        <div class="col-12 l-header">
                            <div class="l-header-dot">
                                <div class="l-header-inner-dot"></div>
                            </div>
                            <div class="l-header-logo">
                                <a href="#"><img src="assets/files/ws-logo.png" class="rounded mx-auto d-block" alt=""></a>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-12">
                            <div class="stepper">
                                <div class="stepper-single">
                                    <i class="far fa-check-circle"></i>
                                    <p style="text-decoration: underline;">Verify Email</p>
                                </div>
                                <div class="stepper-line"></div>
                                <div class="stepper-single">
                                    <i class="far fa-check-circle"></i>
                                    <p>Profile Info</p>
                                </div>
                                <div class="stepper-line"></div>
                                <div class="stepper-single">
                                    <i class="far fa-check-circle"></i>
                                    <p>Profile Image</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center l-form">
                            <span>Verify Email</span>
                        </div>
                        <div class="col-12 l-form-fields">
                            <form method="POST" action="<?php echo site_url('front/login/verify_email_in_db') ?>" class="verify-form">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Enter the OTP</label>
                                    <input type="number" class="form-control" id="otp" aria-describedby="emailHelp" placeholder="Enter your OTP" name="otp" style="-webkit-appearance: none; margin: 0;">
                                </div>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary" id="sign-up" >Verify</button>
                                </div>
                                <p class="mb-0 text-center">Did Not Receive the OTP? <a href="<?php// echo site_url('login') ?>"><b>Resend OTP</b></a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Left Box End -->

            <!-- Right box -->
            <div class="l-box-2">
                <div class="l-read-more text-center">
                    <a href="">
                        <i class="fas fa-play-circle"></i>
                        <span>Read More</span>
                    </a>                    
                </div>
            </div>
            <!-- Right Box End -->
        </div>
    </div>

    <!--General Scripts-->

    <script src="assets/js/jquery-3.5.1.slim.min.js"></script>
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/thether.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="<?php echo site_url('assets-admin/vendor/jquery/jquery-3.3.1.min.js') ?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
           
        });
    </script>
</body>

</html>
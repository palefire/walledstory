<link rel="stylesheet" href="<?php echo base_url('assets/css/style-dark-mode.css?v=').microtime()?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css?v=').microtime()?>">
<style type="text/css">
    .active-post-content{width: 100%;} 
    @media only screen and (max-device-width: 360px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important }
       /* .croppie-container .cr-boundary{width: 278px !important}
        .croppie-container .cr-image, .croppie-container .cr-overlay, .croppie-container .cr-viewport{width: 278px !important}*/
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
    }
    @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important }
       /* .croppie-container .cr-boundary{width: 278px !important}
        .croppie-container .cr-image, .croppie-container .cr-overlay, .croppie-container .cr-viewport{width: 278px !important}*/
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
    }
</style>
<div class="row box">

    <!-- Left box -->
    <div class="box-1">

        <!-- Left sidebar -->
        <div class="pro-left-side-bar">
            <!-- Profile Picture -->
            <div class="side-bar-dp text-center">
                <?php
                    if( $profile['show_image'] == 1 ){
                        if( $profile['user_image'] != null ){
                ?>
                            <img id="profile-image-left" src="<?php echo $profile['user_image'] ?>" alt="">
                <?php            
                        }else{
                ?>
                            <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                <?php
                        }
                ?>
                    
                <?php
                    }else{
                ?>
                        <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                <?php
                    }
                ?>
                <span>
                    About 
                     <?php
                        if( $profile['user_info_fname'] == null ){
                            echo $profile['user_name'];
                        }else{
                            if( $profile['show_name'] == 1 ){
                                echo $profile['user_info_fname'];
                            }else{
                                echo $profile['user_name'];
                            }
                            
                        }
                    ?>
                </span>
                <p class="text-left">
                    <?php 
                        if( $profile['show_bio'] == 1 ){
                            echo $profile['user_bio'];
                        }
                    ?>
                </p>
            </div>
            <!-- End Profile Picture -->
            <!-- <div class="side-bar-dp-footer text-left">
                <p>
                    Followers&nbsp;&nbsp;&nbsp;&nbsp;20k
                    <br>
                    Following&nbsp;&nbsp;&nbsp;&nbsp;200
                </p>
            </div> -->
        </div>
        <!-- Left sidebar end -->
        <div class="pro-left-side-bar-arrow" data-toggle="modal" data-target="#notificationModal">
            <span>&#10095;</span>
        </div>
    </div>
    <!-- Left Box End -->

    <!-- Middle box -->
    <div class="box-2">
        <!-- Popular Stories -->
        <div class="pro-cover" style="background-image: url('<?php 
            if( $profile["user_cover_image"] != null ){
                    echo $profile["user_cover_image"];
                }else{
                   echo $this->data["profile_cover_placeholders"] ; 
                }
            
        ?>');">
            <div class="overlay"></div>
            <div class="pro-cover-content">
                <div class="container">
                    <div class="row">
                        <div class="col-8 profile-info text-left">
                            <span>
                                <?php
                                    if( $profile['user_info_fname'] == null ){
                                        echo $profile['user_name'].', ';
                                    }else{
                                        if( $profile['show_name'] == 1 ){
                                            echo $profile['user_info_fname'].' '.$profile['user_info_lname'].', ';
                                        }else{
                                            echo $profile['user_name'].', ';
                                        }
                                        
                                    }
                                ?> 
                                <?php 
                                    if( $profile['show_profession'] == 1 and $profile['user_profession']!=''){
                                        echo $profile['user_profession'].', ';
                                    }
                                ?> 
                                <?php 
                                    if( $profile['show_address'] == 1 and $profile['user_info_city']!=''){
                                        echo $profile['user_info_city'];
                                    }
                                ?>
                            </span>
                        </div>
                        <!-- <div class="col-4 profile-connect-buttons">
                            <button class="btn btn-md">Follow&nbsp;&nbsp;<i
                                    class="fas fa-user-plus"></i></button>
                            <button class="btn btn-md">Message&nbsp;&nbsp;<i
                                    class="fas fa-paper-plane"></i></button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Popular Stories end -->
        <div class="container">
            <div class="row posts">
                <div class="col-12"><label for="post-box">
                    <?php 
                        if( $profile['user_info_fname'] == null ){
                            echo $profile['user_name'];
                        }else{
                            if( $profile['show_name'] == 1 ){
                                echo $profile['user_info_fname'];
                            }else{
                                echo $profile['user_name'];
                            }
                            
                        }
                ?>
                's Posts</label></div>
            </div>

            <!-- Current Posts -->
            <div class="row current-posts">
               <?php echo $all_blog_view; ?>
            </div>
            <div class="text-center" id="load-more-parent-div">
                <button class="btn btn-primary btn-lg load-more" id="load-more-btn" data-load_more_index = "1" data-profie_id = <?php echo $profile['user_id'] ?>>
                    Load more 
                    <i class="fas fa-angle-double-right load-more-i"></i>
                    <i class="fas fa-spinner fa-spin loading-more-i" style="display: none"></i>
                </button>
            </div>
            <!-- Current Posts End -->
        </div>
    </div>

    <!-- Right box -->
    <div class="box-3">

        <!-- Right sidebar-->
        <?php echo $right_bar_view; ?>
        <!-- Right Sidebar end -->

    </div>
    <!-- Right Box End -->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#load-more-btn").on('click', function(){
            var that = $(this);
            var load_more_index = that.attr('data-load_more_index');
            var profie_id = that.attr('data-profie_id');
            load_more_index = parseInt( load_more_index );
            // alert(load_more_index);

            $.ajax({
                beforeSend : function(xhr){
                  that.children('.load-more-i').hide();
                  that.children('.loading-more-i').show();
                },
                url: "<?php echo site_url('front/profile/load_more_others_posts') ?>",
                type: "POST",
                data: {
                    "load_more_index":load_more_index,
                    "profie_id":profie_id,
                },
                success: function (data) {
                    console.log(data);
                    $data_array = jQuery.parseJSON(data);
                    if( $data_array.more_exist == 1 ){
                        that.parent('#load-more-parent-div').siblings('.current-posts').append($data_array.posts);
                        that.children('.load-more-i').show();
                        that.children('.loading-more-i').hide();
                    }else{
                        that.parent('#load-more-parent-div').siblings('.current-posts').append($data_array.posts);
                        that.hide();
                    }
                // console.log(data)
                    var new_load_more_index = load_more_index +1;
                    that.attr('data-load_more_index', new_load_more_index);
                },
                error : function(response ){
                    console.log(response)
                }
            });//ajax

        });
    });
</script>
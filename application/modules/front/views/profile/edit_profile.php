<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<style type="text/css">

ul.nav-pills{list-style: none;display: inline-flex;margin-left: -40px}
.new-pils li a{background-color: #000;margin-right: 20px;padding: 10px;border-radius: 4px;color: #fff;text-decoration: none;letter-spacing: 1px;position: relative;}
.new-pils li a.active{background-color: #7C88D2}

.switch { position: relative; display: inline-block; width: 60px; height: 34px; } 
.switch input { opacity: 0; width: 0; height: 0; } 
.slider { position: absolute; cursor: pointer; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; -webkit-transition: .4s; transition: .4s; } 
.slider:before { position: absolute; content: ""; height: 26px; width: 26px; left: 4px; bottom: 4px; background-color: white; -webkit-transition: .4s; transition: .4s; } 
input:checked + .slider { background-color: #7C88D2; } 
input:focus + .slider { box-shadow: 0 0 1px #7C88D2; } 
input:checked + .slider:before { -webkit-transform: translateX(26px); -ms-transform: translateX(26px); transform: translateX(26px); } /* Rounded sliders */ 
.slider.round { border-radius: 34px; } 
.slider.round:before { border-radius: 50%; }

@media only screen and (max-device-width: 360px){
    body{padding: 0px; margin: 0 0 55px 0;}
    .box-1{display: none}
    .box-3{display: none}
    .side-main-box{display: none}
    .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

    .box .box-2{width: 100%}
    .main-header{padding: 1vh 1vh 1vh 1vh}
    .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
    .main-header .main-header-left .main-header-image a img{height: 2vh}
    .main-header .main-header-right{width:20%;justify-content: space-evenly;}
    .main-header .main-header-right .main-header-logout{display: none}
    .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

    .stories .story-box{max-width: 100%; min-width: 20%}
    .stories .story-box span{display: none}

    .ws-modal{max-width:300px !important }
   /* .croppie-container .cr-boundary{width: 278px !important}
    .croppie-container .cr-image, .croppie-container .cr-overlay, .croppie-container .cr-viewport{width: 278px !important}*/
    .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
    }
    @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
    body{padding: 0px; margin: 0 0 55px 0;}
    .box-1{display: none}
    .box-3{display: none}
    .side-main-box{display: none}
    .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

    .box .box-2{width: 100%}
    .main-header{padding: 1vh 1vh 1vh 1vh}
    .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
    .main-header .main-header-left .main-header-image a img{height: 2vh}
    .main-header .main-header-right{width:20%;justify-content: space-evenly;}
    .main-header .main-header-right .main-header-logout{display: none}
    .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

    .stories .story-box{max-width: 100%; min-width: 20%}
    .stories .story-box span{display: none}

    .ws-modal{max-width:300px !important }
   /* .croppie-container .cr-boundary{width: 278px !important}
    .croppie-container .cr-image, .croppie-container .cr-overlay, .croppie-container .cr-viewport{width: 278px !important}*/
    .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
    }


</style>



<div class="row box">
	<!-- Left box -->
	<div class="box-1">
        <!-- Left sidebar -->
        <div class="left-side-bar">
            <div class="container-fluid">
                <div class="row">

                    <!-- Profile Picture -->
                    <div class="col-12 side-bar-dp text-center">
                    	<?php
		                    if( $profile['show_image'] == 1 ){
		                        if( $profile['user_image'] != null ){
		                ?>
		                            <img id="profile-image-left" src="<?php echo $profile['user_image'] ?>" alt="">
		                <?php            
		                        }else{
		                ?>
		                            <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
		                <?php
		                        }
		                ?>
		                    
		                <?php
		                    }else{
		                ?>
		                        <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
		                <?php
		                    }
		                ?>
                        <span><?php echo $header_data_array['fname'].' '.$header_data_array['lname'] ?> </span>
                    </div>
                    <!-- End Profile Picture -->

                    <div class="col-12 side-bar-menu-list">
                        <a href="<?php echo site_url() ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item active"><span><i class="fas fa-home"></i></span>Home
                            </div>
                        </a>
                        <a href="<?php echo site_url('profile') ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-user"></i></span>Profile
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-bell"></i></span>Notifications
                            </div>
                        </a>
                       <!--  <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-envelope"></i></span>Messages
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-star"></i></span>Favourites
                            </div>
                        </a> -->
                    </div>

                </div>
            </div>
        </div>
        <!-- Left sidebar end -->
    </div>

    <!-- Middle box -->
    <div class="box-2">
    	<div style="position: absolute; z-index: 999999; right: 320px; top: 6vh; ">
	        <?php 
	            $profile_updated = $this->session->flashdata('profile_updated');
	            if( $profile_updated ){
	         ?>
	            <div class="alert alert-dismissible alert-success">
	                <button type="button" class="close" data-dismiss="alert">&times;</button>
	                <h4 class="alert-heading">Great!</h4>
	                <p class="mb-0"><?php echo $profile_updated; ?></p>
	            </div>
	        <?php } 
	        ?>
	        <?php 
	            $profile_updated_fail = $this->session->flashdata('profile_updated_fail');
	            if( $profile_updated_fail ){
	         ?>
	            <div class="alert alert-dismissible alert-danger">
	                <button type="button" class="close" data-dismiss="alert">&times;</button>
	                <h4 class="alert-heading">Great!</h4>
	                <p class="mb-0"><?php echo $profile_updated_fail; ?></p>
	            </div>
	        <?php } 
	        ?>
	    </div>
    	<div class="container">
    		<div class="row">
    			<div class="col-12">
    				<h3>Edit Profile</h3>
    			</div>
    			<div class="col-12 mt-2">
    				<ul class="nav-pills mb-3 new-pils" id="pills-tab" role="tablist">
					  <li class="nav-item" role="presentation">
					    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Personal Info</a>
					  </li>
					  <li class="nav-item" role="presentation">
					    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Contact Info</a>
					  </li>
					  
					</ul>
					<div class="tab-content" id="pills-tabContent">
					  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
					  	<form method="POST" action="<?php echo site_url('front/save_profile_personal_info') ?>">
							<div class="row">
								<div class="col-md-5 form-group">
									<label>First Name</label>
									<input type="text" name="user_info_fname" class="form-control" value="<?php echo $profile['user_info_fname'] ?>">
								</div>
								<div class="col-md-5 form-group">
									<label>Last Name</label>
									<input type="text" name="user_info_lname" class="form-control" value="<?php echo $profile['user_info_lname'] ?>">
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Show</label>
										<label class="switch">
										  <input type="checkbox" value="1" <?php if($profile['show_name'] == '1'){ echo 'checked';} ?> name="show_name">
										  <span class="slider round"></span>
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
									  <div class="form-group">
									    <label for="exampleFormControlSelect1">Gender</label>
									    <select class="form-control" id="exampleFormControlSelect1"name="user_info_gender">
									      <option value="1" <?php if( $profile['user_info_gender'] == '1' ){ echo 'selected';} ?> >Male</option>
									      <option value="2" <?php if( $profile['user_info_gender'] == '2' ){ echo 'selected';} ?>>Female</option>
									      <option value="3" <?php if( $profile['user_info_gender'] == '3' ){ echo 'selected';} ?>>Do not want to specify</option>
									    </select>
									  </div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Show</label>
										<label class="switch">
										  <input type="checkbox" value="1"<?php if($profile['show_gender'] == '1'){ echo 'checked';} ?> name="show_gender">
										  <span class="slider round"></span>
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
								  <div class="form-group">
								    <label for="exampleFormControlTextarea1">Bio</label>
								    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="user_bio"><?php echo $profile['user_bio'] ?></textarea>
								  </div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Show</label>
										<label class="switch">
										  <input type="checkbox" value="1" <?php if($profile['show_bio'] == '1'){ echo 'checked';} ?> name="show_bio">
										  <span class="slider round"></span>
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-10">
								  <div class="form-group">
								    <label for="exampleFormControlTextarea1">Proffesion</label>
								    <input type="text" name="user_profession" class="form-control" value="<?php echo $profile['user_profession'] ?>">
								  </div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label>Show</label>
										<label class="switch">
										  <input type="checkbox" value="1" <?php if($profile['show_profession'] == '1'){ echo 'checked';} ?> name="show_profession">
										  <span class="slider round"></span>
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="submit" name="" class="btn btn-primary float-right" value="Save">
								</div>
							</div>
						</form>
					  </div>
					  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
					  	<form method="POST" action="<?php echo site_url('front/save_profile_contact_info') ?>">
					     	<div class="row">
					     		<div class="col-md-12">
									<div class="form-group float-right">
										<label class="mr-2">Show</label>
										<label class="switch">
										  <input type="checkbox" value="1" <?php if($profile['show_address'] == '1'){ echo 'checked';} ?> name="show_address">
										  <span class="slider round"></span>
										</label>
									</div>
					     		</div>
					     	</div>
					     	<div class="row">
					     		<div class="col-md-12 form-group">
									<label>Phone No</label>
									<input type="text" name="user_info_phone" class="form-control" value="<?php echo $profile['user_info_phone'] ?>">
								</div>
								<div class="form-group col-md-12">
								    <label for="exampleFormControlTextarea1">Address</label>
								    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="user_info_address"><?php echo $profile['user_info_address'] ?></textarea>
								</div>
					     	</div>
					     	<div class="row">
					     		<div class="col-md-3 form-group">
									<label>City</label>
									<input type="text" name="user_info_city" class="form-control" value="<?php echo $profile['user_info_city'] ?>">
								</div>
								<div class="col-md-3 form-group">
									<label>State</label>
									<input type="text" name="user_info_state" class="form-control" value="<?php echo $profile['user_info_state'] ?>">
								</div>
								<div class="col-md-3 form-group">
									<label>Zip</label>
									<input type="text" name="user_info_zip" class="form-control" value="<?php echo $profile['user_info_zip'] ?>">
								</div>
								<div class="col-md-3 form-group">
									<label>Country</label>
									<input type="text" name="user_info_country" class="form-control" value="<?php echo $profile['user_info_country'] ?>">
								</div>
					     	</div>
					     	<div class="row">
								<div class="col-md-12">
									<input type="submit" name="" class="btn btn-primary float-right" value="Save">
								</div>
							</div>
						</form>
					  </div>
					 
					</div>

    			</div>
    		</div>

    	</div>
    </div>
    <!-- Right box -->
    <div class="box-3">
        <!-- Right sidebar-->
        <?php echo $right_bar_view; ?>
        <!-- Right Sidebar end -->
    </div>
</div>


<?php 
	if($notifications){
		foreach( $notifications as $notification ){
?>

	 <a href="#"  class="one-notification <?php if( $notification['not_status'] == '0' or $notification['not_status'] == '1' ){ echo 'dj not-unseen'; } ?>" style="font-size:14px;display: block;border-bottom: 1px solid #ddd;color: #000;text-decoration: none;padding: 10px 10px" data-url="<?php echo $notification['not_url'] ?>" data-not_id = "<?php echo $notification['not_id'] ?>">
	 	<span style="height: 16px;width: 16px;margin-right: 6px">
	 		<?php echo $this->data['notification_icons'][$notification['not_type']] ?>
	 		
	 	</span>
	 	<span>
	 		<?php echo $notification['content'] ?>
	 	</span>
	 </a>
<?php
		}
	}else{
?>
	<p>No Notification.</p>
<?php
	}
?>
<script type="text/javascript">
	$('.modal-body').on('click', '.one-notification', function(e){
		e.preventDefault();
		var notification_id = $(this).data("not_id");
      	var url             = $(this).data("url");
      	// alert(notification_id);
      	$.ajax({
          beforeSend : function(xhr){
            $('#ajax-load').show();
          },
          url : "<?php echo site_url('front/change_notification_status_to_read') ?>",
          type : 'POST',
          data : {
              'notification_id' : notification_id,
          },

          success: function( data ){
              console.log(data);
              if( data == 1 ){
                window.location.href = `${url}`;
              }else{
                alert('Ajax error')
                                      }


          },
          error: function(response){
              console.log(response);
          }
      });//ajax
	});
</script>
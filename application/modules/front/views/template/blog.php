<style>
    .active-post-content{width: 100%;} 
    .active-post-content-footer{position: relative; height: 32px}
    .active-post-content-footer a:hover{text-decoration: none;}
    .active-post-content-footer a p{font-size: 10px; text-align: center; margin-top: -11px; margin-bottom: -4px; text-decoration: none;}
    .like-list{display: none; position: absolute; width:120px; height: auto; background:rgba(355,355,355,0.9); bottom:45px;left:-50px; color:#000; font-size:13px; padding-top: 10px}
    .like-list ul{list-style: none; margin-left: -20px}
    .like-list ul li{}
    .like-btn:hover .like-list{display: block;}
</style>
<?php
    $CI =& get_instance();
    $CI->load->model('front/frontmodel','frontmodel');
    // echo $this->user_id;
    if( !empty($posts) ){
        foreach( $posts as $post ){
            // print_r($post);
            $check_like = $CI->frontmodel->check_like( $post['post_id'], $this->user_id );
            $like_count = $CI->frontmodel->like_count( $post['post_id'] );

            $prelim_like_list = $CI->frontmodel->fetch_like_list( $post['post_id'], 8, 0 );
            $check_like_exceeds_eight = $CI->frontmodel->check_like_exceeds_eight( $post['post_id'] );


            $check_favourite = $CI->frontmodel->check_favourite( $post['post_id'], $this->user_id );

?>
            <div class="col-12 active-posts">
                <!-- <img class="posts-owner-dp" src="<?php echo $post['user_image'] ?>" alt=""> -->
                <?php
                    if( $post['show_image'] == 1 ){
                        if( $post['user_image'] != null ){
                ?>
                            <img  class="posts-owner-dp" src="<?php echo $post['user_image'] ?>" alt="">
                <?php            
                        }else{
                ?>
                            <img class="posts-owner-dp" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                <?php
                        }
                ?>
                    
                <?php
                    }else{
                ?>
                        <img class="posts-owner-dp" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                <?php
                    }
                ?>
                <div class="active-post-content">
                    <div class="active-post-content-header">
                        <div class="info">
                            <a href="<?php echo site_url('profile/').$post['user_name'] ?>" style="text-decoration: none;">
                                <h5>
                                    <?php 
                                        if(  $post['show_name'] == 0 or  $post['user_info_fname'] == null){
                                            echo $post['user_name'];
                                        }else{
                                            echo $post['user_info_fname'].' '.$post['user_info_lname'] ;
                                        }
                                         
                                    ?>
                                </h5>
                            </a>
                            <span>Posted <?php echo $post['post_time'] ?></span>
                        </div>
                        <div class="three-dots" data-toggle="modal" data-target="#postmenuModal" data-post_id="<?php echo $post['post_id'] ?>" t="#postmenuModal"  data-user_token="<?php echo $this->session->userdata['wst23xyzsdfretw89lk_user_token'] ?>" data-author = "<?php echo $post['user_token'] ?>" data-post_slug="<?php echo $post['post_slug'] ?>" style="cursor: pointer;">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                    <div class="active-post-content-body">
                        <?php if( $post['featured_img_url'] != null ){ ?>
                                <a href="<?php echo site_url('post/').$post['post_slug'] ?>"> <img class="content-body-image" src="<?php echo $post['featured_img_url'] ?>" alt=""></a>
                        <?php  } ?>
                        <h5><a href="<?php echo site_url('post/').$post['post_slug'] ?>"><?php echo $post['post_title'] ?></a></h5>
                        <p>
                            <?php echo $post['post_content'] ?>
                        </p>
                    </div>
                    <div class="active-post-content-footer">
                        <div class="fa-1x ws-like-spinner">
                            <i class="fas fa-spinner fa-spin" style="color: #5164CB"></i>
                        </div>
                        <a href="#" data-post_id="<?php echo $post['post_id'] ?>" data-user_token="<?php echo $this->session->userdata['wst23xyzsdfretw89lk_user_token'] ?>" data-like_count="<?php echo $like_count ?>" data-author_id="<?php echo $post['user_id'] ?>" data-author_token="<?php echo $post['user_token'] ?>" data-post_url = "<?php echo site_url('post/').$post['post_slug'] ?>" class="like-btn" style="position: relative;">
                            <p><?php echo $like_count; ?></p>

                            <i class="
                            <?php 
                                if( $check_like ){
                                    echo 'fas';
                                }else{
                                    echo 'far';
                                }
                             ?> 
                            fa-heart"></i>
                            <div class="like-list">
                                <ul>
                                    <?php
                                        if( $prelim_like_list ){
                                            foreach ($prelim_like_list as $key => $person) {
                                                if( $person['show_name'] == '0'  ){
                                    ?>
                                                    <li><?php echo $person['user_name'] ?></li>
                                    <?php
                                                }else{
                                                    if( $person['user_info_fname'] == null or  $person['user_info_lname'] == null){
                                    ?>
                                                        <li><?php echo $person['user_name'] ?></li>
                                    <?php
                                                    }else{
                                    ?>
                                                        <li><?php echo $person['user_info_fname'].' '.$person['user_info_lname'] ?></li>
                                    <?php
                                                    }
                                    ?>

                                    <?php                
                                                }
                                            }
                                        }else{
                                    ?>
                                            <li>No likes</li>
                                    <?php
                                        }
                                        if( $check_like_exceeds_eight ){
                                    ?>
                                 
                                            <li data-toggle="modal" data-target="#underdevelopmentModal">See More</li>
                                    <?php
                                        }
                                    ?>
                                </ul>

                            </div>
                        </a>


                        <!-- <a href="#"><i class="fas fa-comment"></i></a> -->
                        <?php
                        if( $post['user_id'] != $this->user_id ){
                        ?>
                        <div class="fa-1x ws-favourite-spinner">
                            <i class="fas fa-spinner fa-spin" style="color: #5164CB"></i>
                        </div>
                        <a href="#" data-post_id="<?php echo $post['post_id'] ?>" data-author_id="<?php echo $post['user_id'] ?>" class="favourite-btn"><i class="
                            <?php 
                                if( $check_favourite ){
                                    echo 'fas';
                                }else{
                                    echo 'far';
                                }
                            ?>  
                            fa-star"></i>
                        </a>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
<?php
        }
    }else{
?>
    <div class="col-12 active-posts">
        <div class="active-post-content">
            <div class="active-post-content-body">
                
                <p>
                    No More Posts to show.
                </p>
            </div>
            
        </div>
    </div>
<?php
    }
?>

<div class="modal fade" id="postmenuModal" tabindex="-1" aria-labelledby="postmenuModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="postmenuModalLabel">Post Menu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul id="post-modal-ul">
            
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $('.like-btn').on('click', function(e){
        e.preventDefault();
        var that = $(this);
        var post_id = that.attr('data-post_id');
        var user_token = that.attr('data-user_token');
        var like_count = that.attr('data-like_count');
        var author_id = that.attr('data-author_id');
        var author_token = that.attr('data-author_token');
        var post_url = that.attr('data-post_url');
        // var post_url = window.location.href;
        like_count = parseInt( like_count );
        var add_like_count = like_count + 1;
        var minus_like_count = like_count - 1;
        // alert( post_url );
        $.ajax({
            beforeSend : function(xhr){
              that.hide();
              that.siblings('.ws-like-spinner').show();
            },
            url: "<?php echo site_url('front/check_like') ?>",
            type: "POST",
            data: {
                "post_id"       :post_id,
                
            },
            success: function (data) {
                // console.log(data)
                if( data == 0 ){
                    // send like request
                    $.ajax({
                        beforeSend : function(xhr){
                          that.hide();
                          
                        },
                        url: "<?php echo site_url('front/add_like') ?>",
                        type: "POST",
                        data: {
                            "post_id":post_id,
                            "author_id"     :author_id,
                            "author_token"  :author_token,
                            "post_url"      :post_url,
                        },
                        success: function (data) {
                            console.log(data)
                            if( data == 1 ){
                                that.children('i').removeClass('far');
                                that.children('i').removeClass('fas');
                                that.children('i').addClass('fas');
                                that.children('p').html('');
                                that.children('p').html(add_like_count);
                                that.attr('data-like_count', add_like_count);
                                that.siblings('.ws-like-spinner').hide();
                                that.show();

                            }else{
                                alert('Somethig Went Wrong!')
                            }
                        },
                        error : function(response ){
                            console.log(response)
                            }
                    });//ajax
                }else{
                    // send remove like request
                    $.ajax({
                        beforeSend : function(xhr){
                          that.hide();
                          
                        },
                        url: "<?php echo site_url('front/remove_like') ?>",
                        type: "POST",
                        data: {
                            "post_id":post_id,
                        },
                        success: function (data) {
                            console.log(data)
                            if( data == 1 ){
                                that.children('i').removeClass('far');
                                that.children('i').removeClass('fas');
                                that.children('i').addClass('far');
                                that.children('p').html('');
                                that.children('p').html(minus_like_count);
                                that.attr('data-like_count', minus_like_count);
                                that.siblings('.ws-like-spinner').hide();
                                that.show();
                            }else{
                                alert('Somethig Went Wrong!');
                                location.reload();
                            }
                        },
                        error : function(response ){
                            console.log(response)
                            }
                    });//ajax
                }
            },
            error : function(response ){
                console.log(response)
                }
        });//ajax
       
    });
 // $('.like-btn').hover(function(e){
 //        e.preventDefault();       
 //    }, function(){
 //        alert('unhover');
 // });

    $('.favourite-btn').on('click', function(e){
        e.preventDefault();
        var that = $(this);
        var post_id = that.attr('data-post_id');
        var author_id = that.attr('data-author_id');
        
        // alert( post_url );
        $.ajax({
            beforeSend : function(xhr){
              that.hide();
              that.siblings('.ws-favourite-spinner').show();
            },
            url: "<?php echo site_url('front/check_favourite') ?>",
            type: "POST",
            data: {
                "post_id"       :post_id,
                
            },
            success: function (data) {
                // console.log(data)
                if( data == 0 ){
                    // send add favourite request
                    $.ajax({
                        beforeSend : function(xhr){
                          that.hide();
                          
                        },
                        url: "<?php echo site_url('front/add_favourite') ?>",
                        type: "POST",
                        data: {
                            "post_id":post_id,
                            "author_id"     :author_id,
                        },
                        success: function (data) {
                            console.log(data)
                            if( data == 1 ){
                                that.children('i').removeClass('far');
                                that.children('i').removeClass('fas');
                                that.children('i').addClass('fas');
                                that.siblings('.ws-favourite-spinner').hide();
                                that.show();

                            }else{
                                alert('Somethig Went Wrong!')
                            }
                        },
                        error : function(response ){
                            console.log(response)
                            }
                    });//ajax
                }else{
                    // send remove favourite request
                    $.ajax({
                        beforeSend : function(xhr){
                          that.hide();
                          
                        },
                        url: "<?php echo site_url('front/remove_favourite') ?>",
                        type: "POST",
                        data: {
                            "post_id":post_id,
                        },
                        success: function (data) {
                            console.log(data)
                            if( data == 1 ){
                                that.children('i').removeClass('far');
                                that.children('i').removeClass('fas');
                                that.children('i').addClass('far');
                                that.siblings('.ws-favourite-spinner').hide();
                                that.show();
                            }else{
                                alert('Somethig Went Wrong!');
                                location.reload();
                            }
                        },
                        error : function(response ){
                            console.log(response)
                            }
                    });//ajax
                }
            },
            error : function(response ){
                console.log(response)
                }
        });//ajax
       
    });

    $('#postmenuModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var post_id = button.data('post_id');
        var post_slug = button.data('post_slug');
        var author_token = button.data('author');
        var user_token = button.data('user_token');
        var current_url = window.location.href;
        var modal = $(this);
        // modal.find('.modal-title').text('New message to ' + recipient)
        // modal.find('.modal-body input').val(recipient)
        var edit_href = "<?php echo site_url('edit-post/') ?>"+post_id;
        var report_href = "<?php echo site_url('report-post/') ?>"+post_slug+"?redirect="+current_url;
        if( author_token == user_token ){
            var html = '<li><a href="'+edit_href+'" >Edit Post</a></li>';
        }else{
            var html = '<li><a href="'+report_href+'" >Report Post</a></li>';
        }
        

        modal.find('#post-modal-ul').html( '' );
        modal.find('#post-modal-ul').append( html );
    })
</script>
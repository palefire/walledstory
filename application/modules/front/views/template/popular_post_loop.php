<style type="text/css">
    a :hover{text-decoration: none}
     @media only screen and (max-device-width: 360px){
        .stories .story-box{width: 95%}
     }
     @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        .stories .story-box{width: 95%}
     }
</style>
<?php
    foreach( $popular_posts as $post ){
?>
        <a href="<?php echo site_url('post/').$post['post_slug'] ?>">
            <div class="story-box">
                <?php if( $post['featured_img_url'] != null ){ ?>
                    <img src="<?php echo $post['featured_img_url'] ?>" alt="">
                <?php } ?>
                <span style="text-decoration: none; color: #000">
                    <?php
                        if( $post['user_info_fname'] == null ){
                            echo $post['user_name'];
                        }else{
                            if( $post['show_name'] == 1 ){
                                echo $post['user_info_fname'].' '.$post['user_info_lname'];
                            }else{
                                echo $post['user_name'];
                            }
                            
                        } 
                    ?>
                </span>
            </div>
        </a>
        
<?php 
    }
?>
<!-- <a href="#">
     <div class="story-box">
        <img src="<?php echo $post['featured_img_url'] ?>" alt="">
        <span style="text-decoration: none; color: #000">
            Sarasij Roy
        </span>
     </div>
</a>
<a href="#">
     <div class="story-box">
        <img src="<?php echo $post['featured_img_url'] ?>" alt="">
        <span style="text-decoration: none; color: #000">
            Sarasij Roy
        </span>
     </div>
</a> -->
<!--Right sidebar-->
<div class="right-side-bar">
    <div class="container-fluid">
        <div class="row">

            <div class="col-12 right-side-bar-items">
                <span class="right-side-bar-items-title">Check what's Special</span>
                <a href="<?php echo site_url('category/art') ?>" style="color: #fff">
                    <div class="section-selection">
                        <div class="section-image">
                            <img src="<?php echo site_url('assets/files/right_side_bar/arts.jpg') ?>" alt="">
                            <div class="image-overlay"><!-- <i class="fas fa-play-circle"></i> --></div>
                        </div>
                        <span>Arts</span>
                    </div>
                </a>
                <a href="<?php echo site_url('category/sports') ?>" style="color: #fff">
                    <div class="section-selection">
                        <div class="section-image">
                            <img src="<?php echo site_url('assets/files/right_side_bar/travel.jpg') ?>" alt="">
                            <div class="image-overlay"><!-- <i class="fas fa-play-circle"></i> --></div>
                        </div>
                        <span>Sports</span>
                    </div>
                </a>
                <a href="<?php echo site_url('category/art') ?>" style="color: #fff">
                    <div class="section-selection">
                        <div class="section-image">
                            <img src="<?php echo site_url('assets/files/right_side_bar/technology.jpg') ?>" alt="">
                            <div class="image-overlay"><!-- <i class="fas fa-play-circle"></i> --></div>
                        </div>
                        <span>Media</span>
                    </div>
                </a>

                <a href="<?php echo site_url('categories') ?>" class="see-more">See more</a>
            </div>

            <div class="col-12 right-side-bar-switch">
                <div class="right-side-bar-switch-title">
                    <div class="title-dot">
                        <div class="title-inner-dot"></div>
                    </div>
                    Switch to
                </div>
                <div class="right-side-bar-switch-body">
                    <div class="right-side-bar-switch-body-items">
                        <ul>
                            <li>
                                <!-- <img src="assets/files/ws-logo-white.png" alt=""> -->
                                <a href="<?php echo site_url('shorts') ?>"><span>WS Shorts</span></a>
                            </li>
                            <li>
                                <!-- <img src="assets/files/ws-logo-white.png" alt=""> -->
                                <a href="<?php echo site_url('shots') ?>"><span>WS Shots</span></a>
                            </li>
                            <!-- <li>
                                <img src="assets/files/ws-logo-white.png" alt="">
                                <span>Video</span>
                            </li> -->
                        </ul>
                    </div>  
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Right Sidebar end -->
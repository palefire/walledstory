<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<style type="text/css">

.nav-pills li{}
.nav-pills li a{background-color: #000;margin-right: 20px;padding: 10px;border-radius: 4px;color: #fff;text-decoration: none;letter-spacing: 1px}
.nav-pills li a.active{background-color: #7C88D2}

.switch { position: relative; display: inline-block; width: 60px; height: 34px; } 
.switch input { opacity: 0; width: 0; height: 0; } 
.slider { position: absolute; cursor: pointer; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; -webkit-transition: .4s; transition: .4s; } 
.slider:before { position: absolute; content: ""; height: 26px; width: 26px; left: 4px; bottom: 4px; background-color: white; -webkit-transition: .4s; transition: .4s; } 
input:checked + .slider { background-color: #7C88D2; } 
input:focus + .slider { box-shadow: 0 0 1px #7C88D2; } 
input:checked + .slider:before { -webkit-transform: translateX(26px); -ms-transform: translateX(26px); transform: translateX(26px); } /* Rounded sliders */ 
.slider.round { border-radius: 34px; } 
.slider.round:before { border-radius: 50%; }
.mobile-edit-featured-img{display: none}
.laptop-display-none{display: none;}
@media only screen and (max-device-width: 360px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important }
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
        .posts{display: none}
        .edit-featured-img{display: none}
        .mobile-edit-featured-img{display: block}
        .alert-right{right: 210px !important}
        .mobile-display_none{display: none}
        .laptop-display-none{display: block;}
    }
     @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important ; top:46%;}
        .croppie-container .cr-boundary{width: 230px !important; height: 150px !important}
        .croppie-container .cr-viewport{width: 210px !important; height: 135px !important}
        .croppie-container .cr-image{width: 585 !important; height: 388 !important}
        .croppie-container .cr-overlay,{width: 210px !important; height: 135px !important}
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
        .posts{display: none}
        .edit-featured-img{display: none}
        .mobile-edit-featured-img{display: block}
        .alert-right{right: 210px !important}
        .mobile-display_none{display: none}
        .laptop-display-none{display: block;}
    }
</style>



<div class="row box">
	<!-- Left box -->
	<div class="box-1">
        <!-- Left sidebar -->
        <div class="left-side-bar">
            <div class="container-fluid">
                <div class="row">

                    <!-- Profile Picture -->
                    <div class="col-12 side-bar-dp text-center">
                    	<?php
		                    if( $header_data_array['show_image'] == 1 ){
		                        if( $header_data_array['profile_img'] != null ){
		                ?>
		                            <img id="profile-image-left" src="<?php echo $header_data_array['profile_img'] ?>" alt="">
		                <?php            
		                        }else{
		                ?>
		                            <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
		                <?php
		                        }
		                ?>
		                    
		                <?php
		                    }else{
		                ?>
		                        <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
		                <?php
		                    }
		                ?>
                        <span><?php echo $header_data_array['fname'].' '.$header_data_array['lname'] ?> </span>
                    </div>
                    <!-- End Profile Picture -->

                    <div class="col-12 side-bar-menu-list">
                        <a href="<?php echo site_url() ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item active"><span><i class="fas fa-home"></i></span>Home
                            </div>
                        </a>
                        <a href="<?php echo site_url('profile') ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-user"></i></span>Profile
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-bell"></i></span>Notifications
                            </div>
                        </a>
                        <!-- <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-envelope"></i></span>Messages
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-star"></i></span>Favourites
                            </div>
                        </a> -->
                    </div>

                </div>
            </div>
        </div>
        <!-- Left sidebar end -->
    </div>

    <!-- Middle box -->
    <div class="box-2">
    	<div style="position: absolute; z-index: 999999; right: 320px; top: 6vh; " class="alert-right">
	        <?php 
	            $profile_updated = $this->session->flashdata('profile_updated');
	            if( $profile_updated ){
	         ?>
	            <div class="alert alert-dismissible alert-success">
	                <button type="button" class="close" data-dismiss="alert">&times;</button>
	                <h4 class="alert-heading">Great!</h4>
	                <p class="mb-0"><?php echo $profile_updated; ?></p>
	            </div>
	        <?php } 
	        ?>
	        <?php 
	            $profile_updated_fail = $this->session->flashdata('profile_updated_fail');
	            if( $profile_updated_fail ){
	         ?>
	            <div class="alert alert-dismissible alert-danger">
	                <button type="button" class="close" data-dismiss="alert">&times;</button>
	                <h4 class="alert-heading">Great!</h4>
	                <p class="mb-0"><?php echo $profile_updated_fail; ?></p>
	            </div>
	        <?php } 
	        ?>
	    </div>
    	<div class="container">
    		<div class="row">
    			<div class="col-12">
    				<h3>Edit Post</h3>
    			</div>
    			<div class="col-12 mt-2">
    				<ul class="nav nav-pills mb-3 new-pils" id="pills-tab" role="tablist" style="position: relative; display: inline-flex; background-color: transparent; box-shadow: unset">
					  <li class="nav-item" role="presentation">
					    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Post Detalis</a>
					  </li>
					  <li class="nav-item mobile-display_none" role="presentation">
					    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Post Image</a>
					  </li>
					  <li class="nav-item laptop-display-none" role="presentation" style="color:#fff">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="modal" data-target="#downloadtheappModal">Post Image</a>
                      </li>
					</ul>
					<div class="tab-content" id="pills-tabContent">
					  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
					  	<form method="POST" action="<?php echo site_url('front/edit_post_to_db') ?>">
							<div class="row">
								<div class="col-md-5 form-group">
									<label>Title</label>
									<input type="text" name="post_title" class="form-control" value="<?php echo $post_details['post_title'] ?>">
								</div>
								
								
							</div>
							<div class="row">
								<div class="col-md-10">
									  <div class="form-group">
									    <label for="exampleFormControlSelect1">Category</label>
									    <select class="form-control" id="exampleFormControlSelect1"name="cat_id">
									    	<?php
									    		foreach( $all_cat as $cat ){
									    	?>
									    	<option value="<?php echo $cat['cat_id'] ?>" <?php if( $post_details['cat_id'] == $cat['cat_id'] ){ echo 'selected';} ?> ><?php echo $cat['cat_name'] ?></option>
									      	
									      	<?php
									      		}
									      	?>
									    </select>
									  </div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-10">
								  <div class="form-group">
								    <label for="exampleFormControlTextarea1">Content</label>
								    <textarea class="form-control tinymce" id="exampleFormControlTextarea1" rows="3" name="post_content"><?php echo $post_details['post_content'] ?></textarea>
								  </div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-12">
									<input type="hidden" name="post_id" value="<?php echo $post_details['post_id'] ?>">
                                    <button type="button" id="delete-post" class="btn btn-danger float-left ml-3" data-post_id="<?php echo $post_details['post_id'] ?>">Delete</button>
									<input type="submit" name="" class="btn btn-primary float-right" value="Save">
								</div>
							</div>
						</form>
					  </div>
					  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
					     	<div class="row">
					     		<div class="col-md-12 form-group">
									<div class="edit-featured-img" data-toggle="modal" data-target="#post-image-modal" style="cursor: pointer;">
										<?php
											if( $post_details['featured_img_url'] != null ){
										?>
												<img src="<?php echo $post_details['featured_img_url'] ?>" style="width: 100%">
										<?php
											}else{
										?>
												<img src="<?php echo $this->data['default_placeholders'] ?>">
										<?php
											}
										?>
									</div>
                                    <div class="mobile-edit-featured-img" >
                                        <?php
                                            if( $post_details['featured_img_url'] != null ){
                                        ?>
                                                <img src="<?php echo $post_details['featured_img_url'] ?>" style="width: 100%">
                                        <?php
                                            }else{
                                        ?>
                                                <img src="<?php echo $this->data['default_placeholders'] ?>">
                                        <?php
                                            }
                                        ?>
                                    </div>
								</div>
								<div class="form-group col-md-12">
								    <label for="exampleFormControlTextarea1">Change Picture</label>
								    <button class="edit-featured-img" id="exampleFormControlTextarea1"
                                 rows="2" data-toggle="modal" data-target="#post-image-modal">Upload</button>
                                 <input type="file" class="mobile-edit-featured-img" id="change-mobile-featured-image" rows="2" >
								</div>
					     	</div>
					  </div>
					 
					</div>

    			</div>
    		</div>

    	</div>
    </div>
    <!-- Right box -->
    <div class="box-3">
        <!-- Right sidebar-->
        <?php echo $right_bar_view; ?>
        <!-- Right Sidebar end -->
    </div>
</div>

<div class="modal fade" id="post-image-modal" tabindex="-1" role="dialog" aria-labelledby="postFormModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable ws-modal" role="document" style="top:55%">
    	<form class="ws-modal-form" autocomplete="off" id="post-form-image">
	        <div class="modal-content">
	            <div class="modal-body">
	                <div class="container">
	                    <div class="row">
	                        <div class="col-12 ws-modal-header">
	                            <div class="ws-modal-title">Change Image for Post</div>
	                            
	                        </div>
	                        <div class="ws-modal-body">
	                            <div class="container">
	                                <div class="row">
	                                    <div class="col-12">
	                                           
	                                        <div class="form-group content-parent">
	                                          <label for="title">Image</label>
	                                          <input type="file" class="form-control" id="image" >
	                                          <div id="upload-demo" style="margin-top: 20px"></div>
	                                        </div>
	                                            
	                                        
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            	<input type="hidden" id="post-id" value="<?php echo $post_details['post_id'] ?>">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                <button type="button" class="btn btn-primary" id="save-change-image-post">Save changes</button>
	            </div>
	        </div>
	    </form>
    </div>
</div>
<script type="text/javascript">
	var resize = $('#upload-demo').croppie({
        enableExif: true,
        enableOrientation: true,    
        viewport: { // Default { width: 100, height: 100, type: 'square' } 
            width: 585,
            height: 388,
            type: 'square' //square
        },
        boundary: {
            width: 650,
            height: 450
        }
    });
    $('#image').on('change', function () { 
      var reader = new FileReader();
        reader.onload = function (e) {
          resize.croppie('bind',{
            url: e.target.result
          }).then(function(){
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
    });
    $('#save-change-image-post').on('click', function (ev) {
    	ev.preventDefault();
    	var that = $(this);
    	var post_id = that.siblings('#post-id').val();
    	// alert(post_id);

    	resize.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            if( document.getElementById("image").files.length == 0 ){
                img = '';
            }
            $.ajax({
              url: "<?php echo site_url('front/update_post_image') ?>",
              type: "POST",
              data: {
                "img"		:img,
                "post_id"	:post_id,
                
                },
              success: function (data) {
                
                var data_array = jQuery.parseJSON(data);
                if( data_array.code == 0 ){
                    alert(data_array.message);
                }
                 if( data_array.code == 5 ){
                    console.log(data_array.message);
                }
                if( data_array.code == 1 ){
                    // location.reload();
                }else{
                     console.log(data_array.message)
                }
                // console.log(data)
              },
              error : function(response ){
                console.log(response)
              }
            });//ajax
        });

    });

    $('#delete-post').on( 'click', function(e){
        e.preventDefault();
        var that = $(this);
        var post_id = that.data('post_id');
        // alert(post_id);
        if(confirm("Are you sure? Do you want to delet the post?")){
           $.ajax({
              url: "<?php echo site_url('front/delete_post_db') ?>",
              type: "POST",
              data: {
                "post_id"   :post_id,
                
                },
              success: function (data) {
                console.log(data);
                var data_array = jQuery.parseJSON(data);
                // if( data_array.code == 0 ){
                //     alert(data_array.message);
                // }
                //  if( data_array.code == 5 ){
                //     console.log(data_array.message);
                // }
                if( data_array.code == 1 ){
                    window.location.replace("<?php echo site_url(); ?>")
                }else{
                     console.log(data_array.message)
                }
                // console.log(data)
              },
              error : function(response ){
                console.log(response)
              }
            });//ajax 
        }//confirm

    } );

    $('#change-mobile-featured-image').on( 'change', function(){
        var that = $(this);
        var data = new FormData();
        var post_id = <?php echo $post_details['post_id'] ?>;
        data.append('pstimg', that.prop('files')[0]);
        data.append('post_id', post_id);

        $.ajax({
            url: "<?php echo site_url('front/update_post_image_mobile') ?>",
            type: "POST",
            data : data,
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                
                var data_array = jQuery.parseJSON(data);
                if( data_array.code == 0 ){
                    alert(data_array.message);
                }
                 if( data_array.code == 5 ){
                    console.log(data_array.message);
                }
                if( data_array.code == 1 ){
                    location.reload();
                }else{
                     console.log(data_array.message)
                }
                // console.log(data)
            },
            error : function(response ){
                console.log(response)
            }
        });//ajax
    } );
</script>
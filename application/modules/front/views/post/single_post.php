<link rel="stylesheet" href="<?php echo base_url('assets/css/style-dark-mode.css?v=').microtime()?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css?v=').microtime()?>">
<style>
    .active-post-content{width: 100%;} 
    .active-post-content-footer{position: relative;}
    .active-post-content-footer a:hover{text-decoration: none;}
    .active-post-content-footer a p{font-size: 10px; text-align: center; margin-top: -11px; margin-bottom: -4px; text-decoration: none;}

    @media only screen and (max-device-width: 360px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important }
       /* .croppie-container .cr-boundary{width: 278px !important}
        .croppie-container .cr-image, .croppie-container .cr-overlay, .croppie-container .cr-viewport{width: 278px !important}*/
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
    }
     @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important }
       /* .croppie-container .cr-boundary{width: 278px !important}
        .croppie-container .cr-image, .croppie-container .cr-overlay, .croppie-container .cr-viewport{width: 278px !important}*/
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
    }
</style>
<?php 
    $CI =& get_instance();
    $CI->load->model('front/frontmodel','frontmodel');
    $check_like = $CI->frontmodel->check_like( $post_details['post_id'], $this->user_id );
    $like_count = $CI->frontmodel->like_count( $post_details['post_id'] );
    $check_favourite = $CI->frontmodel->check_favourite( $post_details['post_id'], $this->user_id );
?>
<div class="row box">
	<div class="box-1">
		<!-- Left sidebar -->
        <div class="pro-left-side-bar">
        	<div class="side-bar-dp text-center" style="position: relative; padding-top: 10px">
        		<!-- <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt=""> -->
        		<span style="">
                    About 
                    <?php
                        if( $post_details['user_info_fname'] == null ){
                            echo $post_details['user_name'];
                        }else{
                            if( $post_details['show_name'] == 1 ){
                                echo $post_details['user_info_fname'];
                            }else{
                                echo $post_details['user_name'];
                            }
                            
                        }
                    ?>
                </span>
                <div class="row" style="margin: 10px">
                    <?php
                        if( $profile_images != false ){
                            foreach( $profile_images as $profile_image ){
                    ?>
                                <div class="col-6">
                                    <img style="border-radius: 2px" src="<?php echo $profile_image['file_url'] ?>" alt="">
                                </div>
                    <?php 
                            }
                        }
                    ?>
                    
                </div>
                <div class="row">
                    <div class="col-12">
                        <a href="#">
                            <button class="btn btn-sm btn-secondary">See All&nbsp;&nbsp;<i class="fas fa-reply-all"></i></button>
                        </a>
                    </div>
                    
                </div>
                <p class="text-left" style="padding-top: 20px">
                    <?php 
                        if( $post_details['show_bio'] == 1 ){
                            echo $post_details['user_bio'];
                        }
                        
                    ?>
                </p>
        	</div>
        	<!-- <div class="side-bar-dp-footer text-left">
                <p>
                    Followers&nbsp;&nbsp;&nbsp;&nbsp;20k
                    <br>
                    Following&nbsp;&nbsp;&nbsp;&nbsp;200
                </p>
            </div> -->
        </div>
        <div class="pro-left-side-bar-arrow">
            <span>&#10095;</span>
        </div>
	</div>
	<!-- Left Box End -->
	<!-- Middle box -->
    <div class="box-2">
    	<div class="pro-cover" style="background-image: url('<?php 
            if( $post_details["user_cover_image"] != null ){
                    echo $post_details["user_cover_image"];
                }else{
                   echo $this->data["profile_cover_placeholders"] ; 
                }
            
        ?>');">
    		<div class="overlay"></div>
            <div style="position: absolute;bottom: -30px; left: 50%; z-index: 99; height: 130px; width: 110px; overflow: hidden; transform: translateX(-50%);  border-radius: 8px">
                <?php
                    if( $post_details['show_image'] == 1 ){
                        if( $post_details['user_image'] != null ){
                ?>
                            <img class="posts-owner-dp" src="<?php echo $post_details['user_image'] ?>" alt="" style="width: 100%">
                <?php            
                        }else{
                ?>
                            <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="" style="width: 100%">
                <?php
                        }
                ?>
                    
                <?php
                    }else{
                ?>
                        <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="" style="width: 100%">
                <?php
                    }
                ?>
                
            </div>

    		<div class="pro-cover-content">
                <div class="container">
                    <div class="row">
                        <div class="col-10 profile-info text-left">
                           
                        </div>
                        <div class="col-2 profile-connect-buttons">
                            
                            <!-- <a href="#">
                                <button class="btn btn-md">Edit&nbsp;&nbsp;<i class="fas fa-user-edit"></i></button>
                            </a> -->
                        </div>
                    </div>
                </div>
            </div>
    	</div>
    	 <div class="container">
            <div class="row">
                <div class="col-12">
                    <p style="color: #fff; text-align: center; margin-top: 35px">
                        <span>
                             <?php
                                    if( $post_details['user_info_fname'] == null ){
                                        echo $post_details['user_name'];
                                    }else{
                                        if( $post_details['show_name'] == 1 ){
                                            echo $post_details['user_info_fname'].' '.$post_details['user_info_lname'];
                                        }else{
                                            echo $post_details['user_name'];
                                        }
                                        
                                    }
                                ?> .
                        </span>
                    </p>
                </div>
            </div>
           <!--  <div class="row posts">
                <div class="col-12">
                	<label for="post-box"><?php// echo $post_details['post_title'] ?></label>
            	</div>
            </div> -->

            <!-- Current Posts -->
            <div class="row current-posts">

                
			    <div class="col-12 active-posts">
                
                
                <!-- <?php
                    if( $post_details['show_image'] == 1 ){
                        if( $post_details['user_image'] != null ){
                ?>
                            <img class="posts-owner-dp" src="<?php echo $post_details['user_image'] ?>" alt="">
                <?php            
                        }else{
                ?>
                            <img class="posts-owner-dp" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                <?php
                        }
                ?>
                    
                <?php
                    }else{
                ?>
                        <img class="posts-owner-dp" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
                <?php
                    }
                ?> -->
                    
                    <div class="active-post-content">
                        <div class="active-post-content-header">
                            <div class="info">
                                <a href="#" style="text-decoration: none;">
                                    <h5>
                                        <?php
                                            echo $post_details['post_title']
                                        ?>
                                    </h5>
                                </a>
                                <span>Posted <?php echo date('d-m-Y', strtotime($post_details['post_time'])) ?></span>
                            </div>
                            <div class="three-dots" data-toggle="modal" data-target="#postmenuModal" data-post_id="<?php echo $post_details['post_id'] ?>" data-user_token="<?php echo $this->session->userdata['wst23xyzsdfretw89lk_user_token'] ?>" data-author = "<?php echo $post_details['user_token'] ?>" style="cursor: pointer;">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                        <div class="active-post-content-body">
                            <?php if( $post_details['featured_img_url'] != null ){ ?>
                                    <img class="content-body-image" src="<?php echo $post_details['featured_img_url'] ?>" alt="">
                            <?php  } ?>
                                    
                            
                            <p>
                                <?php echo $post_details['post_content'] ?>
                            </p>
                        </div>
                        <div class="active-post-content-footer">
                            <a href="#" data-post_id="<?php echo $post_details['post_id'] ?>" data-user_token="<?php echo $this->session->userdata['wst23xyzsdfretw89lk_user_token'] ?>" data-like_count="<?php echo $like_count ?>" data-author_id="<?php echo $post_details['user_id'] ?>" data-author_token="<?php echo $post_details['user_token'] ?>" data-post_url = "<?php echo site_url('post/').$post_details['post_slug'] ?>" class="like-btn">
                                <p><?php echo $like_count ?></p>
                                <i class="<?php 
                                    if( $check_like ){
                                        echo 'fas';
                                    }else{
                                        echo 'far';
                                    }
                                 ?> fa-heart"></i></a>
                            <!-- <a href="#"><i class="fas fa-comment"></i></a> -->
                            <?php
                            if( $post_details['user_id'] != $this->user_id ){
                            ?>
                            <div class="fa-1x ws-favourite-spinner">
                                <i class="fas fa-spinner fa-spin" style="color: #5164CB"></i>
                            </div>
                            <a href="#" data-post_id="<?php echo $post_details['post_id'] ?>" data-author_id="<?php echo $post_details['user_id'] ?>" class="favourite-btn"><i class="
                                <?php 
                                    if( $check_favourite ){
                                        echo 'fas';
                                    }else{
                                        echo 'far';
                                    }
                                ?>  
                                fa-star"></i>
                            </a>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Current Posts End -->
        </div>
    </div>

    <!-- Right box -->
    <div class="box-3">

        <!-- Right sidebar-->
       <?php echo $right_bar_view; ?>
        <!-- Right Sidebar end -->

    </div>
    <!-- Right Box End -->
</div>


<div class="modal fade" id="postmenuModal" tabindex="-1" aria-labelledby="postmenuModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="postmenuModalLabel">Post Menu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul id="post-modal-ul">
            
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>






<script type="text/javascript">
    //Like button
    $('.like-btn').on('click', function(e){
        e.preventDefault();
        var that = $(this);
        var post_id = that.attr('data-post_id');
        var user_token = that.attr('data-user_token');
        var like_count = that.attr('data-like_count');
        var author_id = that.attr('data-author_id');
        var author_token = that.attr('data-author_token');
        var post_url = that.attr('data-post_url');
        like_count = parseInt( like_count );
        var add_like_count = like_count + 1;
        var minus_like_count = like_count - 1;
        // alert( like_count );
        $.ajax({
            beforeSend : function(xhr){
              that.hide();
            },
            url: "<?php echo site_url('front/check_like') ?>",
            type: "POST",
            data: {
                "post_id":post_id,
            },
            success: function (data) {
                // console.log(data)
                if( data == 0 ){
                    // send like request
                    $.ajax({
                        beforeSend : function(xhr){
                          that.hide();
                          
                        },
                        url: "<?php echo site_url('front/add_like') ?>",
                        type: "POST",
                        data: {
                            "post_id":post_id,
                            "author_id"     :author_id,
                            "author_token"  :author_token,
                            "post_url"      :post_url,
                        },
                        success: function (data) {
                            console.log(data)
                            if( data == 1 ){
                                that.children('i').removeClass('far');
                                that.children('i').removeClass('fas');
                                that.children('i').addClass('fas');
                                that.children('p').html('');
                                that.children('p').html(add_like_count);
                                that.attr('data-like_count', add_like_count);
                                that.show();

                            }else{
                                alert('Somethig Went Wrong!')
                            }
                        },
                        error : function(response ){
                            console.log(response)
                            }
                    });//ajax
                }else{
                    // send remove like request
                    $.ajax({
                        beforeSend : function(xhr){
                          that.hide();
                          
                        },
                        url: "<?php echo site_url('front/remove_like') ?>",
                        type: "POST",
                        data: {
                            "post_id":post_id,
                        },
                        success: function (data) {
                            console.log(data)
                            if( data == 1 ){
                                that.children('i').removeClass('far');
                                that.children('i').removeClass('fas');
                                that.children('i').addClass('far');
                                that.children('p').html('');
                                that.children('p').html(minus_like_count);
                                that.attr('data-like_count', minus_like_count);
                                that.show();
                            }else{
                                alert('Somethig Went Wrong!');
                                location.reload();
                            }
                        },
                        error : function(response ){
                            console.log(response)
                            }
                    });//ajax
                }
            },
            error : function(response ){
                console.log(response)
                }
        });//ajax
       
    });
    $('.favourite-btn').on('click', function(e){
        e.preventDefault();
        var that = $(this);
        var post_id = that.attr('data-post_id');
        var author_id = that.attr('data-author_id');
        
        // alert( post_url );
        $.ajax({
            beforeSend : function(xhr){
              that.hide();
              that.siblings('.ws-favourite-spinner').show();
            },
            url: "<?php echo site_url('front/check_favourite') ?>",
            type: "POST",
            data: {
                "post_id"       :post_id,
                
            },
            success: function (data) {
                // console.log(data)
                if( data == 0 ){
                    // send add favourite request
                    $.ajax({
                        beforeSend : function(xhr){
                          that.hide();
                          
                        },
                        url: "<?php echo site_url('front/add_favourite') ?>",
                        type: "POST",
                        data: {
                            "post_id":post_id,
                            "author_id"     :author_id,
                        },
                        success: function (data) {
                            console.log(data)
                            if( data == 1 ){
                                that.children('i').removeClass('far');
                                that.children('i').removeClass('fas');
                                that.children('i').addClass('fas');
                                that.siblings('.ws-favourite-spinner').hide();
                                that.show();

                            }else{
                                alert('Somethig Went Wrong!')
                            }
                        },
                        error : function(response ){
                            console.log(response)
                            }
                    });//ajax
                }else{
                    // send remove favourite request
                    $.ajax({
                        beforeSend : function(xhr){
                          that.hide();
                          
                        },
                        url: "<?php echo site_url('front/remove_favourite') ?>",
                        type: "POST",
                        data: {
                            "post_id":post_id,
                        },
                        success: function (data) {
                            console.log(data)
                            if( data == 1 ){
                                that.children('i').removeClass('far');
                                that.children('i').removeClass('fas');
                                that.children('i').addClass('far');
                                that.siblings('.ws-favourite-spinner').hide();
                                that.show();
                            }else{
                                alert('Somethig Went Wrong!');
                                location.reload();
                            }
                        },
                        error : function(response ){
                            console.log(response)
                            }
                    });//ajax
                }
            },
            error : function(response ){
                console.log(response)
                }
        });//ajax
       
    });
    $('#postmenuModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var post_id = button.data('post_id');
        var author_token = button.data('author');
        var user_token = button.data('user_token');
        // alert(author_token);
        var modal = $(this);
        // modal.find('.modal-title').text('New message to ' + recipient)
        // modal.find('.modal-body input').val(recipient)
        var edit_href = "<?php echo site_url('edit-post/') ?>"+post_id;
        var report_href = "<?php echo site_url('report-post/') ?>"+post_id;
        if( author_token == user_token ){
            var html = '<li><a href="'+edit_href+'" >Edit Post</a></li>';
        }else{
            var html = '<li><a href="'+report_href+'" >Report Post</a></li>';
        }
        

        modal.find('#post-modal-ul').html( '' );
        modal.find('#post-modal-ul').append( html );
        
    })
</script>
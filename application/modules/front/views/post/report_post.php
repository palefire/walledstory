<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<style type="text/css">

.nav-pills li{}
.nav-pills li a{background-color: #000;margin-right: 20px;padding: 10px;border-radius: 4px;color: #fff;text-decoration: none;letter-spacing: 1px}
.nav-pills li a.active{background-color: #7C88D2}

.switch { position: relative; display: inline-block; width: 60px; height: 34px; } 
.switch input { opacity: 0; width: 0; height: 0; } 
.slider { position: absolute; cursor: pointer; top: 0; left: 0; right: 0; bottom: 0; background-color: #ccc; -webkit-transition: .4s; transition: .4s; } 
.slider:before { position: absolute; content: ""; height: 26px; width: 26px; left: 4px; bottom: 4px; background-color: white; -webkit-transition: .4s; transition: .4s; } 
input:checked + .slider { background-color: #7C88D2; } 
input:focus + .slider { box-shadow: 0 0 1px #7C88D2; } 
input:checked + .slider:before { -webkit-transform: translateX(26px); -ms-transform: translateX(26px); transform: translateX(26px); } /* Rounded sliders */ 
.slider.round { border-radius: 34px; } 
.slider.round:before { border-radius: 50%; }
.mobile-edit-featured-img{display: none}
@media only screen and (max-device-width: 360px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important }
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
        .posts{display: none}
        .edit-featured-img{display: none}
        .mobile-edit-featured-img{display: block}
        .alert-right{right: 210px !important}

    }
     @media only screen and (min-device-width: 361px) and (max-device-width: 570px){
        body{padding: 0px; margin: 0 0 55px 0;}
        .box-1{display: none}
        .box-3{display: none}
        .side-main-box{display: none}
        .main-box{width: 100%;  border-radius: 0px 0px 0px 0px}

        .box .box-2{width: 100%}
        .main-header{padding: 1vh 1vh 1vh 1vh}
        .main-header .main-header-left{width: 80%;padding-top: 6px; padding-left: 15px}
        .main-header .main-header-left .main-header-image a img{height: 2vh}
        .main-header .main-header-right{width:20%;justify-content: space-evenly;}
        .main-header .main-header-right .main-header-logout{display: none}
        .main-header .main-header-right .main-header-dp a img{height: 40px; width: 40px}

        .stories .story-box{max-width: 100%; min-width: 20%}
        .stories .story-box span{display: none}

        .ws-modal{max-width:300px !important ; top:46%;}
        .croppie-container .cr-boundary{width: 230px !important; height: 150px !important}
        .croppie-container .cr-viewport{width: 210px !important; height: 135px !important}
        .croppie-container .cr-image{width: 585 !important; height: 388 !important}
        .croppie-container .cr-overlay,{width: 210px !important; height: 135px !important}
        .current-posts .active-posts .active-post-content .active-post-content-body{width: 110%; margin-left: -24px}
        .posts{display: none}
        .edit-featured-img{display: none}
        .mobile-edit-featured-img{display: block}
        .alert-right{right: 210px !important}
    }
</style>



<div class="row box">
	<!-- Left box -->
	<div class="box-1">
        <!-- Left sidebar -->
        <div class="left-side-bar">
            <div class="container-fluid">
                <div class="row">

                    <!-- Profile Picture -->
                    <div class="col-12 side-bar-dp text-center">
                    	<?php
		                    if( $header_data_array['show_image'] == 1 ){
		                        if( $header_data_array['profile_img'] != null ){
		                ?>
		                            <img id="profile-image-left" src="<?php echo $header_data_array['profile_img'] ?>" alt="">
		                <?php            
		                        }else{
		                ?>
		                            <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
		                <?php
		                        }
		                ?>
		                    
		                <?php
		                    }else{
		                ?>
		                        <img id="profile-image-left" src="<?php echo $this->data['profile_pic_placeholders'] ?>" alt="">
		                <?php
		                    }
		                ?>
                        <span><?php echo $header_data_array['fname'].' '.$header_data_array['lname'] ?> </span>
                    </div>
                    <!-- End Profile Picture -->

                    <div class="col-12 side-bar-menu-list">
                        <a href="<?php echo site_url() ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item active"><span><i class="fas fa-home"></i></span>Home
                            </div>
                        </a>
                        <a href="<?php echo site_url('profile') ?>" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-user"></i></span>Profile
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-bell"></i></span>Notifications
                            </div>
                        </a>
                        <!-- <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-envelope"></i></span>Messages
                            </div>
                        </a>
                        <a href="#" style="text-decoration: none">
                            <div class="side-bar-menu-list-item"><span><i class="fas fa-star"></i></span>Favourites
                            </div>
                        </a> -->
                    </div>

                </div>
            </div>
        </div>
        <!-- Left sidebar end -->
    </div>

    <!-- Middle box -->
    <div class="box-2">
    	<div style="position: absolute; z-index: 999999; right: 320px; top: 6vh; " class="alert-right">
	        <?php 
	            $profile_updated = $this->session->flashdata('profile_updated');
	            if( $profile_updated ){
	         ?>
	            <div class="alert alert-dismissible alert-success">
	                <button type="button" class="close" data-dismiss="alert">&times;</button>
	                <h4 class="alert-heading">Great!</h4>
	                <p class="mb-0"><?php echo $profile_updated; ?></p>
	            </div>
	        <?php } 
	        ?>
	        <?php 
	            $profile_updated_fail = $this->session->flashdata('profile_updated_fail');
	            if( $profile_updated_fail ){
	         ?>
	            <div class="alert alert-dismissible alert-danger">
	                <button type="button" class="close" data-dismiss="alert">&times;</button>
	                <h4 class="alert-heading">Great!</h4>
	                <p class="mb-0"><?php echo $profile_updated_fail; ?></p>
	            </div>
	        <?php } 
	        ?>
	    </div>
    	<div class="container">
    		<div class="row">
    			<div class="col-12">
    				<h3>Report Post</h3>
    			</div>
    			<div class="col-12 mt-2">
    				
					  	<form method="POST" action="<?php echo site_url('front/report_post_to_db') ?>">
							<div class="row">
								<div class="col-md-5 form-group">
									<label>State the reason for reporting the post.</label>
								</div>
								
								
							</div>
							<div class="row">
								<div class="col-md-10">
									  <div class="form-group">
									    <label for="exampleFormControlSelect1">Reason</label>
									    <select class="form-control" id="exampleFormControlSelect1"name="report_reason">
                                            <option>Promotes Violence</option>
                                            <option>Racism or Racist Remarks</option>
                                            <option>Promotes Child Abuse</option>
                                            <option>Nudity</option>
                                            <option>Hate mongering</option>
                                            <option>Animal abuse</option>
                                            <option>Gender abuse</option>
                                            <option>False information</option>
                                            <option>Sale of goods</option>
                                            <option>Harrassment</option>
                                            <option>Copyright issues</option>
                                            <option>Provocation of negativity through race, ethnic and class disputes.</option>
									    	<option>Others</option>
									    </select>
									  </div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-10">
								  <div class="form-group">
								    <label for="exampleFormControlTextarea1">Explain in detail.</label>
								    <textarea class="form-control " id="exampleFormControlTextarea1" rows="3" name="report_reason_explain" required></textarea>
								  </div>
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-12">
                                    <input type="hidden" name="post_id" value="<?php echo $post_details['post_id'] ?>">
									<input type="hidden" name="author_id" value="<?php echo $post_details['user_id'] ?>">
                                    <input type="hidden" name="redirect_url" class="form-control" value="<?php echo $redirect_url ?>">
									<input type="submit" name="" class="btn btn-primary float-right" value="Report">
								</div>
							</div>
                            
                            
						</form>
					  

    			</div>
    		</div>

    	</div>
    </div>
    <!-- Right box -->
    <div class="box-3">
        <!-- Right sidebar-->
        <?php echo $right_bar_view; ?>
        <!-- Right Sidebar end -->
    </div>
</div>

<script type="text/javascript">
	

    $('#delete-post').on( 'click', function(){
        
    } );

   
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller{

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        if( !isset($this->session->userdata['wst23xyzsdfretw89lk_beta_token']) ){
			return redirect('beta');
			die();
		}
       
        $this->load->model('admin/adminmodel','adminmodel');
        $this->load->model('front/frontmodel','frontmodel');
        
	}

	/*
	|--------------------------------------------------------------------------
	| Login function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		if( isset($this->session->userdata['wst23xyzsdfretw89lk_user_token']) ){
			return redirect(site_url());
		}
		
		// $remember_token = get_cookie('wstlki435_set_me');
		// $check_remember_me = $this->adminmodel->check_remember_me( $remember_token);

		// if( $check_remember_me ){
		// 	//log in the user
		// 	print_r($check_remember_me);
		// 	$this->session->set_userdata( 'wst23xyzsdfretw89lk_user_id', $check_remember_me['admin_id'] );
		// 	$this->session->set_userdata( 'wst23xyzsdfretw89lk_user_email', $check_remember_me['admin_email'] );
		// 	return redirect('admin');
		// }else{
		// 	$this->load->view('login');
		// }

		$this->load->view('login');
		
	}//fn

	public function verify_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		// $kpln = $this->input->post('kpln');

		$login_success = $this->frontmodel->checklogin( $username, $password);
		$user_token = $login_success['user_token'];
		$user_status = $login_success['user_status'];
		if( $user_status == '0' ){
			$this->session->set_userdata( 'wst23xyzsdfretw89lk_user_token', $user_token );
			return redirect(site_url('verify'));
			die();
		}
		if( $login_success ){
			$this->session->set_userdata( 'wst23xyzsdfretw89lk_user_token', $user_token );
			
			
			//set Remember Me Functionality	
			// if( $kpln ){
			// 	$admin_id = $login_success['admin_id'];
			// 	$admin_email = $login_success['admin_email'];
			// 	$admin_username = $login_success['admin_username'];
			// 	$token = md5($admin_email.$admin_username);
			// 	$remember_me_token = $token; 
			// 	$data = array(
			// 		'remember_me_token' => $remember_me_token,
			// 	);
			// 	$token_update = $this->db->update('bewbcs_admin', $data, array('admin_id' => $admin_id));
			// 	if( $token_update ){
			// 		//create cookies
					
			// 	    set_cookie('wstlki435_set_me', $remember_me_token, time()+6500);
			// 	}
			// }

			return redirect(site_url());
		}else{
			$this->session->set_flashdata( 'login_failed' , 'User Name or Password does not match!' );
			return redirect('login');
		}
	}//fn

	public function logout(){
		// $email = $this->session->userdata['wst23xyzsdfretw89lk_admin_email'];
		$this->session->unset_userdata('wst23xyzsdfretw89lk_user_token');
		// $data = array(
		// 			'remember_me_token' => NULL,
		// 		);
		// $token_update = $this->db->update('wst_admin', $data, array('admin_email' => $email));
	 //    delete_cookie('wstlki435_set_me');

		
		
		return redirect('login' );
	}//fn

	public function signup(){
		// echo '<pre>';
		// print_r($_SERVER);
		$this->load->view('register');
	}//fn

	public function register_user_to_db(){
		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');
		$username 	= $this->input->post('username');
		$phone 		= $this->input->post('phone');
		// echo '<pre>';
		// print_r($_SERVER);
		$register_user_token = $this->frontmodel->register_user_to_db( $email, $password, $username, $_SERVER, $phone );
		if( $register_user_token ){
			$this->sendotp($phone, $register_user_token[1]);
			$this->session->set_userdata( 'wst23xyzsdfretw89lk_user_token', $register_user_token[0] );
			return redirect(site_url('verify'));
		}else{
			$this->session->set_flashdata( 'DB_ERROR' , 'OOPS! There was a database error. Please wait and try after sometime.' );
			return redirect(site_url('signup'));
		}
	}//fn

	public function check_username(){
		$username 		= $this->input->post('username');
		$check_result 	= $this->frontmodel->check_username_exist( $username );
		if( $check_result ){
			echo 0;
		}else{
			echo 1;
		}
	}//fn

	public function check_email(){
		$email 		= $this->input->post('email');
		$check_result 	= $this->frontmodel->check_email_exist( $email );
		if( $check_result ){
			echo 0;
		}else{
			echo 1;
		}
	}//fn


	public function verify_email(){
		$user_token = $this->session->userdata['wst23xyzsdfretw89lk_user_token'];
		$user_details = $this->frontmodel->get_user_info( $user_token );
		$user_status = $user_details['user_status'];
		if( $user_status == '1' ){
			return redirect(site_url());
			die();
		}
		$this->load->view('verify_email');
	}//fn

	public function verify_email_in_db(){
		$user_token = $this->session->userdata['wst23xyzsdfretw89lk_user_token'];
		$otp 		= $this->input->post('otp');
		$verify_email = $this->frontmodel->verify_email( $user_token, $otp );
		if( $verify_email ){
			return redirect(site_url('profile-info'));
		}else{
			$this->session->set_flashdata( 'login_failed' , 'Incorrect Otp.' );
			return redirect(site_url('verify'));
		}
	}//fn

	public function get_extra_profile_info(){
		$user_token = $this->session->userdata['wst23xyzsdfretw89lk_user_token'];
		$user_details = $this->frontmodel->get_user_info( $user_token );
		$user_info_phone = $user_details['user_info_phone'];
		$this->load->view('extra_profile_info', array(
			'user_info_phone' => $user_info_phone
		));
	}//fn

	public function save_extra_profile_info_to_db(){
		$user_token 			= $this->session->userdata['wst23xyzsdfretw89lk_user_token'];
		$fname 					= $this->input->post('fname');
		$lname 					= $this->input->post('lname');
		$user_info_address 		= $this->input->post('user_info_address');
		$user_info_city 		= $this->input->post('user_info_city');
		$user_info_state 		= $this->input->post('user_info_state');
		$user_info_zip 			= $this->input->post('user_info_zip');
		$user_info_phone 		= $this->input->post('user_info_phone');
		$user_info_country 		= $this->input->post('user_info_country');
		$user_info_gender 		= $this->input->post('user_info_gender');

		$data = array(
			'user_info_address' => $user_info_address,
			'user_info_city' 	=> $user_info_city,
			'user_info_state'	=> $user_info_state,
			'user_info_zip'		=> $user_info_zip,
			'user_info_phone'	=> $user_info_phone,
			'user_info_country'	=> $user_info_country,
			'user_info_fname'	=> $fname,
			'user_info_lname'	=> $lname,
			'user_info_gender'	=> $user_info_gender,
		);
		$insert = $this->frontmodel->save_extra_info( $data );
		// print_r($data);
		if( $insert ){
			return redirect(site_url());
		}else{
			return redirect(site_url('profile-info'));
		}
	}//fn


}
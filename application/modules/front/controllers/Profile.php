<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller{
	public $user_id;
	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();
		if( !isset($this->session->userdata['wst23xyzsdfretw89lk_user_token']) ){
			return redirect('login');
			die();
		}
		if( !isset($this->session->userdata['wst23xyzsdfretw89lk_beta_token']) ){
			return redirect('beta');
			die();
		} 
		$this->load->model('admin/adminmodel');
        $this->load->model('front/frontmodel');
        $user_token = $this->session->userdata['wst23xyzsdfretw89lk_user_token'];
		$user_details = $this->frontmodel->get_user_info( $user_token );
		$user_status = $user_details['user_status'];
		if( $user_status == '0' ){
			return site_url('verify');
		}
       	$this->user_id =  $user_details['user_id'];
        
        $this->load->model('admin/adminmodel');
        $this->load->model('front/frontmodel');
	}

	/*
	|--------------------------------------------------------------------------
	| Profile function
	|--------------------------------------------------------------------------
	*/

	public function index( $user_name= null ){
		if( $user_name == null ){
			$posts = $this->frontmodel->get_post_user(10, 0, $this->user_id);
			// echo '<pre>';
			// print_r($posts);
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);

			$profile_info = $this->frontmodel->get_user_by_token($this->session->userdata['wst23xyzsdfretw89lk_user_token']);
			// print_r($profile_info);
			$right_bar_view = $this->load->view('template/right-bar',array(
				// 'posts' => $posts
			), true);

			// ====== Main View ======
			$this->load->view('header', array(
				'header_data_array'	 => $this->data['header_data_array'],
				// 'admin_details'	 => $this->data['admin_details'],
			));
			$this->load->view('profile/personal_profile', array(
				'all_blog_view'	=> $blog_view,
				'profile'		=> $profile_info,
				'right_bar_view'	=> $right_bar_view,
			));
			$this->load->view('footer');
		}else{
			
			$profile_user_id = $this->frontmodel->get_user_id_by_username($user_name);
			$profile_user_token = $this->frontmodel->get_user_token_by_username($user_name);
			$posts = $this->frontmodel->get_post_user(10, 0, $profile_user_id);
			// echo '<pre>';
			// print_r($posts);
			$right_bar_view = $this->load->view('template/right-bar',array(
				// 'posts' => $posts
			), true);
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$profile_info = $this->frontmodel->get_user_by_token($profile_user_token);
			// ====== Main View ======
			$this->load->view('header', array(
				'header_data_array'	 => $this->data['header_data_array'],
				// 'admin_details'	 => $this->data['admin_details'],
			));
			$this->load->view('profile/other_profile', array(
				'all_blog_view'	=> $blog_view,
				'profile'		=> $profile_info,
				'right_bar_view'	=> $right_bar_view,
			));
			$this->load->view('footer');
		}
       	
		
	}//fn
	public function load_more_personal_posts(){
		$load_more_index 		= $this->input->post('load_more_index');
		$offset = $load_more_index * 10;
		// $offset = ($load_more_index * 10) - 10;
		// echo $limit.'-'.$offset;
		$posts = $this->frontmodel->get_post_user(10, $offset, $this->user_id);
		// echo '<pre>';
		// print_r($posts);
		if( $posts == null ){
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 0
			);
		}else{
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 1
			);
		}
		
		echo json_encode($array);
	}//fn

	public function load_more_others_posts(){
		$load_more_index 	= $this->input->post('load_more_index');
		$profile_id 		= $this->input->post('profie_id');
		$offset = $load_more_index * 10;
		// $offset = ($load_more_index * 10) - 10;
		// echo $limit.'-'.$offset;
		$posts = $this->frontmodel->get_post_user(10, $offset, $profile_id);
		// echo '<pre>';
		// print_r($posts);
		if( $posts == null ){
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 0
			);
		}else{
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 1
			);
		}
		
		echo json_encode($array);
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Edit Profile function
	|--------------------------------------------------------------------------
	*/
	
	public function edit_profile(  ){
		
		$profile_info = $this->frontmodel->get_user_by_token($this->session->userdata['wst23xyzsdfretw89lk_user_token']);
		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);
       	// ====== Main View ======
       	$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
			// 'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('profile/edit_profile', array(
			// 'all_blog_view'	=> $blog_view,
			'profile'		=> $profile_info,
			'right_bar_view'	=> $right_bar_view,
		));
		$this->load->view('footer');

	}//fn

	


	

}//class
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MY_Controller{
		 
	public $user_id;
	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();
        if( !isset($this->session->userdata['wst23xyzsdfretw89lk_user_token']) ){
			return redirect('login');
			die();
		}
		if( !isset($this->session->userdata['wst23xyzsdfretw89lk_beta_token']) ){
			return redirect('beta');
			die();
		} 
		$this->load->model('admin/adminmodel');
        $this->load->model('front/frontmodel');
        $user_token = $this->session->userdata['wst23xyzsdfretw89lk_user_token'];
		$user_details = $this->frontmodel->get_user_info( $user_token );
		$user_status = $user_details['user_status'];
		if( $user_status == '0' ){
			return site_url('verify');
		}
       	$this->user_id =  $user_details['user_id'];

        
        
	}

	/*
	|--------------------------------------------------------------------------
	| Post function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		$posts = $this->frontmodel->get_post(10,0);
		// echo '<pre>';
		// print_r($posts);
		$blog_view = $this->load->view('template/blog',array(
			'posts' => $posts
		), true);

		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);

		$popular_posts = $this->frontmodel->get_popular_posts();
		$popular_post_view = $this->load->view('template/popular_post_loop', array(
			'popular_posts'		=> $popular_posts
		), true);
		$all_cat = $this->adminmodel->get_cat_list();

		$profile_info = $this->frontmodel->get_user_by_token($this->session->userdata['wst23xyzsdfretw89lk_user_token']);

		$fetch_notification = $this->frontmodel->fetch_notification( $this->user_id ); 
		// echo '<pre>';
		// print_r($fetch_notification);
		// ====== Main View ======
		
		$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
		));
		$this->load->view('home',array(
			'cat_list' 			=> $all_cat,
			'all_blog_view'		=> $blog_view,
			'right_bar_view'	=> $right_bar_view,
			'popular_post_view'	=> $popular_post_view,
			'profile'			=> $profile_info,
			'notification'		=> $fetch_notification,
		));
		$this->load->view('footer');
		
	}//fn

	public function load_more_posts(){
		$load_more_index 		= $this->input->post('load_more_index');
		$offset = $load_more_index * 10;
		// $offset = ($load_more_index * 10) - 10;
		// echo $limit.'-'.$offset;
		$posts = $this->frontmodel->get_post(10, $offset);
		// echo '<pre>';
		// print_r($posts);
		if( $posts == null ){
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 0
			);
		}else{
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 1
			);
		}
		
		echo json_encode($array);
	}//fn

	public function shorts(){
		$posts = $this->frontmodel->get_shorts_post(10,0);
		// echo '<pre>';
		// print_r($posts);
		$blog_view = $this->load->view('template/blog',array(
			'posts' => $posts
		), true);

		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);

		
		$all_cat = $this->adminmodel->get_cat_list();

		$profile_info = $this->frontmodel->get_user_by_token($this->session->userdata['wst23xyzsdfretw89lk_user_token']);

		$fetch_notification = $this->frontmodel->fetch_notification( $this->user_id ); 
		// echo '<pre>';
		// print_r($fetch_notification);
		// ====== Main View ======
		
		$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
		));
		$this->load->view('shorts',array(
			'cat_list' 			=> $all_cat,
			'all_blog_view'		=> $blog_view,
			'right_bar_view'	=> $right_bar_view,
			// 'popular_post_view'	=> $popular_post_view,
			'profile'			=> $profile_info,
			'notification'		=> $fetch_notification,
		));
		$this->load->view('footer');
		
	}//fn
	public function load_more_shorts_posts(){
		$load_more_index 		= $this->input->post('load_more_index');
		$offset = $load_more_index * 10;
		// $offset = ($load_more_index * 10) - 10;
		// echo $limit.'-'.$offset;
		$posts = $this->frontmodel->get_shorts_post(10, $offset);
		// echo '<pre>';
		// print_r($posts);
		if( $posts == null ){
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 0
			);
		}else{
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 1
			);
		}
		
		echo json_encode($array);
	}//fn
	public function shots(){
		$posts = $this->frontmodel->get_shots_post(10,0);
		// echo '<pre>';
		// print_r($posts);
		$blog_view = $this->load->view('template/blog',array(
			'posts' => $posts
		), true);

		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);

		
		$all_cat = $this->adminmodel->get_cat_list();

		$profile_info = $this->frontmodel->get_user_by_token($this->session->userdata['wst23xyzsdfretw89lk_user_token']);

		$fetch_notification = $this->frontmodel->fetch_notification( $this->user_id ); 
		// echo '<pre>';
		// print_r($fetch_notification);
		// ====== Main View ======
		
		$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
		));
		$this->load->view('shots',array(
			'cat_list' 			=> $all_cat,
			'all_blog_view'		=> $blog_view,
			'right_bar_view'	=> $right_bar_view,
			// 'popular_post_view'	=> $popular_post_view,
			'profile'			=> $profile_info,
			'notification'		=> $fetch_notification,
		));
		$this->load->view('footer');
		
	}//fn
	public function load_more_shots_posts(){
		$load_more_index 		= $this->input->post('load_more_index');
		$offset = $load_more_index * 10;
		// $offset = ($load_more_index * 10) - 10;
		// echo $limit.'-'.$offset;
		$posts = $this->frontmodel->get_shots_post(10, $offset);
		// echo '<pre>';
		// print_r($posts);
		if( $posts == null ){
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 0
			);
		}else{
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 1
			);
		}
		
		echo json_encode($array);
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Add Post function
	|--------------------------------------------------------------------------
	*/

	public function add_post_db(){
		$title 		= $this->input->post('title');
		$slug 		= $this->frontmodel->generate_post_slug( $title );
		$content 	= $this->input->post('content');
		$cat_id 	= $this->input->post('cat_id');
		$template 	= $this->input->post('template');

		$image 		= $this->input->post('img');
		if( $image != '' ){
			list($type, $image) = explode(';',$image);
			list(, $image) = explode(',',$image);
			$image = base64_decode($image);
			$image_name = time().'.jpeg';
			$image_size = file_put_contents(APPPATH.'../uploads/post/'.$image_name, $image);
			if( $image_size ){
				// add img details to gallery table and store the $gallery_id
				$gallery_data = array(
					'user_id'	=> $this->user_id,
					'file_name' => $image_name,
					'file_url' 	=> site_url('uploads/post/').$image_name,
					'file_size' 	=> $image_size,
					'gallery_type' 	=> '1',
				);
				$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
				$gallery_id = $this->db->insert_id();
				$featured_img_url = site_url('uploads/post/').$image_name;
			}else{
				$array = array(
					'code' 		=> 0,
					'message' 	=> 'Error in image upload'
				);
				echo json_encode($array);
				die();
			}
			
		}else{
			$featured_img_url = null;
			$gallery_id 	= null;
		}

		$post_data = array(
			'user_id' 			=> $this->user_id,
			'post_slug' 		=> $slug,
			'post_title' 		=> $title,
			'post_content' 		=> $content,
			'featured_img_url' 	=> $featured_img_url,
			'featured_img_id' 	=> $gallery_id,
			'cat_id' 			=> $cat_id,
			'template' 			=> $template,
		);

		
		$insert_post = $this->db->insert('wst_posts', $post_data);
		if( $insert_post ){
			$update_user_cat_stat = $this->frontmodel->update_user_cat_stat($this->user_id, $cat_id);
			if( $update_user_cat_stat ){
				$array = array(
					'code'	=> 1,
					'message'	=> 'Succesfully post created'
				);
				echo json_encode($array);
				die();
			}else{
				$array = array(
					'code' 		=> 0,
					'message' 	=> 'Error in Updateing the user category statistics'
				);
				echo json_encode($array);
				die();
			}
		}else{
			$array = array(
				'code' 		=> 0,
				'message' 	=> 'Error in Post upload to database'
			);
			echo json_encode($array);
			die();
		}

	}//fn

	public function add_post_db_mobile(){
		$title 		= $this->input->post('title');
		$slug 		= $this->frontmodel->generate_post_slug( $title );
		$content 	= $this->input->post('content');
		$cat_id 	= $this->input->post('cat_id');
		$template 	= $this->input->post('template');

		if( isset($_FILES["pstimg"]["name"]) ){
			$config['upload_path']          = './uploads/post/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 2000;
            $config['max_width']            = 5000;
            $config['max_height']           = 3000;
            $config['file_name']           	= time().'.jpeg';
            $this->load->library('upload', $config);
            if ( !$this->upload->do_upload('pstimg')){
            	$array = array(
					'code' 		=> 0,
					'message' 	=> strip_tags( $this->upload->display_errors() ),
				);
				echo json_encode($array);
				die();
            }else{
            	$uploaded_data =  $this->upload->data();
            	$gallery_data = array(
					'user_id'	=> $this->user_id,
					'file_name' => $uploaded_data["file_name"],
					'file_url' 	=> site_url('uploads/post/').$uploaded_data["file_name"],
					'file_size' 	=> $uploaded_data["file_size"],
					'gallery_type' 	=> '1',
				);
				$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
				$gallery_id = $this->db->insert_id();
				$featured_img_url = site_url('uploads/post/').$uploaded_data["file_name"];
            }
		}else{
			$featured_img_url = null;
			$gallery_id 	= null;
		}
		$post_data = array(
			'user_id' 			=> $this->user_id,
			'post_slug' 		=> $slug,
			'post_title' 		=> $title,
			'post_content' 		=> $content,
			'featured_img_url' 	=> $featured_img_url,
			'featured_img_id' 	=> $gallery_id,
			'cat_id' 			=> $cat_id,
			'template' 			=> $template,
		);

		
		$insert_post = $this->db->insert('wst_posts', $post_data);
		if( $insert_post ){
			$update_user_cat_stat = $this->frontmodel->update_user_cat_stat($this->user_id, $cat_id);
			if( $update_user_cat_stat ){
				$array = array(
					'code'	=> 1,
					'message'	=> 'Succesfully post created'
				);
				echo json_encode($array);
				die();
			}else{
				$array = array(
					'code' 		=> 0,
					'message' 	=> 'Error in Updateing the user category statistics'
				);
				echo json_encode($array);
				die();
			}
		}else{
			$array = array(
				'code' 		=> 0,
				'message' 	=> 'Error in Post upload to database'
			);
			echo json_encode($array);
			die();
		}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Single Post nad Edit Post function
	|--------------------------------------------------------------------------
	*/
	public function single_post( $post_slug ){
		// fetch post details by slug from the table.
		$post_details = $this->frontmodel->get_post_by_slug($post_slug);
		$user_id = $post_details['user_id'];
		$profile_images = $this->frontmodel->get_all_profile_image($user_id );
		// echo '<pre>';
		// print_r($post_details);
		// print_r($profile_images);
		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);
		// ====== Main View ======
		$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
		));
		$this->load->view('post/single_post',array(
			'post_details' 	=> $post_details,
			'profile_images'	=> $profile_images,
			'right_bar_view'	=> $right_bar_view,
			// 'profile'		=> $profile_info,
		));
		$this->load->view('footer');
		//load views with post details
	}//fn

	public function edit_post($post_id){
		$post_details = $this->frontmodel->get_post_by_id($post_id);
		$user_id = $post_details['user_id'];
		$all_cat = $this->adminmodel->get_cat_list();
		// echo '<pre>';
		// print_r($post_details);
		// print_r($all_cat);
		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);
		// ====== Main View ======
		$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
		));
		$this->load->view('post/edit_post',array(
			'post_details' 	=> $post_details,
			'all_cat'		=> $all_cat,
			'right_bar_view'	=> $right_bar_view,
			// 'profile'		=> $profile_info,
		));
		$this->load->view('footer');
	}//fn

	public function edit_post_to_db(){
		$post_title 	= $this->input->post('post_title');
		$cat_id 		= $this->input->post('cat_id');
		$post_content 	= $this->input->post('post_content');
		$post_id 		= $this->input->post('post_id');

		$data = array(
			'post_title' 	=>$post_title,
			'cat_id' 		=>$cat_id,
			'post_content' 	=>$post_content,
		);
		// print_r($data);
		$update = $this->db->update('wst_posts', $data, array('post_id' => $post_id));
		if( $update ){
			$this->session->set_flashdata( 'profile_updated' , 'Post Updated Succesfully' );
			return redirect(site_url('edit-post/').$post_id);
		}else{
			$this->session->set_flashdata( 'profile_updated_fail' , 'Something Went Wrong!' );
			return redirect(site_url('edit-post/').$post_id);
		}
	}//fn

	public function report_post($post_slug){
		$post_details = $this->frontmodel->get_post_by_slug($post_slug);
		$author_id = $post_details['user_id'];
		$post_id = $post_details['post_id'];
		$redirect_url = $_GET['redirect'];
		if( $this->frontmodel->if_already_reported( $post_id, $this->user_id ) ){
			// echo '<script>alert("Already Reported")</script>';
			return redirect($redirect_url);
			die();
		}
		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);
		$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
		));
		$this->load->view('post/report_post',array(
			'post_details' 	=> $post_details,
			'redirect_url'	=> $redirect_url,
			'right_bar_view'	=> $right_bar_view,
			// 'profile'		=> $profile_info,
		));
		$this->load->view('footer');
	}//fn

	public function report_post_to_db(){
		$report_reason 			= $this->input->post('report_reason');
		$report_reason_explain 	= $this->input->post('report_reason_explain');
		$post_id 				= $this->input->post('post_id');
		$author_id 				= $this->input->post('author_id');
		$redirect_url 			= $this->input->post('redirect_url');

		$data = array(
			'post_id' 				=> $post_id,
			'user_id' 				=> $this->user_id,
			'author_id' 			=> $author_id,
			'report_reason' 		=> $report_reason,
			'report_reason_explain' => $report_reason_explain,
			'redirect_url' 			=> $redirect_url,
		);
		// print_r($data);
		$insert = $this->db->insert( 'wst_report_posts', $data );
		if( $insert ){
			return redirect($redirect_url);
		}else{
			return redirect(site_url());
		}

	}//fn

	public function update_post_image(){
		$post_id 		= $this->input->post('post_id');
		$image 		= $this->input->post('img');
		$old_image_id = $this->frontmodel->get_img_id_for_post( $post_id );
		if( $image != '' ){
			list($type, $image) = explode(';',$image);
			list(, $image) = explode(',',$image);
			$image = base64_decode($image);
			$image_name = time().'.jpeg';
			$image_size = file_put_contents(APPPATH.'../uploads/post/'.$image_name, $image);
			if( $image_size ){
				// add img details to gallery table and store the $gallery_id
				$gallery_data = array(
					'user_id'	=> $this->user_id,
					'file_name' => $image_name,
					'file_url' 	=> site_url('uploads/post/').$image_name,
					'file_size' 	=> $image_size,
					'gallery_type' 	=> '1',
				);
				$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
				$gallery_id = $this->db->insert_id();
				$featured_img_url = site_url('uploads/post/').$image_name;
			}else{
				$array = array(
					'code' 		=> 0,
					'message' 	=> 'Error in image upload'
				);
				echo json_encode($array);
				die();
			}
			
		}else{
			$featured_img_url = null;
			$gallery_id 	= null;
		}

		$post_data = array(
			'featured_img_url' 	=> $featured_img_url,
			'featured_img_id' 	=> $gallery_id,
		);

		
		$update_post = $this->db->update('wst_posts', $post_data, array('post_id' => $post_id));
		if( $update_post ){
			// $update_user_cat_stat = $this->frontmodel->update_user_cat_stat($this->user_id, $cat_id);
			// if( $update_user_cat_stat ){
			if( $old_image_id ){
				$old_delete = $this->frontmodel->delete_old_image_after_post_image_update($old_image_id);
				if( $old_delete ){
					$array = array(
						'code'	=> 1,
						'message'	=> 'Succesfully image updated'
					);
					$this->session->set_flashdata( 'profile_updated' , 'Post Updated Succesfully' );
					echo json_encode($array);
					//delete the old image.

					die();
				}else{
					$array = array(
						'code'	=> 5,
						'message'	=> 'delete problem'
					);
					echo json_encode($array);
					//delete the old image.

					die();
				}
			
			}else{
				$array = array(
						'code'	=> 5,
						'message'	=> 'old image id problem'
					);
					echo json_encode($array);
			}
			
		}else{
			$array = array(
				'code' 		=> 0,
				'message' 	=> 'Error in Post upload to database'
			);
			echo json_encode($array);
			die();
		}

	}//fn
	public function update_post_image_mobile(){
		$post_id 		= $this->input->post('post_id');
		$old_image_id = $this->frontmodel->get_img_id_for_post( $post_id );
		if( isset($_FILES["pstimg"]["name"]) ){
			$config['upload_path']          = './uploads/post/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 2000;
            $config['max_width']            = 5000;
            $config['max_height']           = 3000;
            $config['file_name']           	= time().'.jpeg';
            $this->load->library('upload', $config);
            if ( !$this->upload->do_upload('pstimg')){
            	$array = array(
					'code' 		=> 0,
					'message' 	=> strip_tags( $this->upload->display_errors() ),
				);
				echo json_encode($array);
				die();
            }else{
            	$uploaded_data =  $this->upload->data();
            	$gallery_data = array(
					'user_id'	=> $this->user_id,
					'file_name' => $uploaded_data["file_name"],
					'file_url' 	=> site_url('uploads/post/').$uploaded_data["file_name"],
					'file_size' 	=> $uploaded_data["file_size"],
					'gallery_type' 	=> '1',
				);
				$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
				$gallery_id = $this->db->insert_id();
				$featured_img_url = site_url('uploads/post/').$uploaded_data["file_name"];
            }
		}else{
			$featured_img_url = null;
			$gallery_id 	= null;
		}

		$post_data = array(
			'featured_img_url' 	=> $featured_img_url,
			'featured_img_id' 	=> $gallery_id,
		);
		$update_post = $this->db->update('wst_posts', $post_data, array('post_id' => $post_id));
		if( $update_post ){
			if( $old_image_id ){
				$old_delete = $this->frontmodel->delete_old_image_after_post_image_update($old_image_id);
				if( $old_delete ){
					$array = array(
						'code'	=> 1,
						'message'	=> 'Succesfully image updated'
					);
					$this->session->set_flashdata( 'profile_updated' , 'Post Updated Succesfully' );
					echo json_encode($array);
					//delete the old image.

					die();
				}else{
					$array = array(
						'code'	=> 5,
						'message'	=> 'delete problem'
					);
					echo json_encode($array);
					//delete the old image.

					die();
				}
			
			}else{
				$array = array(
						'code'	=> 5,
						'message'	=> 'old image id problem'
					);
					echo json_encode($array);
			}
			
			
		}else{
			$array = array(
				'code' 		=> 9,
				'message' 	=> 'Error in Post upload to database'
			);
			echo json_encode($array);
			die();
		}
	}//fn

	public function delete_post_db(){
		$post_id 		= $this->input->post('post_id');
		$old_image_id = $this->frontmodel->get_img_id_for_post( $post_id );
		$delete_post = $this->db->delete('wst_posts', array('post_id' => $post_id));
		if( $delete_post ){
			$delete_like  		= $this->db->delete('wst_posts_like', array('post_id' => $post_id));
			$delete_favourite  = $this->db->delete('wst_favourite_post', array('post_id' => $post_id));
			$array = array(
				'code' 		=> 1,
			);
			echo json_encode($array);
			die();
		}else{
			$array = array(
				'code' 		=> 0,
				'message' 	=> 'Error in Post delete from database'
			);
			echo json_encode($array);
			die();
		}
	}
	/*
	|--------------------------------------------------------------------------
	| Upload Cover function
	|--------------------------------------------------------------------------
	*/

	public function upload_cover_image(){
		$image 	= $this->input->post('img');
		if( $image != '' ){
			list($type, $image) = explode(';',$image);
			list(, $image) = explode(',',$image);
			$image = base64_decode($image);
			$image_name = time().'.jpeg';
			$image_size = file_put_contents(APPPATH.'../uploads/cover/'.$image_name, $image);
			if( $image_size ){
				$gallery_data = array(
					'user_id'	=> $this->user_id,
					'file_name' => $image_name,
					'file_url' 	=> site_url('uploads/cover/').$image_name,
					'file_size' 	=> $image_size,
					'gallery_type' 	=> '2',
				);
				$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
				$gallery_id = $this->db->insert_id();
				$cover_img_url = site_url('uploads/cover/').$image_name;

				$update = $this->db->update('user_info',array('user_cover_image'=>$cover_img_url),array('user_id'=>$this->user_id));
				if( $update ){
					$array = array(
						'code' 		=> 1,
						'message'	=> 'Cover Picture Uploaded'
					);
					echo json_encode($array);
				}else{
					$array = array(
						'code' 		=> 0,
						'message'	=> 'database Error'
					);
					echo json_encode($array);
				}
			}else{
				$array = array(
					'code' 		=> 0,
					'message'	=> 'Upload Error'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				'code' 		=> 0,
				'message'	=> 'Empty in image'
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Upload Profile Image function
	|--------------------------------------------------------------------------
	*/

	public function upload_profile_image(){
		$image 	= $this->input->post('img');
		if( $image != '' ){
			list($type, $image) = explode(';',$image);
			list(, $image) = explode(',',$image);
			$image = base64_decode($image);
			$image_name = time().'.jpeg';
			$image_size = file_put_contents(APPPATH.'../uploads/profile/'.$image_name, $image);
			if( $image_size ){
				$gallery_data = array(
					'user_id'	=> $this->user_id,
					'file_name' => $image_name,
					'file_url' 	=> site_url('uploads/profile/').$image_name,
					'file_size' 	=> $image_size,
					'gallery_type' 	=> '0',
				);
				$insert_gallery = $this->db->insert('wst_gallery', $gallery_data);
				$gallery_id = $this->db->insert_id();
				$cover_img_url = site_url('uploads/profile/').$image_name;

				$update = $this->db->update('user_info',array('user_image'=>$cover_img_url),array('user_id'=>$this->user_id));
				if( $update ){
					$array = array(
						'code' 		=> 1,
						'message'	=> 'Profile Picture Uploaded'
					);
					echo json_encode($array);
				}else{
					$array = array(
						'code' 		=> 0,
						'message'	=> 'database Error'
					);
					echo json_encode($array);
				}
			}else{
				$array = array(
					'code' 		=> 0,
					'message'	=> 'Upload Error'
				);
				echo json_encode($array);
			}
		}else{
			$array = array(
				'code' 		=> 0,
				'message'	=> 'Empty in image'
			);
			echo json_encode($array);
		}
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Save Profile Personal and contact Info function
	|--------------------------------------------------------------------------
	*/

	public function save_profile_personal_info(){
		$user_info_fname 	= $this->input->post('user_info_fname');
		$user_info_lname 	= $this->input->post('user_info_lname');
		$user_info_gender 	= $this->input->post('user_info_gender');
		$user_bio 			= $this->input->post('user_bio');
		$user_profession 	= $this->input->post('user_profession');
		$show_gender 		= $this->input->post('show_gender');
		if( !isset($show_gender) ){
			$show_gender = '0';
		}
		$show_name 			= $this->input->post('show_name');
		if( !isset($show_name) ){
			$show_name = '0';
		}
		$show_bio 			= $this->input->post('show_bio');
		if( !isset($show_bio) ){
			$show_bio = '0';
		}
		$show_profession 			= $this->input->post('show_profession');
		if( !isset($show_profession) ){
			$show_profession = '0';
		}
		$data = array(
			'user_info_fname' 		=> $user_info_fname,
			'user_info_lname' 		=> $user_info_lname,
			'show_name' 			=> $show_name,
			'user_info_gender' 		=> $user_info_gender,
			'user_profession' 		=> $user_profession,
			'show_gender' 			=> $show_gender,
			'user_bio' 				=> $user_bio,
			'show_bio' 				=> $show_bio,
			'show_profession' 		=> $show_profession,
		);
		$update = $this->db->update('user_info', $data, array('user_id' => $this->user_id));
		if( $update ){
			$this->session->set_flashdata( 'profile_updated' , 'Profile Updated Succesfully' );
			return redirect(site_url('/edit-profile'));
		}else{
			$this->session->set_flashdata( 'profile_updated_fail' , 'Something Went Wrong!' );
			return redirect(site_url('/edit-profile'));
		}
		// print_r($data);
	}//fn

	public function save_profile_contact_info(){
		$show_address 		= $this->input->post('show_address');
		if( !isset($show_address) ){
			$show_address = '0';
		}
		$user_info_phone 	= $this->input->post('user_info_phone');
		$user_info_address 	= $this->input->post('user_info_address');
		$user_info_city 	= $this->input->post('user_info_city');
		$user_info_state 	= $this->input->post('user_info_state');
		$user_info_zip 		= $this->input->post('user_info_zip');
		$user_info_country 	= $this->input->post('user_info_country');
		$data = array(
			'show_address' 			=> $show_address,
			'user_info_phone' 		=> $user_info_phone,
			'user_info_address' 	=> $user_info_address,
			'user_info_city' 		=> $user_info_city,
			'user_info_state' 		=> $user_info_state,
			'user_info_zip' 		=> $user_info_zip,
			'user_info_country' 	=> $user_info_country,
		);
		$update = $this->db->update('user_info', $data, array('user_id' => $this->user_id));
		if( $update ){
			$this->session->set_flashdata( 'profile_updated' , 'Profile Updated Succesfully' );
			return redirect('/edit-profile');
		}else{
			$this->session->set_flashdata( 'profile_updated_fail' , 'Something Went Wrong!' );
			return redirect('/edit-profile');
		}
		// print_r($data);
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Like Unlike checklike function
	|--------------------------------------------------------------------------
	*/

	public function check_like(){
		$post_id 		= $this->input->post('post_id');
		$q = $this->db
					->where( array('post_id' => $post_id, 'user_id' => $this->user_id) )
					->get('wst_posts_like');
		if( $q->num_rows() > 0 ){
			echo 1;
		}else{
			echo 0;
		}
	}//fn

	public function add_like(){
		$post_id 		= $this->input->post('post_id');
		$author_id		= $this->input->post('author_id');
		$author_token 	= $this->input->post('author_token');
		$post_url 		= $this->input->post('post_url');
		$data = array(
			'post_id'		=> $post_id,
			'user_id'		=> $this->user_id,
		);
		$insert = $this->db->insert('wst_posts_like', $data);
		if( $insert ){
			$user_full_name  	= $this->frontmodel->get_user_name_by_id($this->user_id);
			$content 			= '<b>'.$user_full_name.'</b> liked your post.';

			$not_data 			= array(
				'not_type' 		=> '0',
				'not_user_id'	=> $author_id,
				'not_user_token'=> $author_token,
				'content'		=> $content,
				'not_url'		=> $post_url,
				'not_status'	=> '0',
				'created_by'	=> $this->user_id,
			);
			if( $author_id == $this->user_id ){
				$insert_not = true;
			}else{
				$insert_not = $this->db->insert('wst_notification', $not_data);
				$liked_by_details = $this->frontmodel->get_user_by_token( $this->session->userdata['wst23xyzsdfretw89lk_user_token'] );
				//firebase notification
				$repl = site_url('/post/');
				$post_slug = str_replace($repl, '', $post_url);
				$details = array(
					'notification_id'	=> $this->db->insert_id(),
					'token' 			=> $this->session->userdata['wst23xyzsdfretw89lk_user_token'],
					'username'			=> $liked_by_details['user_name'],
					'user_info_fname'	=> $liked_by_details['user_info_fname'],
					'user_info_lname'	=> $liked_by_details['user_info_lname'],
					'user_image'		=> str_replace('localhost','192.168.0.99',$liked_by_details['user_image']),
					'show_name'			=> $liked_by_details['show_name'],
					'show_image'		=> $liked_by_details['show_image'],
					'message'			=> '[[[author_name]]] has liked your post',
					'details'			=> array('notification_type' => 1, 'post_slug'=> $post_slug)
				);
				//code => 1 (general notofication not inserted in db)
				//code => 2 (ws notification inserted in db)
				$notification = array(
					'code' 	=> 2,
					'data' 	=> $details
				);
				$token = $this->frontmodel->get_user_device_token($author_id);
				// $this->push_notification( $body, $author_id);
				$this->push_notification_like_post( $token, $notification, $author_id);
			}
			
			if( $insert_not ){
				echo 1;
			}else{
				echo 5;
			}
		}else{
			echo 0;
		}
	}//fn

	public function remove_like(){
		$post_id = $this->input->post('post_id');

		$delete = $this->db->delete('wst_posts_like', array( 'post_id' => $post_id,'user_id' => $this->user_id, ));
		if( $delete ){
			echo 1;
		}else{
			echo 0;
		}
	}//fn

	

	/*
	|--------------------------------------------------------------------------
	| Favourite function
	|--------------------------------------------------------------------------
	*/
	public function check_favourite(){
		$post_id 		= $this->input->post('post_id');
		$q = $this->db
					->where( array('post_id' => $post_id, 'user_id' => $this->user_id) )
					->get('wst_favourite_post');
		if( $q->num_rows() > 0 ){
			echo 1;
		}else{
			echo 0;
		}
	}//fn

	public function add_favourite(){
		$post_id 		= $this->input->post('post_id');
		$author_id		= $this->input->post('author_id');
		$data = array(
			'post_id'		=> $post_id,
			'user_id'		=> $this->user_id,
			'author_id'		=> $author_id,
		);
		$insert = $this->db->insert('wst_favourite_post', $data);
		if( $insert ){
			echo 1;
		}else{
			echo 0;
		}
	}//fn

	public function remove_favourite(){
		$post_id = $this->input->post('post_id');

		$delete = $this->db->delete('wst_favourite_post', array( 'post_id' => $post_id,'user_id' => $this->user_id, ));
		if( $delete ){
			echo 1;
		}else{
			echo 0;
		}
	}//fn
	public function favourites(  ){
		$profile_info = $this->frontmodel->get_user_by_token($this->session->userdata['wst23xyzsdfretw89lk_user_token']);
		$posts = $this->frontmodel->get_favourite_post( $this->user_id );
		$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);
		$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
			// 'admin_details'	 => $this->data['admin_details'],
		));
		$this->load->view('favourite/favourites', array(
			'all_blog_view'	=> $blog_view,
			'profile'		=> $profile_info,
			'right_bar_view'	=> $right_bar_view,
		));
		$this->load->view('footer');
	}
	/*
	|--------------------------------------------------------------------------
	| Notification functions
	|--------------------------------------------------------------------------
	*/

	public function fetch_unseen_notification_number(){
		$q_unseen = $this->db
                    ->where(array('not_status' => '0', 'not_user_id' => $this->user_id))
                    ->get('wst_notification');

        if( $q_unseen->num_rows() > 0 ){
            echo $q_unseen->num_rows();
            
        }else{
            echo 0;
        }
	}

	public function load_modal_notification_data(){
		$header_notifications	= $this->frontmodel->fetch_header_notification( $this->user_id );

		$notification_list_view = $this->load->view('template/modal_notification_list',array(
			'notifications' => $header_notifications
		), true);

		echo $notification_list_view;

		$update_not = $this->db->update('wst_notification',array('not_status' => '1'), array('not_user_id' => $this->user_id ));

	}//fn

	public function change_notification_status_to_seen(){
		$notification_id = $this->input->post('notification_id');
		$update = $this->db->update('wst_notification', array('not_status' => '1'), array('not_id' => $notification_id));
		if( $update ){
    		echo 1;
    	}else{
    		echo 0;
    	}
	}//fn

	public function change_notification_status_to_read(){
		$notification_id = $this->input->post('notification_id');
		$update = $this->db->update('wst_notification', array('not_status' => '2'), array('not_id' => $notification_id));
		if( $update ){
    		echo 1;
    	}else{
    		echo 0;
    	}
	}//fn
	/*
	|--------------------------------------------------------------------------
	| Search functions
	|--------------------------------------------------------------------------
	*/

	public function ws_search(){
		$keyword = $this->input->post('keyword');

		$search_post_by_keyword = $this->frontmodel->search_post_by_keyword($keyword);
		$search_post_list_view = $this->load->view('template/search-post-list',array(
			'search_post_by_keyword' => $search_post_by_keyword
		), true);

		$search_user_by_keyword = $this->frontmodel->search_user_by_keyword($keyword);
		$search_user_list_view = $this->load->view('template/search-user-list',array(
			'search_user_by_keyword' => $search_user_by_keyword
		), true);

		$array = array(
			'code' => 1,
			'post_list' 	=> $search_post_list_view,
			'people_list' 	=> $search_user_list_view,

		);
		echo json_encode($array);
	}//fn


	public function test_mail(){
		$this->load->library('email');
        //SMTP & mail configuration

        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'palefire2019@gmail.com',
            'smtp_pass' => '1pale2fire',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        $this->email->to('sarasij94@gmail.com');
        $this->email->from('palefire2019@gmail.com','Walledstory');
        $this->email->subject('OTP');
        $this->email->message( 'hello' );

        if(!$this->email->send()){
            print_r($this->email->print_debugger());
        }else{
            echo  1;
        }
	}//fn

	public function test_push_notification(){
		$token = 'cllMfBIsSi6PgW162LGYsY:APA91bHAkGbzyoU4OmsBW7-7bgrecLAtTSWlcFoHXcAe85LXLzntVoLQgaaPdLKcNjst5pIjnlKkwIUzKxmj0zXuFPo933txc0pBEBccSFW0vT3YYjdihg99JkTB3QqBr2EDTnCouHmJ';
		$author_id = $this->user_id;
		$notification = array(
			'id'		=> '1',
		);
		$this->push_notification_like_post( $token, $notification, $author_id);
	}//fn

}//class
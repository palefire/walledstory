<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller{
	public $user_id;
	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/

	function __construct(){
        parent::__construct();

        if( !isset($this->session->userdata['wst23xyzsdfretw89lk_user_token']) ){
			return redirect('login');
			die();
		}
		if( !isset($this->session->userdata['wst23xyzsdfretw89lk_beta_token']) ){
			return redirect('beta');
			die();
		} 
		$this->load->model('admin/adminmodel');
        $this->load->model('front/frontmodel');
        $user_token = $this->session->userdata['wst23xyzsdfretw89lk_user_token'];
		$user_details = $this->frontmodel->get_user_info( $user_token );
		$user_status = $user_details['user_status'];
		if( $user_status == '0' ){
			return site_url('verify');
		}
       	$this->user_id =  $user_details['user_id'];
        
        $this->load->model('admin/adminmodel');
        $this->load->model('front/frontmodel');
        
	}

	/*
	|--------------------------------------------------------------------------
	| Categories function
	|--------------------------------------------------------------------------
	*/

	public function index(){
		$profile_info = $this->frontmodel->get_user_by_token($this->session->userdata['wst23xyzsdfretw89lk_user_token']);
		$fetch_notification = $this->frontmodel->fetch_notification( $this->user_id ); 
		$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);
		$this->load->view('header', array(
			'header_data_array'	 => $this->data['header_data_array'],
		));
		$this->load->view('category/all_category', array(
			'profile'			=> $profile_info,
			'notification'		=> $fetch_notification,
			'right_bar_view'	=> $right_bar_view,
		));
		$this->load->view('footer');
		
	}//fn

	public function category( $cat_slug ){
		$cat_id = $this->frontmodel->get_cat_id_by_cat_slug($cat_slug);
		$cat_name = $this->frontmodel->get_cat_name_by_cat_slug($cat_slug);
		if( $cat_id ){
			$posts = $this->frontmodel->get_post_by_cat(10, 0, $cat_id);
			// print_r($posts);
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$right_bar_view = $this->load->view('template/right-bar',array(
			// 'posts' => $posts
		), true);
			$profile_info = $this->frontmodel->get_user_by_token($this->session->userdata['wst23xyzsdfretw89lk_user_token']);
			// ====== Main View ======
			$this->load->view('header', array(
				'header_data_array'	 => $this->data['header_data_array'],
				// 'admin_details'	 => $this->data['admin_details'],
			));
			$this->load->view('category/single_category', array(
				'all_blog_view'	=> $blog_view,
				'profile'		=> $profile_info,
				'cat_id'		=> $cat_id,
				'cat_name'		=> $cat_name,
				'right_bar_view'	=> $right_bar_view,
			));
			$this->load->view('footer');
		}else{
			// load a view for cat does not exist
		}
	}//fn

	public function load_more_cat_posts(){
		$load_more_index 		= $this->input->post('load_more_index');
		$cat_id 		= $this->input->post('cat_id');
		$offset = $load_more_index * 10;
		// $offset = ($load_more_index * 10) - 10;
		// echo $limit.'-'.$offset;
		$posts = $this->frontmodel->get_post_by_cat(10, $offset, $cat_id);
		// echo '<pre>';
		// print_r($posts);
		if( $posts == null ){
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 0
			);
		}else{
			$blog_view = $this->load->view('template/blog',array(
				'posts' => $posts
			), true);
			$array = array(
				'posts'		=> $blog_view,
				'more_exist'=> 1
			);
		}
		
		echo json_encode($array);
	}//fn

	/*
	|--------------------------------------------------------------------------
	| Categories function
	|--------------------------------------------------------------------------
	*/


	

}//class
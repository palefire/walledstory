<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class Frontmodel extends CI_model {


    /**
     * CONSTRUCTOR FUNCTION
     */
    public function __construct() {
        parent::__construct();
        // Your own constructor code
    }

    /*
    |--------------------------------------------------------------------------
    | Login function
    |--------------------------------------------------------------------------
    */
    /**
     * Checklogin FUNCTION
     */
    public function checklogin( $username, $password){
        $q = $this->db
                    ->join('user_info','user_authentication.user_id = user_info.user_id')
                    ->where( array('user_email'=>$username) )
                    ->get('user_authentication');
        $result = $q->result_array();
        if( $q->num_rows() > 0 ){
            $password_db = $result[0]['user_password'];
            if(password_verify( $password, $password_db)) {
                return $result[0];
            }else{
                return FALSE;
            }

        }else{
            return FALSE; 
        }
    }//fn

     /**
     * Check Remember Me FUNCTION
     */
    public function check_remember_me( $token){
        if( $token != '' ){
            $q = $this->db
                    ->where( array('remember_me_token'=>$token) )
                    ->get('wst_admin');
            $result = $q->result_array();
            if( $q->num_rows() > 0 ){
                return $result[0];
            }else{
                return false;
            }
        }else{
            return false;
        }
       
    }//fn

     /**
     * Create Slug FUNCTION
     */
    public function create_slug( $name ){
        $slug = strtolower($name); 
        $slug = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $slug = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.

        return $slug;
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Register function
    |--------------------------------------------------------------------------
    */

    public function generate_user_token(  ){
        $user_token = substr(str_shuffle(md5(time())), 0, 32);;
        $q = $this->db
                    ->where( array('user_token'=>$user_token) )
                    ->get('user_authentication');
        if( $q->num_rows() > 0 ){
            $this->generate_user_token();
        }else{
            return $user_token;
        }
    }//fn

    public function register_user_to_db( $email, $password, $username, $server, $phone){
        $user_token = $this->generate_user_token();

        $qcheckusername = $this->db
                    ->select('user_authentication.user_id')
                    ->where( array('user_name'=>$username) )
                    ->get('user_authentication');
        if( $qcheckusername->num_rows() > 0 ){
            return false;
        }else{
            $qcheckemail = $this->db
                        ->select('user_authentication.user_id')
                        ->where( array('user_email'=>$email) )
                        ->get('user_authentication');
            if( $qcheckemail->num_rows() > 0 ){
                return false;
            }else{
                $data1 = array(
                    'user_token'        =>$user_token,
                    'user_slug'         =>$user_token,
                    'user_name'         =>$username,
                    'user_password'     =>password_hash($password, CRYPT_BLOWFISH),
                    'user_email'        =>$email,
                    'user_status'       =>'0'
                );
                $insert1 = $this->db->insert('user_authentication', $data1);
                $user_id = $this->db->insert_id();
                if( $insert1 ){
                    $data2 = array(
                        'user_id'           => $user_id,
                        'user_token'        => $user_token,
                        'user_info_phone'   => $phone,
                    );
                    $insert2 = $this->db->insert('user_info', $data2);
                    if( $insert2 ){
                        $data3 = array(
                            'user_id'           => $user_id,
                            'user_token'        => $user_token,
                        );
                        $insert3 = $this->db->insert('user_extra', $data3);
                        // $insert7 = $this->db->insert('user_mobile_device_info', $data3);
                        if( $insert3 ){
                            if(  !array_key_exists('HTTP_HOST',$server) ){
                               $server['HTTP_HOST'] == null; 
                            }
                            if( !array_key_exists('HTTP_USER_AGENT',$server) ){
                               $server['HTTP_USER_AGENT'] == null; 
                            }
                            if(!array_key_exists('HTTP_USER_AGENT',$server) ){
                               $server['HTTP_USER_AGENT'] == null; 
                            }
                            if( !array_key_exists('SERVER_ADDR',$server) ){
                               $server['SERVER_ADDR'] == null; 
                            }
                            if( !array_key_exists('REMOTE_ADDR',$server) ){
                               $server['REMOTE_ADDR'] == null; 
                            }
                            $data4 = array(
                                'user_id'       => $user_id,
                                'user_token'    => $user_token,
                                'http_host'     => $server['HTTP_HOST'],
                                'browser'       => null,
                                'mobile_browser'=> null,
                                'device'        => $server['HTTP_USER_AGENT'],
                                'server_address'=> $server['SERVER_ADDR'],
                                'server_port'   => $server['SERVER_PORT'],
                                'remote_address'=> $server['REMOTE_ADDR']    
                            );
                            
                            $insert4 = $this->db->insert('user_web_device_info', $data4);
                            $insert6 = $this->db->insert('user_mobile_device_info', $data3);
                            if( $insert4 AND $insert6){
                                $otp = rand(100000,999999);

                                $this->load->library('email');
                                //SMTP & mail configuration
                                $config = array(
                                    'protocol'  => 'smtp',
                                    'smtp_host' => 'ssl://smtp.googlemail.com',
                                    'smtp_port' => 465,
                                    'smtp_user' => 'palefire2019@gmail.com',
                                    'smtp_pass' => '1sarasij2roy',
                                    'mailtype'  => 'html',
                                    'charset'   => 'utf-8'
                                );
                                $this->email->initialize($config);
                                $this->email->set_mailtype("html");
                                $this->email->set_newline("\r\n");

                                $this->email->to(strval($email));
                                $this->email->from('palefire2019@gmail.com','Walledstory');
                                $list = array('sarasij94@gmail.com', 'sarasij@thepalefire.com');
                                $this->email->cc($list);
                                $this->email->subject('OTP');
                                $this->email->message( $otp );

                                if(!$this->email->send()){
                                    // print_r($this->email->print_debugger());
                                }else{
                                    // echo  1;
                                }
                                $otp_data = array(
                                    'user_id'       => $user_id,
                                    'user_token'    => $user_token,
                                    'otp'           => $otp,
                                );
                                $insert5 = $this->db->insert('user_registration_otp', $otp_data);
                                if( $insert5 ){
                                    return array($user_token, $otp); 



                                    
                                }else{
                                    false;
                                }
                                
                            }
                        }
                    }
                }
            }
        }

        
        return $data1;
    }//fn
    
    public function check_username_exist( $username ){
        $q = $this->db
                    ->select('user_authentication.user_id')
                    ->where( array('user_name'=>$username) )
                    ->get('user_authentication');
        if( $q->num_rows() > 0 ){
            return TRUE;
        }else{
            return FALSE;
        }
    }//fn
    public function check_email_exist( $email ){
        $q = $this->db
                    ->select('user_authentication.user_id')
                    ->where( array('user_email'=>$email) )
                    ->get('user_authentication');
        if( $q->num_rows() > 0 ){
            return TRUE;
        }else{
            return FALSE;
        }
    }//fn

    public function verify_email( $user_token, $otp ){
        $q1 = $this->db
                    ->where(array('user_token' => $user_token, 'otp'=> $otp))
                    ->get('user_registration_otp');
        if( $q1->num_rows()>0 ){
            $update_user_auth_table = $this->db->update('user_authentication', array('user_status' => '1'), array('user_token' => $user_token));
            if( $update_user_auth_table ){
                $delete_otp = $this->db->delete('user_registration_otp', array('user_token'=> $user_token));
                if( $delete_otp ){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{  
            return false;
        }
    }//fn

    public function get_user_info( $user_token ){
        $q = $this->db
                    ->select('
                        user_authentication.user_id,
                        user_authentication.user_slug,
                        user_authentication.user_name,
                        user_authentication.user_email,
                        user_authentication.user_status,
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_info.user_info_phone,
                        user_info.user_image,
                        user_info.show_name,
                        user_info.show_image,
                        ')
                    ->join('user_info', 'user_authentication.user_token = user_info.user_token')
                    ->where( array('user_authentication.user_token'=> $user_token) )
                    ->get('user_authentication');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0];
        }else{
            return false;
        }
    }//fn

    public function save_extra_info( $data ){
        $insert = $this->db->update('user_info', $data, array('user_token' => $this->session->userdata['wst23xyzsdfretw89lk_user_token']));
        if( $insert  ){
            return true;
        }else{
            return false;
        }
    }//fn

    public function save_extra_info_api( $data, $user_token ){
        $insert = $this->db->update('user_info', $data, array('user_token' => $user_token));
        if( $insert  ){
            return true;
        }else{
            return false;
        }
    }//fn
    
    public function get_user_id_by_username( $user_name ){
        $q =$this->db
                    ->select('user_id')
                    ->where(array('user_name' => $user_name))
                    ->get('user_authentication');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0]['user_id'];
        }else{
            return false;
        }
    }//fn

    public function get_user_token_by_username( $user_name ){
        $q = $this->db
                    ->select('user_token')
                    ->where(array('user_name' => $user_name))
                    ->get('user_authentication');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0]['user_token'];
        }else{
            return false;
        }
    }//fn

    public function get_user_name_by_id( $user_id ){
        $q = $this->db
                ->select('user_info.user_info_fname, user_info.user_info_lname,user_authentication.user_name')
                ->join('user_authentication','user_info.user_id = user_authentication.user_id')
                ->where( array('user_info.user_id' => $user_id) )
                ->get('user_info');

        if( $q->num_rows() > 0 ){
            $name = $q->result_array()[0]['user_info_fname'].' '.$q->result_array()[0]['user_info_lname'];
            if( $name == null ){
                $name = $q->result_array()[0]['user_name'];
            }
            return $name;
        }else{
            return false;
        }

    }//fn

    public function get_user_token_by_id( $user_id ){
        $q = $this->db
                ->select('user_authentication.user_token')
                ->where( array('user_authentication.user_id' => $user_id) )
                ->get('user_authentication');

        if( $q->num_rows() > 0 ){
            $token = $q->result_array()[0]['user_token'];
            return $token;
        }else{
            return false;
        }

    }//fn
    public function get_user_id_by_token( $user_token ){
        $q = $this->db
                ->select('user_authentication.user_id')
                ->where( array('user_authentication.user_token' => $user_token) )
                ->get('user_authentication');

        if( $q->num_rows() > 0 ){
            $token = $q->result_array()[0]['user_id'];
            return $token;
        }else{
            return false;
        }

    }//fn
    public function get_user_device_token( $user_id ){
        $q = $this->db
                ->select('user_mobile_device_info.firebase_token')
                ->where( array('user_mobile_device_info.user_id' => $user_id) )
                ->get('user_mobile_device_info');

        if( $q->num_rows() > 0 ){
            $token = $q->result_array()[0]['firebase_token'];
            return $token;
        }else{
            return false;
        }

    }//fn
    /*
    |--------------------------------------------------------------------------
    | Generate Post slug function
    |--------------------------------------------------------------------------
    */

    public function generate_post_slug( $title ){
        if (!preg_match('/[A-Za-z]/', $title)){
           $title='No title';
        }
        $slug       = strtolower($title); 
        $slug       = str_replace(' ', '-', $slug); // Replaces all spaces with hyphens.
        $slug       = preg_replace('/[^A-Za-z0-9\-]/', '', $slug); // Removes special chars.
        $post_slug  = preg_replace('/-+/', '-', $slug); // Replaces multiple hyphens with single one.

        $q = $this->db
                    ->like( 'post_slug',$post_slug )
                    ->get('wst_posts');
        if( $q->num_rows() > 0 ){
            $post_slug = $post_slug.'-'.$q->num_rows();
        }else{
            $post_slug = $post_slug;
        }
        return $post_slug;
    
    }//fn

     /*
    |--------------------------------------------------------------------------
    | Update user_cat_stat function
    |--------------------------------------------------------------------------
    */

    public function update_user_cat_stat( $user_id, $cat_id ){
        $q = $this->db
                    ->where(array('cat_id' => $cat_id, 'user_id' => $user_id))
                    ->get('user_cat_stat');
        if( $q->num_rows()> 0 ){
            $usr_cat_stat_id = $q->result_array()[0]['usr_cat_stat_id'];
            $count = $q->result_array()[0]['count'];
            $count++;
            $update = $this->db->update('user_cat_stat', array('count' => $count), array('usr_cat_stat_id' => $usr_cat_stat_id));
            if( $update ){
                return true;
            }else{
                return false;
            }
        }else{
            $insert = $this->db->insert('user_cat_stat', array('cat_id' => $cat_id, 'user_id' => $user_id,'count'=>1));
            if( $insert ){
                return true;
            }else{
                return false;
            }
        }
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Get POST function
    |--------------------------------------------------------------------------
    */

    public function get_post( $limit = null, $offset = null ){
        if( $limit == null ){
            $q = $this->db
                        ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->where( array('post_status' => '1') )
                        ->order_by('post_id', 'DESC')
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }else{
            $q = $this->db
                        ->select('wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->where( array('post_status' => '1') )
                        ->order_by('post_id', 'DESC')
                        ->limit($limit, $offset)
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }
    }//fn

    public function get_shorts_post( $limit = null, $offset = null ){
        if( $limit == null ){
            $q = $this->db
                        ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->where( array('post_status' => '1','template' =>'0') )
                        ->order_by('post_id', 'DESC')
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }else{
            $q = $this->db
                        ->select('wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->where( array('post_status' => '1', 'template' =>'0') )
                        ->order_by('post_id', 'DESC')
                        ->limit($limit, $offset)
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }
    }//fn

    public function get_shots_post( $limit = null, $offset = null ){
        if( $limit == null ){
            $q = $this->db
                        ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->where( array('post_status' => '1','template' =>'1') )
                        ->order_by('post_id', 'DESC')
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }else{
            $q = $this->db
                        ->select('wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->where( array('post_status' => '1', 'template' =>'1') )
                        ->order_by('post_id', 'DESC')
                        ->limit($limit, $offset)
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }
    }//fn
    public function get_post_user( $limit = null, $offset = null, $user_id ){
        if( $limit == null ){
            $q = $this->db
                        ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_id,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->order_by('post_id', 'DESC')
                        ->where(array('wst_posts.user_id' => $user_id))
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }else{
            $q = $this->db
                        ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_id,
                            user_info.user_info_fname,
                            user_info.user_info_lname, 
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->order_by('post_id', 'DESC')
                        ->where(array('wst_posts.user_id' => $user_id))
                        ->limit($limit, $offset)
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }
    }//fn
    public function get_post_by_slug( $post_slug ){
        $q = $this->db
                    ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname, 
                            user_info.user_image,
                            user_info.user_cover_image,
                            user_info.user_bio,
                            user_info.show_image,
                            user_info.show_bio,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                    ->where(array('wst_posts.post_slug' => $post_slug))
                    ->join('user_info','wst_posts.user_id = user_info.user_id')
                    ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                    ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                    ->get('wst_posts');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0];
        }else{
            return false;
        }

    }//fn

    public function get_post_by_id( $post_id ){
        $q = $this->db
                    ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname, 
                            user_info.user_image,
                            user_info.user_cover_image,
                            user_info.user_bio,
                            user_info.show_image,
                            user_info.show_bio,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                    ->where(array('wst_posts.post_id' => $post_id))
                    ->join('user_info','wst_posts.user_id = user_info.user_id')
                    ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                    ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                    ->get('wst_posts');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0];
        }else{
            return false;
        }

    }//fn

    public function get_post_by_cat( $limit = null, $offset = null, $cat_id ){
        if( $limit == null ){
            $q = $this->db
                        ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->where( array('post_status' => '1', 'wst_posts.cat_id'=>$cat_id) )
                        ->order_by('post_id', 'DESC')
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }else{
            $q = $this->db
                        ->select('wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.cat_id,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                        ->join('user_info','wst_posts.user_id = user_info.user_id')
                        ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                        ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                        ->where( array('post_status' => '1', 'wst_posts.cat_id'=>$cat_id) )
                        ->order_by('post_id', 'DESC')
                        ->limit($limit, $offset)
                        ->get('wst_posts');
            if( $q->num_rows()>0 ){
                return $q->result_array();
            }else{
                return null;
            }
        }
    }//fn

    public function get_popular_posts(){
        $sql = "SELECT COUNT(wst_posts_like.post_id) mycount,wst_posts_like.post_id FROM wst_posts_like GROUP BY wst_posts_like.post_id LIMIT 5";

        $q1 = $this->db->query($sql);
        $post_id_array = $q1->result_array();

        $result = array();
        $counter = 0;
        foreach ($post_id_array as $key => $post_id) {
            $post_details = $this->get_post_by_id($post_id['post_id']);
            $result[$counter] = $post_details;
            $counter++;
        }
        return $result;

    }//fn

    public function get_favourite_post( $user_id ){
        
        $q = $this->db
                    ->select('
                        wst_posts.post_id,
                        wst_posts.user_id,
                        wst_posts.post_slug,
                        wst_posts.post_title,
                        wst_posts.post_content,
                        wst_posts.featured_img_url,
                        wst_posts.cat_id,
                        wst_posts.template,
                        wst_posts.post_time,
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_info.user_image,
                        user_info.show_image,
                        user_info.show_name,
                        user_authentication.user_name,
                        user_authentication.user_token,
                        wst_category.cat_name
                        ')
                    ->where( array('wst_favourite_post.user_id' => $user_id) )
                    ->join('wst_posts','wst_favourite_post.post_id = wst_posts.post_id')
                    ->join('user_info','wst_posts.user_id = user_info.user_id')
                    ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                    ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                    ->order_by('post_id', 'DESC')
                    ->get('wst_favourite_post');
        if( $q->num_rows()>0 ){
            return $q->result_array();
        }else{
            return null;
        }
        
    }//fn

    public function get_post_slug_by_id( $post_id ){
        $q = $this->db
                    ->select('
                            wst_posts.post_slug,
                            ')
                    ->where(array('wst_posts.post_id' => $post_id))
                    // ->join('user_info','wst_posts.user_id = user_info.user_id')
                    // ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                    // ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                    ->get('wst_posts');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0]['post_slug'];
        }else{
            return false;
        }
    }//fn
    
    /*
    |--------------------------------------------------------------------------
    | Get profile image  function
    |--------------------------------------------------------------------------
    */
    public function get_all_profile_image( $user_id ){
        $q = $this->db
                    ->where( array('user_id' => $user_id, 'gallery_type' => '0') )
                    ->get('wst_gallery');
        if( $q->num_rows()>0 ){
            return $q->result_array();
        }else{
            false;
        }
    }//fn
    /*
    |--------------------------------------------------------------------------
    | Get User function
    |--------------------------------------------------------------------------
    */
    public function get_user_by_token( $user_token ){
        $q = $this->db
                    ->select('
                        user_authentication.user_id,
                        user_authentication.user_name,
                        user_authentication.user_email,
                        user_info.user_info_address,
                        user_info.user_info_city,
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_info.user_image,
                        user_info.user_cover_image,
                        user_info.user_bio,
                        user_info.user_profession,
                        user_info.user_info_gender,
                        user_info.user_info_phone,
                        user_info.user_info_country,
                        user_info.user_info_zip,
                        user_info.user_info_state,
                        user_info.show_name,
                        user_info.show_profession,
                        user_info.show_image,
                        user_info.show_address,
                        user_info.show_gender,
                        user_info.show_bio,
                        ')
                    ->join('user_info','user_authentication.user_token = user_info.user_token')
                    ->where( array('user_authentication.user_token' => $user_token) )
                    ->get('user_authentication');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0];
        }else{
            return null;
        }
    }//fn

    public function get_user_by_token_without_id( $user_token ){
        $q = $this->db
                    ->select('
                        user_authentication.user_name,
                        user_authentication.user_email,
                        user_info.user_info_address,
                        user_info.user_info_city,
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_info.user_image,
                        user_info.user_cover_image,
                        user_info.user_bio,
                        user_info.user_profession,
                        user_info.user_info_gender,
                        user_info.user_info_phone,
                        user_info.user_info_country,
                        user_info.user_info_zip,
                        user_info.user_info_state,
                        user_info.show_name,
                        user_info.show_profession,
                        user_info.show_image,
                        user_info.show_address,
                        user_info.show_gender,
                        user_info.show_bio,
                        ')
                    ->join('user_info','user_authentication.user_token = user_info.user_token')
                    ->where( array('user_authentication.user_token' => $user_token) )
                    ->get('user_authentication');
        if( $q->num_rows()>0 ){
            return $q->result_array()[0];
        }else{
            return null;
        }
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Like Unlike function
    |--------------------------------------------------------------------------
    */
    public function check_like( $post_id, $user_id ){
        $q = $this->db
                    ->where( array('post_id' => $post_id, 'user_id' => $user_id) )
                    ->get('wst_posts_like');
        if( $q->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }//fn

    public function like_count( $post_id ){
        $q = $this->db
                    ->where( array('post_id' => $post_id) )
                    ->get('wst_posts_like');
        if( $q->num_rows() > 0 ){
            return $q->num_rows();
        }else{
            return 0;
        }
    }//fn

    public function fetch_like_list( $post_id, $limit=null, $offset=null ){
        if( $limit = null ){
           $q = $this->db
                    ->select('
                        wst_posts_like.post_like_id,
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_authentication.user_name,
                        user_info.show_name,
                        ')
                    ->where( array('post_id' => $post_id) )
                    ->order_by('post_like_id', 'DESC')
                    ->get('wst_posts_like'); 
            if( $q->num_rows()>0 ){
                $like_persons_list = $q->result_array();
                return $like_persons_list;
            }else{
                return false;
            }
        }
            $q = $this->db
                    ->select('
                        wst_posts_like.post_like_id,
                        user_info.user_info_fname,
                        user_info.user_info_lname,
                        user_authentication.user_name,
                        user_info.show_name,
                        ')
                    ->where( array('post_id' => $post_id) )
                    ->join('user_info', 'wst_posts_like.user_id=user_info.user_id')
                    ->join('user_authentication', 'wst_posts_like.user_id=user_authentication.user_id')
                    ->order_by('post_like_id', 'DESC')
                    ->limit($limit, $offset)
                    ->get('wst_posts_like');
            if( $q->num_rows()>0 ){
                $like_persons_list = $q->result_array();
                return $like_persons_list;
            }else{
                return false;
            }
        
    }//fn

    public function check_like_exceeds_eight($post_id){
         $q = $this->db
                    ->select('post_like_id,')
                    ->where( array('post_id' => $post_id) )
                    ->get('wst_posts_like');
            if( $q->num_rows()>8 ){
                return true;
            }else{
                return false;
            }
    }
    /*
    |--------------------------------------------------------------------------
    | Favourite Posts function
    |--------------------------------------------------------------------------
    */
    public function check_favourite( $post_id, $user_id ){
        $q = $this->db
                    ->where( array('post_id' => $post_id, 'user_id' => $user_id) )
                    ->get('wst_favourite_post');
        if( $q->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }//fn
    /*
    |--------------------------------------------------------------------------
    | Notification function
    |--------------------------------------------------------------------------
    */
    public function fetch_notification( $user_id ){
        $q_unseen = $this->db
                    ->where(array('not_status' => '0', 'not_user_id' => $user_id))
                    ->get('wst_notification');

        if( $q_unseen->num_rows() > 0 ){
            $q_unseen_not_nos = $q_unseen->num_rows();
            $q_unseen_nots = $q_unseen->result_array();
        }else{
            $q_unseen_not_nos = 0;
            $q_unseen_nots = array();
        }

        $q_seen = $this->db
                    ->where(array('not_status' => '1', 'not_user_id' => $user_id))
                    ->get('wst_notification');

        if( $q_seen->num_rows() > 0 ){
            $q_seen_not_nos = $q_seen->num_rows();
            $q_seen_nots = $q_seen->result_array();
        }else{
            $q_seen_not_nos = 0;
            $q_seen_nots = array();
        }


        $q_all = $this->db
                    ->where(array('not_user_id' => $user_id))
                    ->get('wst_notification');

        if( $q_all->num_rows() > 0 ){
            $q_all_not_nos = $q_all->num_rows();
            $q_all_nots = $q_all->result_array();
        }else{
            $q_all_not_nos = 0;
            $q_all_nots = array();
        }

        $array = array(
            'unseen' => array( 'number' => $q_unseen_not_nos, 'notifications' => $q_unseen_nots  ),
            'seen'   => array( 'number' => $q_seen_not_nos , 'notifications' => $q_seen_nots),
            'all'    => array( 'number' => $q_all_not_nos , 'notifications' => $q_all_nots),
        );
        return $array;
    }//fn

    public function fetch_header_notification( $user_id ){
        $q_all = $this->db
                    ->where(array('not_user_id' => $user_id))
                    ->limit(10)
                    ->get('wst_notification');
        if( $q_all->num_rows() > 0 ){
            return $q_all_nots = $q_all->result_array();
        }else{
            return false;
        }
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Post Image function
    |--------------------------------------------------------------------------
    */

    public function delete_old_image_after_post_image_update( $gallery_id ){
        $q = $this->db
                    ->where(array( 'gallery_id' => $gallery_id ,'gallery_type' =>'1' ))
                    ->get('wst_gallery');
        if( $q->num_rows() > 0 ){
            $gallery_details = $q->result_array()[0];
            $file_name = $gallery_details['file_name'];
            $delete_path = './uploads/post/'.$file_name;
            $unlink = unlink( $delete_path );
            if( $unlink ){
                $delete = $this->db->delete( 'wst_gallery', array('gallery_id' => $gallery_id) );
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }//fn
    public function get_img_id_for_post( $post_id ){
        $q = $this->db
                    ->select('post_id,featured_img_id')
                    ->where( array('post_id' => $post_id) )
                    ->get('wst_posts');
        if( $q->num_rows() > 0 ){
            return $q->result_array()[0]['featured_img_id'];
        }else{
            return false;
        }
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Category function
    |--------------------------------------------------------------------------
    */

    public function get_cat_id_by_cat_slug( $cat_slug ){
        $q = $this->db
                    ->select('cat_id')
                    ->where( array('cat_slug' => $cat_slug) )
                    ->get('wst_category');
        if( $q->num_rows() > 0 ){
            return $q->result_array()[0]['cat_id'];
        }else{
            return false;
        }
    }//fn
    public function get_cat_name_by_cat_slug( $cat_slug ){
        $q = $this->db
                    ->select('cat_name')
                    ->where( array('cat_slug' => $cat_slug) )
                    ->get('wst_category');
        if( $q->num_rows() > 0 ){
            return $q->result_array()[0]['cat_name'];
        }else{
            return false;
        }
    }//fn

    /*
    |--------------------------------------------------------------------------
    | Report function
    |--------------------------------------------------------------------------
    */

    public function if_already_reported( $post_id, $user_id ){
        $q = $this->db
                    ->select('report_id')
                    ->where( array('post_id' => $post_id, 'user_id'=> $user_id) )
                    ->get('wst_report_posts');
        if( $q->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }//fn
    /*
    |--------------------------------------------------------------------------
    | Seacrh function
    |--------------------------------------------------------------------------
    */
    public function search_post_by_keyword( $keyword ){

        $q = $this->db
                    ->select('
                            wst_posts.post_id,
                            wst_posts.user_id,
                            wst_posts.post_slug,
                            wst_posts.post_title,
                            wst_posts.post_content,
                            wst_posts.featured_img_url,
                            wst_posts.template,
                            wst_posts.post_time,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            wst_category.cat_name
                            ')
                    ->join('user_info','wst_posts.user_id = user_info.user_id')
                    ->join('user_authentication','wst_posts.user_id = user_authentication.user_id')
                    ->join('wst_category','wst_posts.cat_id = wst_category.cat_id')
                    ->like('post_title', $keyword)
                    ->limit(8, 0)
                    ->get('wst_posts');
        if( $q->num_rows() > 0 ){
            return $q->result_array();
        }else{
            return false;
        }   
    }//fn

    public function search_user_by_keyword( $keyword ){

        $q = $this->db
                    ->select('
                            user_info.user_id,
                            user_info.user_info_fname,
                            user_info.user_info_lname,
                            user_info.user_image,
                            user_info.show_image,
                            user_info.show_name,
                            user_authentication.user_name,
                            user_authentication.user_token,
                            ')
                    ->join('user_authentication','user_info.user_id = user_authentication.user_id')
                    ->like('user_info_fname', $keyword)
                    ->where(array('user_authentication.user_status' => '1'))
                    ->limit(8, 0)
                    ->get('user_info');
        if( $q->num_rows() > 0 ){
            return $q->result_array();
        }else{
            return false;
        }   
    }//fn
}//class
